import CommuneDTO from "../../../../data-access/repositories/DTO/CommuneDTO"
import RoleDTO from "../../../../data-access/repositories/DTO/RoleDTO"

class UserModel {

    private id?: number
    private lastName?: string
    private firstName?: string
    private mail?: string
    private password?: string
    private isActive?: number
    private commune?: CommuneDTO | undefined
    private role?: RoleDTO | undefined
    public get getCommune(): CommuneDTO | undefined {
        return this.commune
    }
    public set setCommune(value: CommuneDTO | undefined) {
        this.commune = value
    }
    public get getRole(): RoleDTO | undefined {
        return this.role
    }
    public set getRole(value: RoleDTO | undefined) {
        this.role = value
    }

    constructor(
    ){}

    // Getters
    get getFirstName(): string | undefined {
        return this.firstName;
    }

    get getLastName(): string | undefined {
        return this.lastName;
    }

    get getMail(): string | undefined {
        return this.mail;
    }

    get getPassword(): string | undefined {
        return this.password;
    }

    get getIsActive(): number | undefined {
        return this.isActive;
    }

    get getId(): number | undefined {
        return this.id;
    }

    // Setters
    set setFirstName(value: string | undefined) {
        this.firstName = value;
    }

    set setLastName(value: string | undefined) {
        this.lastName = value;
    }

    set setMail(value: string | undefined) {
        this.mail = value;
    }

    set setPassword(value: string | undefined) {
        this.password = value;
    }

    set setIsActive(value: number | undefined) {
        this.isActive = value;
    }

    set setId(value: number | undefined) {
        this.id = value;
    }
}

export default UserModel