import UserModel from "./models/UserModel"
import UserDTO from "../../../data-access/repositories/DTO/UserDTO"
import UserRepository from "../../../data-access/repositories/UserRepository"
import BaseService from "../BaseService"

class UserService implements BaseService<UserDTO, UserModel> {
    userRepository : UserRepository = new UserRepository()

    constructor() {}

    toGetAllModel(dto: UserDTO): UserModel {
        const userModel: UserModel = new UserModel()
        userModel.setId = dto.getId
        userModel.setFirstName = dto.getFirstName
        userModel.setLastName = dto.getLastName
        userModel.setMail = dto.getMail
        userModel.setPassword = dto.getPassword
        userModel.setIsActive = dto.getIsActive
        userModel.setCommune = dto.getCommune
        userModel.getRole = dto.getRole
        
        return userModel
    }

    toUserDTO(model: UserModel): UserDTO {
        const dto: UserDTO = new UserDTO()
        if(model.getId && model.getFirstName && model.getLastName && model.getMail && model.getPassword && model.getIsActive) {
            dto.setId = model.getId
            dto.setFirstName = model.getFirstName
            dto.setLastName = model.getLastName
            dto.setMail = model.getMail
            dto.setPassword = model.getPassword
            dto.setIsActive = model.getIsActive
        }
        return dto
    }

    async getAll(): Promise<UserModel[] | undefined> {
        try {
            const usersDTO: UserDTO[] |undefined = await this.userRepository.getAll()
            const usersModel: UserModel[] | undefined = usersDTO ? usersDTO.map((userDTO: UserDTO) => {
                return this.toGetAllModel(userDTO)
            }) : undefined

            return usersModel
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default UserService