class UserModel {

    private id?: number
    private lastName?: string
    private firstName?: string
    private mail?: string
    private password?: string
    private isActive?: number
    private ZIPCode?: string
    private role?: string

    constructor(
    ){}

    // Getters
    get getFirstName(): string | undefined {
        return this.firstName;
    }

    get getLastName(): string | undefined {
        return this.lastName;
    }

    get getMail(): string | undefined {
        return this.mail;
    }

    get getPassword(): string | undefined {
        return this.password;
    }

    get getIsActive(): number | undefined {
        return this.isActive;
    }

    get getId(): number | undefined {
        return this.id;
    }

    get getZIPCode(): string | undefined {
        return this.ZIPCode;
    }

    get getRole(): string | undefined {
        return this.role
    }

    // Setters
    set setFirstName(value: string | undefined) {
        this.firstName = value;
    }

    set setLastName(value: string | undefined) {
        this.lastName = value;
    }

    set setMail(value: string | undefined) {
        this.mail = value;
    }

    set setPassword(value: string | undefined) {
        this.password = value;
    }

    set setIsActive(value: number | undefined) {
        this.isActive = value;
    }

    set setId(value: number | undefined) {
        this.id = value;
    }
    
    set setZIPCode(value: string | undefined) {
        this.ZIPCode = value
    }

    set setRole(value: string | undefined) {
        this.role = value
    }
}

export default UserModel