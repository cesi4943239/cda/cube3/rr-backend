interface BaseService<T, U> {
    toGetAllModel(...args: any[]): U
    toDTO?<K>(model: U): K
    getAll?(): Promise<U[] | undefined>
    getById?(id: number): Promise<U | undefined>
    create?(model: U): Promise<U | undefined>
    update?(model: U): Promise<U | undefined>
    delete?(id: number): Promise<U | undefined>
}

export default BaseService