import bcrypt from "bcryptjs"
import jwt  from "jsonwebtoken"

import AuthModel from "./models/AuthModel"
import IAuthOutput from "./models/IAuthOutput"
import ILoginInput from "./models/ILoginInput"
import IPayload from "./models/IPayload"
import PayloadModel from "./models/IPayload"
import IRegisterInput from "./models/IRegisterInput"
import config from "../../../../config"
import RoleDTO from "../../../data-access/repositories/DTO/RoleDTO"
import UserDTO from "../../../data-access/repositories/DTO/UserDTO"
import UserRepository from "../../../data-access/repositories/UserRepository"
import ApiError from "../../../presentation/errors/ApiError"
import RolesEnum from "../../../shared/RolesEnum"
import BaseService from "../BaseService"

class AuthService implements BaseService<UserDTO, AuthModel> {
    private userRepository : UserRepository = new UserRepository()

    constructor() {}

    toGetAllModel(dto: UserDTO): AuthModel {
        try {
            const authModel: IAuthOutput = new AuthModel()
            const payload: IPayload = {
                userId: dto.getId,
                mail: dto.getMail,
                role: dto.getRole.code
            }
            authModel.token = this.createJwt(payload)
            if(!authModel.token) { ApiError.internal() }

            return authModel
        } catch(err: any) {
            throw err as Error
        }
    }

    toUserDTO(inputData: IRegisterInput): UserDTO {
        const dto: UserDTO = new UserDTO()
        if(inputData.firstName) dto.setFirstName = inputData.firstName
        if(inputData.lastName) dto.setLastName = inputData.lastName
        if(inputData.mail) dto.setMail = inputData.mail
        if(inputData.password) dto.setPassword = inputData.password
        if(inputData.idTown) dto.setIDCommune = inputData.idTown

        return dto
    }

    public async register(inputData: IRegisterInput): Promise<AuthModel> {
        try {
            const userDTO: UserDTO = this.toUserDTO(inputData)
            userDTO.setIDCommune = 1
            if(!userDTO) { throw ApiError.internal() }

            // new account will always have the user role
            userDTO.setRole = new RoleDTO()
            userDTO.getRole.code = RolesEnum.user
            userDTO.setIsActive = 1

            const createdUserDTO: UserDTO | undefined = await this.userRepository.create(userDTO)
            if(!createdUserDTO || !createdUserDTO.getId || !createdUserDTO.getMail || !createdUserDTO.getRole) { throw ApiError.internal() }

            return this.toGetAllModel(createdUserDTO)
        } catch(err: any) {
            throw err as Error
        }
    }

    public async login(inputData: ILoginInput): Promise<AuthModel> {
        try {
            if(!inputData.mail || !inputData.password) { throw ApiError.badRequest() }

            const userDTO: UserDTO | undefined = await this.userRepository.getByMail(inputData.mail)
            if(!userDTO?.getPassword) { throw ApiError.notFound() }

            const isAuthenticated: boolean = await bcrypt.compare(inputData.password, userDTO?.getPassword)
            if(!isAuthenticated) { throw ApiError.unauthorized() }
            
            return this.toGetAllModel(userDTO)
        } catch(err: any) {
            throw err as Error
        }
    }
    
    private createJwt(payload: PayloadModel): string {
        const secretKey = config.jwt.SECRET_KEY
        const options = { expiresIn: config.jwt.DURATION }
        return jwt.sign(payload, secretKey, options)
    }
}

export default AuthService