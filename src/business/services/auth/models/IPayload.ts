interface IPayload {
    userId: number,
    mail: string,
    role: string
}

export default IPayload