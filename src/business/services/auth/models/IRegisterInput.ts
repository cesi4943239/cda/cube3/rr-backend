
interface IRegisterInput {
    lastName: string
    firstName: string
    mail: string
    password: string
    idTown: number
}

export default IRegisterInput