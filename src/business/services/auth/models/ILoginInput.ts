interface ILoginInput {
    mail?: string
    password?: string
}

export default ILoginInput