import ICreateCodeModel from "./ICreateCodeModel"

interface ICreateResourceModel {
    title: string
    content: string
    resourceCategory: ICreateCodeModel
    resourceType: ICreateCodeModel
    relationTypes: ICreateCodeModel[]
}

export default ICreateResourceModel