import AuthorModel from "../models/AuthorModel"
import CodeModel from "../models/CodeModel"

interface IGetByIdResourceModel {
    id?: number
    title?: string
    creationDate?: Date
    author?: AuthorModel
    status?: CodeModel
    resourceCategory?: CodeModel
    resourceType?: CodeModel
    relationTypes?: CodeModel[]
    likeCount?: number
    savedCount?: number
    exploitedCount?: number
    nonExploitedCount?: number
    content?: string
}

export default IGetByIdResourceModel