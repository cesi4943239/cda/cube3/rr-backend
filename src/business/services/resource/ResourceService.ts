import { Request } from "express"

import IGetAllResourceModel from "./interfaces/IGetAllResourceModel"
import IGetByIdResourceModel from "./interfaces/IGetByIdResourceModel"
import AuthorModel from "./models/AuthorModel"
import CodeModel from "./models/CodeModel"
import ResourceModel from "./models/ResourceModel"
import RelationTypeDTO from "../../../data-access/repositories/DTO/RelationTypeDTO"
import ResourceCategoryDTO from "../../../data-access/repositories/DTO/ResourceCategoryDTO"
import ResourceDTO from "../../../data-access/repositories/DTO/ResourceDTO"
import ResourceTypeDTO from "../../../data-access/repositories/DTO/ResourceTypeDTO"
import StatusDTO from "../../../data-access/repositories/DTO/StatusDTO"
import UserDTO from "../../../data-access/repositories/DTO/UserDTO"
import FilterOptions from "../../../data-access/repositories/FilterOptions"
import ResourceRepository from "../../../data-access/repositories/ResourceRepository"
import ApiError from "../../../presentation/errors/ApiError"
import BaseService from "../BaseService"

class ResourceService implements BaseService<ResourceDTO, ResourceModel> {
    ResourceRepository: ResourceRepository = new ResourceRepository()

    constructor() { }

    toResourceDTO(model: ResourceModel) : ResourceDTO {
        try {
            const dto = new ResourceDTO()
            dto.title = model.title!
            dto.content = model.content!
            dto.relationTypes = model.relationTypes!.map( rt => {
                const rtDTO = new RelationTypeDTO()
                rtDTO.code = rt.code
                return rtDTO
            })
            dto.resourceCategory = new ResourceCategoryDTO()
            dto.resourceCategory.code = model.resourceCategory!.code
            dto.resourceType = new ResourceTypeDTO()
            dto.resourceType.code = model.resourceType!.code

            return dto
        } catch(err: any) {
            throw err as Error
        }   
    }

    toGetAllModel(dto: ResourceDTO): ResourceModel {

        try {
            const resourceModel: IGetAllResourceModel = new ResourceModel()
            resourceModel.id = dto.id
            resourceModel.title = dto.title
            resourceModel.creationDate = dto.creationDate

            resourceModel.author = new AuthorModel()
            resourceModel.author.firstname = dto.author.getFirstName
            resourceModel.author.lastname = dto.author.getLastName
            resourceModel.author.email = dto.author.getMail
            resourceModel.status = new CodeModel(dto.status)
            resourceModel.resourceCategory = new CodeModel(dto.resourceCategory)
            resourceModel.resourceType = new CodeModel(dto.resourceType)
            resourceModel.relationTypes = dto.relationTypes?.map(rt => new CodeModel(rt))

            resourceModel.likeCount = dto.likeCount
            resourceModel.savedCount = dto.savedCount
            resourceModel.exploitedCount = dto.exploitedCount
            resourceModel.nonExploitedCount = dto.nonExploitedCount

            return resourceModel
        } catch (err: any) {
            throw err as Error
        }
    }

    buildFilterOptions(req?: Request): FilterOptions {
        const filterOptions: FilterOptions = new FilterOptions();
        if (!req) return filterOptions

        filterOptions.limit = req.query.limit as number | undefined
        filterOptions.page = req.query.page as number | undefined

        if (req.query.orderBy) filterOptions.orderByField = this.orderMapping(req.query.orderBy as string)
        filterOptions.sort = req.query.sort as string | undefined ?? "desc"
        
        const authors = req.query.authors as string | undefined
        if (authors) filterOptions.authors = authors.split(';')
        filterOptions.search = req.query.search as string | undefined

        const relationTypes = req.query.relationTypes as string | undefined
        const ressourceTypes = req.query.ressourceTypes as string | undefined
        const ressourceCategories = req.query.ressourceCategories as string | undefined
        const ressourceStatus = req.query.status as string | undefined
        if (relationTypes) filterOptions.relationTypes = relationTypes.split(';')
        if (ressourceTypes) filterOptions.ressourceTypes = ressourceTypes.split(';')
        if (ressourceCategories) filterOptions.ressourceCategories = ressourceCategories.split(';')
        if (ressourceStatus) filterOptions.status = ressourceStatus.split(';')
        
        return filterOptions;
    }

    orderMapping(field : string) : string {
        switch (field) {
            case "creationDate": return "DateCreation"
            case "likeCount": return "(SELECT COUNT(*) FROM RessourceFavoris WHERE IDRessource = Ressource.ID)"
            case "exploitedCount": return "(SELECT COUNT(*) FROM RessourceExploite WHERE IDRessource = Ressource.ID)"
        }
        return ""
    }
    
     toGetByIdModel(dto: ResourceDTO): ResourceModel {

        try {
            const resourceModel: IGetByIdResourceModel = new ResourceModel()
            resourceModel.id = dto.id
            resourceModel.title = dto.title
            resourceModel.creationDate = dto.creationDate

            resourceModel.content = dto.content
            resourceModel.author = new AuthorModel()
            resourceModel.author.firstname = dto.author.getFirstName
            resourceModel.author.lastname = dto.author.getLastName
            resourceModel.author.email = dto.author.getMail
            resourceModel.status = new CodeModel(dto.status)
            resourceModel.resourceCategory = new CodeModel(dto.resourceCategory)
            resourceModel.resourceType = new CodeModel(dto.resourceType)
            resourceModel.relationTypes = dto.relationTypes?.map( rt => new CodeModel(rt))

            resourceModel.likeCount = dto.likeCount
            resourceModel.savedCount = dto.savedCount
            resourceModel.exploitedCount = dto.exploitedCount
            resourceModel.nonExploitedCount = dto.nonExploitedCount
            
            return resourceModel
        } catch(err: any) {
            throw err as Error
        }        
    }

    async getAll(req?: Request): Promise<ResourceModel[] | undefined> {
        try {
            const filterOptions = this.buildFilterOptions(req)
            const resourcesDTO: ResourceDTO[] | undefined = await this.ResourceRepository.getAll(filterOptions)

            const ResourcesModel: IGetAllResourceModel[] | undefined = resourcesDTO ? resourcesDTO.map((ResourceDTO: ResourceDTO) => {
                return this.toGetAllModel(ResourceDTO)
            }) : undefined

            return ResourcesModel

        } catch (err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<ResourceModel | undefined> {
        const resourceRepository: ResourceRepository = new ResourceRepository()
        const resourceDTO: ResourceDTO | undefined = await resourceRepository.getById(id)
        if(!resourceDTO) { throw ApiError.notFound() }

        return this.toGetByIdModel(resourceDTO)
    }

    async create(model: ResourceModel, email?: string): Promise<ResourceModel | undefined> {
        const dtoInput = this.toResourceDTO(model)
        dtoInput.author = new UserDTO()
        dtoInput.author.setMail = email!
        dtoInput.status = new StatusDTO()
        dtoInput.status.code = "PUBLIC"
        dtoInput.creationDate = new Date()

        const resourceRepository: ResourceRepository = new ResourceRepository()
        const resourceDTO: ResourceDTO | undefined = await resourceRepository.create(dtoInput)
        if (!resourceDTO) throw ApiError.internal()

        return this.toGetByIdModel(resourceDTO)
    }
}

export default ResourceService