import CodeDTO from "../../../../data-access/repositories/DTO/CodeDTO";

class CodeModel {
    public code: string
    public caption?: string

    constructor(dto: CodeDTO<any>)
    {
        this.code = dto?.code,
        this.caption = dto?.caption
    }
}

export default CodeModel