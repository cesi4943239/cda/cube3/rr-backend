import AuthorModel from "./AuthorModel"
import CodeModel from "./CodeModel"

class ResourceModel {
    public id?: number
    public title?: string
    public creationDate?: Date
    public content?: string
    public author?: AuthorModel
    public status?: CodeModel
    public resourceCategory?: CodeModel
    public resourceType?: CodeModel
    public relationTypes?: CodeModel[]
    public likeCount?: number
    public savedCount?: number
    public exploitedCount?: number
    public nonExploitedCount?: number

    constructor() {}
}

export default ResourceModel