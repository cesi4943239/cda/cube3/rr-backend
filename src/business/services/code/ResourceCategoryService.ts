import CodeModel from "./CodeModel"
import ResourceCategoryRepository from "../../../data-access/repositories/code/ResourceCategoryRepository"
import ResourceCategoryDTO from "../../../data-access/repositories/DTO/ResourceCategoryDTO"
import BaseService from "../BaseService"

class ResourceCategoryService implements BaseService<ResourceCategoryDTO, CodeModel> {
    ResourceCategoryRepository : ResourceCategoryRepository = new ResourceCategoryRepository()

    constructor() {}

    toGetAllModel(dto: ResourceCategoryDTO): CodeModel {
        const ResourceCategoryModel: CodeModel = new CodeModel(dto.code, dto.caption)
        return ResourceCategoryModel
    }

    toResourceCategoryDTO(model: CodeModel): ResourceCategoryDTO {

        const dto: ResourceCategoryDTO = new ResourceCategoryDTO()
        dto.code = model.getCode
        dto.caption = model.getCaption
        return dto
    }

    async getAll(): Promise<CodeModel[] | undefined> {
        try {
            const ResourceCategorysDTO: ResourceCategoryDTO[] |undefined = await this.ResourceCategoryRepository.getAll()
            const ResourceCategorysModel: CodeModel[] | undefined = ResourceCategorysDTO ? ResourceCategorysDTO.map((ResourceCategoryDTO: ResourceCategoryDTO) => {
                return this.toGetAllModel(ResourceCategoryDTO)
            }) : undefined

            return ResourceCategorysModel
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default ResourceCategoryService