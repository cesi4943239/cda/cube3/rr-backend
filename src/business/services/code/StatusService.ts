import CodeModel from "./CodeModel"
import StatusRepository from "../../../data-access/repositories/code/StatusRepository"
import StatusDTO from "../../../data-access/repositories/DTO/StatusDTO"
import BaseService from "../BaseService"

class StatusService implements BaseService<StatusDTO, CodeModel> {
    StatusRepository : StatusRepository = new StatusRepository()

    constructor() {}

    toGetAllModel(dto: StatusDTO): CodeModel {
        const StatusModel: CodeModel = new CodeModel(dto.code, dto.caption)
        return StatusModel
    }

    toStatusDTO(model: CodeModel): StatusDTO {

        const dto: StatusDTO = new StatusDTO()
        dto.code = model.getCode
        dto.caption = model.getCaption
        return dto
    }

    async getAll(): Promise<CodeModel[] | undefined> {
        try {
            const StatussDTO: StatusDTO[] |undefined = await this.StatusRepository.getAll()
            const StatussModel: CodeModel[] | undefined = StatussDTO ? StatussDTO.map((StatusDTO: StatusDTO) => {
                return this.toGetAllModel(StatusDTO)
            }) : undefined

            return StatussModel
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default StatusService