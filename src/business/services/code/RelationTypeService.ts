import CodeModel from "./CodeModel"
import RelationTypeRepository from "../../../data-access/repositories/code/RelationTypeRepository"
import RelationTypeDTO from "../../../data-access/repositories/DTO/RelationTypeDTO"
import BaseService from "../BaseService"

class RelationTypeService implements BaseService<RelationTypeDTO, CodeModel> {
    RelationTypeRepository : RelationTypeRepository = new RelationTypeRepository()

    constructor() {}

    toGetAllModel(dto: RelationTypeDTO): CodeModel {
        const RelationTypeModel: CodeModel = new CodeModel(dto.code, dto.caption)
        return RelationTypeModel
    }

    toRelationTypeDTO(model: CodeModel): RelationTypeDTO {

        const dto: RelationTypeDTO = new RelationTypeDTO()
        dto.code = model.getCode
        dto.caption = model.getCaption
        return dto
    }

    async getAll(): Promise<CodeModel[] | undefined> {
        try {
            const RelationTypesDTO: RelationTypeDTO[] |undefined = await this.RelationTypeRepository.getAll()
            const RelationTypesModel: CodeModel[] | undefined = RelationTypesDTO ? RelationTypesDTO.map((RelationTypeDTO: RelationTypeDTO) => {
                return this.toGetAllModel(RelationTypeDTO)
            }) : undefined

            return RelationTypesModel
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default RelationTypeService