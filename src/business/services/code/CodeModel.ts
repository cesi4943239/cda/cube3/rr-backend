class CodeModel {
    private code: string
    private caption: string

    constructor(code: string, caption: string)
    {
        this.code = code,
        this.caption = caption
    }

    // Getters
    get getCode(): string {
        return this.code;
    }

    get getCaption(): string {
        return this.caption;
    }

    // Setters
    set setCode(value: string) {
        this.code = value;
    }

    set setCaption(value: string) {
        this.caption = value;
    }

}

export default CodeModel