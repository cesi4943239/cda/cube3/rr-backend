import CodeModel from "./CodeModel"
import ResourceTypeRepository from "../../../data-access/repositories/code/ResourceTypeRepository"
import ResourceTypeDTO from "../../../data-access/repositories/DTO/ResourceTypeDTO"
import BaseService from "../BaseService"

class ResourceTypeService implements BaseService<ResourceTypeDTO, CodeModel> {
    ResourceTypeRepository : ResourceTypeRepository = new ResourceTypeRepository()

    constructor() {}

    toGetAllModel(dto: ResourceTypeDTO): CodeModel {
        const ResourceTypeModel: CodeModel = new CodeModel(dto.code, dto.caption)
        return ResourceTypeModel
    }

    toResourceTypeDTO(model: CodeModel): ResourceTypeDTO {

        const dto: ResourceTypeDTO = new ResourceTypeDTO()
        dto.code = model.getCode
        dto.caption = model.getCaption
        return dto
    }

    async getAll(): Promise<CodeModel[] | undefined> {
        try {
            const ResourceTypesDTO: ResourceTypeDTO[] |undefined = await this.ResourceTypeRepository.getAll()
            const ResourceTypesModel: CodeModel[] | undefined = ResourceTypesDTO ? ResourceTypesDTO.map((ResourceTypeDTO: ResourceTypeDTO) => {
                return this.toGetAllModel(ResourceTypeDTO)
            }) : undefined

            return ResourceTypesModel
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default ResourceTypeService