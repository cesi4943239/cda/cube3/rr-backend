import CommentModel from "./models/CommentModel"
import CommentRepository from "../../../data-access/repositories/CommentRepository"
import CommentDTO from "../../../data-access/repositories/DTO/CommentDTO"
import BaseService from "../BaseService"

class CommentService implements BaseService<CommentDTO, CommentModel> {
    commentRepository : CommentRepository = new CommentRepository()

    constructor() {}

    toGetAllModel(dto: CommentDTO): CommentModel {
        const commentModel: CommentModel = new CommentModel()
        if(dto.utilisateur?.getFirstName) { commentModel.author.firstname = dto.utilisateur?.getFirstName }
        if(dto.utilisateur?.getLastName) { commentModel.author.lastname = dto.utilisateur?.getLastName }
        if(dto.contenu) { commentModel.content = dto.contenu }
        if(dto.dateCreation) { commentModel.date = dto.dateCreation.toISOString()}

        // if(dto.reponseCommentaires) { commentModel.response = dto.reponseCommentaires }
        
        return commentModel
    }

    async getByUser(id: number): Promise<CommentDTO[] | undefined> {
        const commentRepo: CommentRepository = new CommentRepository()
        const commentsDTO: CommentDTO[] | undefined = await commentRepo.getByResourceId(id)

        return commentsDTO
    }

}

export default CommentService