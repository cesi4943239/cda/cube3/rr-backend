import IResponseCommentModel from "./IResponseCommentModel"
import AuthorModel from "../../resource/models/AuthorModel"

class CommentModel{
    public content?: string
    public date?: string
    public author!: AuthorModel
    public response?: IResponseCommentModel
}

export default CommentModel