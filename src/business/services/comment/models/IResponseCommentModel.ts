import AuthorModel from "../../resource/models/AuthorModel"

interface IResponseCommentModel{

    content?: string
    date?: string
    author?: AuthorModel

}

export default IResponseCommentModel