import { NextFunction, Request, Response } from "express"
import jwt  from "jsonwebtoken";

import ICreateResourceModel from "../../../business/services/resource/interfaces/ICreateResourceModel"
import ResourceModel from "../../../business/services/resource/models/ResourceModel"
import ResourceService from "../../../business/services/resource/ResourceService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"


class ResourceController implements BaseController<ResourceModel> {
    private ResourceService: ResourceService = new ResourceService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const Resources: ResourceModel[] | undefined = await this.ResourceService.getAll(req)
            if(!Resources) { throw ApiError.notFound() }
            if(Resources.length === 0) { throw ApiError.noContent() }
            if(Resources) { res.status(200).json({ data: Resources, page: req?.query?.page ?? 1 }) }
        } catch(err: any) {
            next(err)
        }
    }

    async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const id: number = parseInt(req.params.id)
            const resource: ResourceModel | undefined = await this.ResourceService.getById(id)
            if(!resource) { throw ApiError.noContent() }
            res.status(200).json({ data: resource })
        } catch(err: any) {
            next(err)
        }
    }

    async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const token = req.header('Authorization')?.replace('Bearer ', '');
            if (!token) throw ApiError.badRequest("Unauthorized")
            const decodedToken: any = jwt.decode(token)
            if (!decodedToken) throw ApiError.badRequest("Unauthorized")
            
            const input: ResourceModel = req.body as ICreateResourceModel
            const resource: ResourceModel | undefined = await this.ResourceService.create(input, decodedToken.email)
            if(!resource) { throw ApiError.noContent() }
            res.status(200).json({ data: resource })
        } catch(err: any) {
            next(err)
        }
    }

    async createTest(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const input: ResourceModel = req.body as ICreateResourceModel
            const resource: ResourceModel | undefined = await this.ResourceService.create(input, "9441Michael.Runte@yahoo.com")
            if(!resource) { throw ApiError.noContent() }
            res.status(200).json({ data: resource })
        } catch(err: any) {
            next(err)
        }
    }
}

export default ResourceController