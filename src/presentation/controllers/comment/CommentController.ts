import { NextFunction, Request, Response } from "express"

import CommentRepository from "../../../data-access/repositories/CommentRepository"
import CommentDTO from "../../../data-access/repositories/DTO/CommentDTO"

class CommentController {
    commentRepository: CommentRepository = new CommentRepository()    
    constructor() {}

    async getByUser(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const id: number = parseInt(req.params.id)
            const commentsDTO: CommentDTO[] | undefined = await this.commentRepository.getByResourceId(id)
            res.status(200).json({ data: commentsDTO })
        } catch(err: any) {
            next(err)
        }
    }
}

export default CommentController