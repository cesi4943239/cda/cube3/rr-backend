import { NextFunction, Request, Response } from "express"

import StatRepository from "../../../data-access/repositories/StatRepository";
import ApiError from "../../errors/ApiError";

class StatController {
    private StatService: StatRepository = new StatRepository()
    
    async countUsers(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const role = req?.query?.role ? req?.query?.role.toString() : undefined
            const result = await this.StatService.countUsers(role)
            if(!result) { throw ApiError.notFound() }
            if(result) { res.status(200).json(result) }
        } catch(err: any) {
            next(err)
        }
    }

    async countResources(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const status = req?.query?.status ? req?.query?.status.toString() : undefined
            const result = await this.StatService.countResources(status)
            if(!result) { throw ApiError.notFound() }
            if(result) { res.status(200).json(result) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default StatController