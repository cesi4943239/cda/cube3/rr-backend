import { NextFunction, Request, Response } from "express"

import AuthService from "../../../business/services/auth/AuthService"
import AuthModel from "../../../business/services/auth/models/AuthModel"
import IAuthOutput from "../../../business/services/auth/models/IAuthOutput"
import ILoginInput from "../../../business/services/auth/models/ILoginInput"
import IRegisterInput from "../../../business/services/auth/models/IRegisterInput"
import ApiError from "../../errors/ApiError"

class AuthController {
    private authService: AuthService = new AuthService()
    
    constructor() {}

    async register(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const input: IRegisterInput = { ...req.body }
            const response: IAuthOutput = await this.authService.register(input)
            if(!response.token) { throw ApiError.internal() }
            if(response) { res.status(200).json({ data: response }) }
        } catch(err: any) {
            next(err)
        }
    }

    async login(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const input: ILoginInput = { ...req.body }
            const response: IAuthOutput = await this.authService.login(input)
            if(!response.token) { throw ApiError.internal() }
            if(response) { res.status(200).json({ data: response })}
        } catch(err: any) {
            next(err)
        } 
    }
}

export default AuthController