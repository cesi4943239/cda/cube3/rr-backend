interface UserData {
    id?: number
    firstName: string
    lastName: string
    mail: string
    password?: string
    role?: string
    commune: string
}

export default UserData