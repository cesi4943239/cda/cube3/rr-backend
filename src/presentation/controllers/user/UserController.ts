import { NextFunction, Request, Response } from "express"

import UserModel from "../../../business/services/user/models/UserModel"
import UserService from "../../../business/services/user/UserService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"

class UserController implements BaseController<UserModel> {
    private userService: UserService = new UserService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const users: UserModel[] | undefined = await this.userService.getAll()
            if(!users) { throw ApiError.notFound() }
            if(users.length === 0) { throw ApiError.noContent() }
            if(users) { res.status(200).json({ data: users }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default UserController