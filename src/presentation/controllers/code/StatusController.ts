import { NextFunction, Request, Response } from "express"

import CodeModel from "../../../business/services/code/CodeModel"
import StatusService from "../../../business/services/code/StatusService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"

class StatusController implements BaseController<CodeModel> {
    private StatusService: StatusService = new StatusService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const Statuss: CodeModel[] | undefined = await this.StatusService.getAll()
            if(!Statuss) { throw ApiError.notFound() }
            if(Statuss.length === 0) { throw ApiError.noContent() }
            if(Statuss) { res.status(200).json({ data: Statuss }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default StatusController