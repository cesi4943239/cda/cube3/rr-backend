import { NextFunction, Request, Response } from "express"

import CodeModel from "../../../business/services/code/CodeModel"
import ResourceTypeService from "../../../business/services/code/ResourceTypeService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"

class ResourceTypeController implements BaseController<CodeModel> {
    private ResourceTypeService: ResourceTypeService = new ResourceTypeService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const ResourceTypes: CodeModel[] | undefined = await this.ResourceTypeService.getAll()
            if(!ResourceTypes) { throw ApiError.notFound() }
            if(ResourceTypes.length === 0) { throw ApiError.noContent() }
            if(ResourceTypes) { res.status(200).json({ data: ResourceTypes }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default ResourceTypeController