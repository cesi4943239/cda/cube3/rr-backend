import { NextFunction, Request, Response } from "express"

import CodeModel from "../../../business/services/code/CodeModel"
import ResourceCategoryService from "../../../business/services/code/ResourceCategoryService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"

class ResourceCategoryController implements BaseController<CodeModel> {
    private ResourceCategoryService: ResourceCategoryService = new ResourceCategoryService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const ResourceCategorys: CodeModel[] | undefined = await this.ResourceCategoryService.getAll()
            if(!ResourceCategorys) { throw ApiError.notFound() }
            if(ResourceCategorys.length === 0) { throw ApiError.noContent() }
            if(ResourceCategorys) { res.status(200).json({ data: ResourceCategorys }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default ResourceCategoryController