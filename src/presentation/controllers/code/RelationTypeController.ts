import { NextFunction, Request, Response } from "express"

import CodeModel from "../../../business/services/code/CodeModel"
import RelationTypeService from "../../../business/services/code/RelationTypeService"
import ApiError from "../../errors/ApiError"
import BaseController from "../BaseController"

class RelationTypeController implements BaseController<CodeModel> {
    private RelationTypeService: RelationTypeService = new RelationTypeService()
    
    constructor() {}

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const RelationTypes: CodeModel[] | undefined = await this.RelationTypeService.getAll()
            if(!RelationTypes) { throw ApiError.notFound() }
            if(RelationTypes.length === 0) { throw ApiError.noContent() }
            if(RelationTypes) { res.status(200).json({ data: RelationTypes }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default RelationTypeController