import { NextFunction, Request, Response } from "express"

import CommuneRepository from "../../../data-access/repositories/CommuneRepository";
import ApiError from "../../errors/ApiError";

class CommuneController {
    private CommuneService: CommuneRepository = new CommuneRepository()
    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const limit = req?.query?.limit ? parseInt(req?.query?.limit.toString()) : undefined
            const filter = req?.query?.filter ? req?.query?.filter.toString() : undefined
            const communes = await this.CommuneService.getAll(limit, filter)
            if(!communes) { throw ApiError.notFound() }
            if(communes.length === 0) { throw ApiError.noContent() }
            if(communes) { res.status(200).json({ data: communes.map(c => {return {zipCode: c.zipCode, town: c.town}}), limit: communes.length }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default CommuneController