import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import ResourceController from "../controllers/resource/ResourceController"
import ErrorsHandler from "../errors/ErrorsHandler"
import ResourceValidators from "../validators/ResourceValidators"

class ResourceRoute implements BaseRoute {

    public router: Router
    private ResourceController: ResourceController = new ResourceController()
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private ResourceValidators: ResourceValidators = new ResourceValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
     * @openapi
     * '/resources':
     *   get:
     *     tags:
     *       - Resource Controller
     *     summary: Retrieve resources
     *     description: Retrieve a list of resources based on provided filters.
     *     parameters:
     *       - name: limit
     *         in: query
     *         description: The maximum number of resources to return.
     *         required: false
     *         schema:
     *           type: integer
     *           default: 30
     *       - name: page
     *         in: query
     *         description: The page number of the resource set to return.
     *         required: false
     *         schema:
     *           type: integer
     *           default: 1
     *       - name: authors
     *         in: query
     *         description: The authors of the resources. (separated by ';')
     *         required: false
     *         schema:
     *           type: string
     *       - name: search
     *         in: query
     *         description: The search term.
     *         required: false
     *         schema:
     *           type: string
     *       - name: ressourceTypes
     *         in: query
     *         description: The types of resource. (separated by ';')
     *         required: false
     *         schema:
     *           type: string
     *       - name: ressourceCategories
     *         in: query
     *         description: The categories of resource. (separated by ';')
     *         required: false
     *         schema:
     *           type: string
     *       - name: relationTypes
     *         in: query
     *         description: The relation types of resource. (separated by ';')
     *         required: false
     *         schema:
     *           type: string
     *       - name: status
     *         in: query
     *         description: The status of resource. (separated by ';')
     *         required: false
     *         schema:
     *           type: string
     *       - name: orderBy
     *         in: query
     *         description: The field to order resources by.
     *         required: false
     *         schema:
     *           type: string
     *           enum: [creationDate, likeCount, exploitedCount]
     *       - name: sort
     *         in: query
     *         description: The sort order, either ascending or descending.
     *         required: false
     *         schema:
     *           type: string
     *           enum: [asc, desc]
     *     responses:
     *       200:
     *         description: Fetched Successfully
     *       400:
     *         description: Bad Request
     *       404:
     *         description: Not Found
     *       500:
     *         description: Server Error
     */
        this.router.get('/',
        this.ResourceValidators.getAllValidators(),
        this.ResourceValidators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)

        /**
         * @openapi
         * '/resources/{id}':
         *   get:
         *     tags:
         *       - Resource Controller
         *     summary: Get resource by id
         *     description: Retrieve a resource by its ID.
         *     parameters:
         *       - in: path
         *         name: id
         *         required: true
         *         description: ID of the resource to retrieve
         *         schema:
         *           type: integer
         *           format: int64
         *     responses:
         *       200:
         *         description: Fetched Successfully
         *       400:
         *         description: Bad Request
         *       404:
         *         description: Not Found
         *       500:
         *         description: Server Error
         */
        this.router.get('/:id',
        this.ResourceValidators.getById(),
        this.ResourceValidators.validatorsHandler,
        this.getById,
        this.errorsHandler.errorsHandler)

        /**
 * @openapi
 * securitySchemes:
 *   bearerAuth:
 *     type: http
 *     scheme: bearer
 * '/resources':
 *   post:
 *     security:
 *       - bearerAuth: []
 *     tags:
 *       - Resource Controller
 *     summary: Create a new Resource
 *     description: Create a new resource using the provided data.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: The title of the resource.
 *               content:
 *                 type: string
 *                 description: The content of the resource.
 *               resourceCategory:
 *                 type: object
 *                 properties:
 *                   code:
 *                     type: string
 *                 description: The category of the resource.
 *               resourceType:
 *                 type: object
 *                 properties:
 *                   code:
 *                     type: string
 *                 description: The type of the resource.
 *               relationTypes:
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     code:
 *                       type: string
 *                 description: The relation types of the resource.
 *     responses:
 *       201:
 *         description: Resource created successfully
 *       400:
 *         description: Bad Request
 *       500:
 *         description: Server Error
 */

        this.router.post('/',
        // this.ResourceValidators.getById(),
        // this.ResourceValidators.validatorsHandler,
        this.create,
        this.errorsHandler.errorsHandler)

            /**
 * @openapi
 * '/resources/demo':
 *   post:
 *     tags:
 *       - Resource Controller
 *     summary: Create a new Resource
 *     description: Create a new resource using the provided data.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *                 description: The title of the resource.
 *               content:
 *                 type: string
 *                 description: The content of the resource.
 *               resourceCategory:
 *                 type: object
 *                 properties:
 *                   code:
 *                     type: string
 *                 description: The category of the resource.
 *               resourceType:
 *                 type: object
 *                 properties:
 *                   code:
 *                     type: string
 *                 description: The type of the resource.
 *               relationTypes:
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     code:
 *                       type: string
 *                 description: The relation types of the resource.
 *     responses:
 *       201:
 *         description: Resource created successfully
 *       400:
 *         description: Bad Request
 *       500:
 *         description: Server Error
 */

            this.router.post('/demo',
            // this.ResourceValidators.getById(),
            // this.ResourceValidators.validatorsHandler,
            this.createTest,
            this.errorsHandler.errorsHandler)
        }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.ResourceController.getAll(req, res, next)
    }
    
    getById = (req: Request, res: Response, next: NextFunction): void => {
        this.ResourceController.getById(req, res, next)
    }

    create = (req: Request, res: Response, next: NextFunction): void => {
        this.ResourceController.create(req, res, next)
    }

    createTest = (req: Request, res: Response, next: NextFunction): void => {
        this.ResourceController.createTest(req, res, next)
    }
}

export default ResourceRoute