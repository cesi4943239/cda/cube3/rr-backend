import { Router } from "express"

interface BaseRoute {
    router: Router
    initializeRoutes(): void
}

export default BaseRoute