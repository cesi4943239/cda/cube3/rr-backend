import express, { NextFunction, Request, Response, Router } from 'express'
import swaggerJsdoc from 'swagger-jsdoc'
import swaggerUI from 'swagger-ui-express'

import BaseRoute from "./BaseRoute";

class SwaggerRoute implements BaseRoute {
    public router: Router
    private swaggerSpec!: any
    private options: any = {
        definition: {
          openapi: '3.0.0',
          info: {
            title: 'RR backend',
            description: "",
            version: '0.0.1',
          },
          servers: [
            {
              url: "http://localhost:3005/",
              description: "Local server"
            },
          ]
        },
        apis: [
          'src/presentation/routes/*'
        ],
      }

    constructor() {
        this.swaggerSpec = swaggerJsdoc(this.options)
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void{
        this.router.use(
        '/',
        swaggerUI.serve,
        swaggerUI.setup(this.swaggerSpec)
        )
    }
}

export default SwaggerRoute