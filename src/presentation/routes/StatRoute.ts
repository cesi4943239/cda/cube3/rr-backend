import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import StatController from "../controllers/stats/StatController"
import ErrorsHandler from "../errors/ErrorsHandler"
import CodeValidators from "../validators/CodeValidators"

class StatRoute implements BaseRoute {

    public router: Router
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private controller: StatController = new StatController()
    private validators: CodeValidators = new CodeValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
 * @openapi
 * '/stats/users':
 *   get:
 *     tags:
 *       - Stat Controller
 *     summary: Get user stats
 *     description: Retrieve the number of users
 *     parameters:
 *       - name: role
 *         in: query
 *         description: Role filter
 *         required: false
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Fetched Successfully
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Server Error
 */
        this.router.get('/users',
        this.validators.getAllValidators(),
        this.validators.validatorsHandler,
        this.countUsers,
        this.errorsHandler.errorsHandler)

    /**
 * @openapi
 * '/stats/resources':
 *   get:
 *     tags:
 *       - Stat Controller
 *     summary: Get resources stats
 *     description: Retrieve the number of resources
 *     parameters:
 *       - name: status
 *         in: query
 *         description: Status filter
 *         required: false
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Fetched Successfully
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Server Error
 */
    this.router.get('/resources',
    this.validators.getAllValidators(),
    this.validators.validatorsHandler,
    this.countResources,
    this.errorsHandler.errorsHandler)
    }

    countUsers = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.countUsers(req, res, next)
    }

    countResources = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.countResources(req, res, next)
    }
}

export default StatRoute