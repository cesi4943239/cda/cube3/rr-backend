import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import AuthController from "../controllers/auth/AuthController"
import CommentController from "../controllers/comment/CommentController"
import ErrorsHandler from "../errors/ErrorsHandler"
import RootValidators from "../validators/RootValidators"

class CommentRoute implements BaseRoute {

    public router: Router
    private commentController: CommentController = new CommentController()
    // private rootValidators: RootValidators = new RootValidators()
    private errorHandler: ErrorsHandler = new ErrorsHandler()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
        /**
         * @openapi
         * '/comment/{id}':
         *   get:
         *     tags:
         *       - Comment Controller
         *     summary: Retrieve comments by resource ID
         *     description: Retrieves comments posted on a specific resource based on its id. The returned comments also include associated responses.
         *     parameters:
         *       - in: path
         *         name: id
         *         required: true
         *         description: ID of the resource to retrieve
         *         schema:
         *           type: integer
         *           format: int64
         *     responses:
         *       200:
         *         description: Fetched Successfully
         *       400:
         *         description: Bad Request
         *       404:
         *         description: Not Found
         *       500:
         *         description: Server Error
         */
        this.router.get(
            '/:id',
            // this.rootValidators.loginValidators(),
            // this.rootValidators.validatorsHandler,
            this.getByUser,
            this.errorHandler.errorsHandler
        )
    }

    getByUser = (req: Request, res: Response, next: NextFunction): void => {
        this.commentController.getByUser(req, res, next)
    }
}

export default CommentRoute