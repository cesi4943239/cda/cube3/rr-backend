import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import StatusController from "../controllers/code/StatusController"
import ErrorsHandler from "../errors/ErrorsHandler"
import CodeValidators from "../validators/CodeValidators"

class StatusRoute implements BaseRoute {

    public router: Router
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private controller: StatusController = new StatusController()
    private validators: CodeValidators = new CodeValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
     * @openapi
     * '/status':
     *   get:
     *     tags:
     *       - Resource Types Controller
     *     summary: Get all status
     *     description: Retrieve a list of all status.
     *     responses:
     *       200:
     *         description: Fetched Successfully
     *       400:
     *         description: Bad Request
     *       404:
     *         description: Not Found
     *       500:
     *         description: Server Error
     */
        this.router.get('/',
        this.validators.getAllValidators(),
        this.validators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)
    }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.getAll(req, res, next)
    }
}

export default StatusRoute