import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import AuthController from "../controllers/auth/AuthController"
import ErrorsHandler from "../errors/ErrorsHandler"
import RootValidators from "../validators/RootValidators"

class RootRoute implements BaseRoute {

    public router: Router
    private authController: AuthController = new AuthController()
    private rootValidators: RootValidators = new RootValidators()
    private errorHandler: ErrorsHandler = new ErrorsHandler()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
        /**
         * @openapi
         * /register:
         *   post:
         *     tags:
         *       - Auth Controller
         *     summary: Register a new basic user
         *     description: Register a new basic user and return a JWT token.
         *                  Upon successful registration, a basic user account will be created.
         *                  The JWT token can be used for authentication in subsequent requests
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/registerInput'
         *     responses:
         *       200:
         *         description: User registered successfully
         *       400:
         *         description: Bad request, missing or invalid body
         *       409:
         *         description: Conflit occured, user already exists
         *       500:
         *         description: Internal server error
         *
         * components:
         *   schemas:
         *     registerInput:
         *       type: object
         *       required:
         *         - lastName
         *         - firstName
         *         - mail
         *         - password
         *       properties:
         *         lastName:
         *           type: string
         *         firstName:
         *           type: string
         *         mail:
         *           type: string
         *           format: email
         *         password:
         *           type: string
         *           format: password
         */
        this.router.post(
            '/register',
            this.rootValidators.registerValidators(),
            this.rootValidators.validatorsHandler,
            this.register,
            this.errorHandler.errorsHandler
        ),

        /**
         * @openapi
         * /login:
         *   post:
         *     tags:
         *       - Auth Controller
         *     summary: User Login
         *     description: Authenticate user with provided credentials. Returns a JWT token upon successful authentication.
         *     requestBody:
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             $ref: '#/components/schemas/loginInput'
         *     responses:
         *       200:
         *         description: Fetched Successfully
         *       400:
         *         description: Bad Request, missing or invalid body
         *       401:
         *         description: Unauthorized
         *       404:
         *         description: Not Found
         *       500:
         *         description: Server Error
         *
         * components:
         *   schemas:
         *     loginInput:
         *       type: object
         *       properties:
         *         mail:
         *           type: string
         *           format: email
         *         password:
         *           type: string
         *           format: password
         */
        this.router.post(
            '/login',
            this.rootValidators.loginValidators(),
            this.rootValidators.validatorsHandler,
            this.login,
            this.errorHandler.errorsHandler
        )
    }

    register = (req: Request, res: Response, next: NextFunction): void => {
        this.authController.register(req, res, next)
    }

    login = (req: Request, res: Response, next: NextFunction): void => {
        this.authController.login(req, res, next)
    }
}

export default RootRoute