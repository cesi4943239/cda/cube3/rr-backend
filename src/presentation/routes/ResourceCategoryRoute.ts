import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import ResourceCategoryController from "../controllers/code/ResourceCategoryController"
import ErrorsHandler from "../errors/ErrorsHandler"
import CodeValidators from "../validators/CodeValidators"

class ResourceCategoryRoute implements BaseRoute {

    public router: Router
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private controller: ResourceCategoryController = new ResourceCategoryController()
    private validators: CodeValidators = new CodeValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
     * @openapi
     * '/resourcecategories':
     *   get:
     *     tags:
     *       - Resource Types Controller
     *     summary: Get all resource categories
     *     description: Retrieve a list of all resource categories.
     *     responses:
     *       200:
     *         description: Fetched Successfully
     *       400:
     *         description: Bad Request
     *       404:
     *         description: Not Found
     *       500:
     *         description: Server Error
     */
        this.router.get('/',
        this.validators.getAllValidators(),
        this.validators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)
    }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.getAll(req, res, next)
    }
}

export default ResourceCategoryRoute