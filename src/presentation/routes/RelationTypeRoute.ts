import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import RelationTypeController from "../controllers/code/RelationTypeController"
import ErrorsHandler from "../errors/ErrorsHandler"
import CodeValidators from "../validators/CodeValidators"

class RelationTypeRoute implements BaseRoute {

    public router: Router
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private controller: RelationTypeController = new RelationTypeController()
    private validators: CodeValidators = new CodeValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
     * @openapi
     * '/relationtypes':
     *   get:
     *     tags:
     *       - Resource Types Controller
     *     summary: Get all relation types
     *     description: Retrieve a list of all relation types.
     *     responses:
     *       200:
     *         description: Fetched Successfully
     *       400:
     *         description: Bad Request
     *       404:
     *         description: Not Found
     *       500:
     *         description: Server Error
     */
        this.router.get('/',
        this.validators.getAllValidators(),
        this.validators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)
    }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.getAll(req, res, next)
    }
}

export default RelationTypeRoute