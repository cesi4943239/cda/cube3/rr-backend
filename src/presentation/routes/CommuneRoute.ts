import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import CommuneController from "../controllers/commune/CommuneController"
import ErrorsHandler from "../errors/ErrorsHandler"
import CodeValidators from "../validators/CodeValidators"

class CommuneRoute implements BaseRoute {

    public router: Router
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private controller: CommuneController = new CommuneController()
    private validators: CodeValidators = new CodeValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
 * @openapi
 * '/towns':
 *   get:
 *     tags:
 *       - Town Controller
 *     summary: Get all towns
 *     description: Retrieve a list of all towns.
 *     parameters:
 *       - name: limit
 *         in: query
 *         description: The maximum number of towns to return.
 *         required: false
 *         schema:
 *           type: integer
 *           default: 100
 *       - name: filter
 *         in: query
 *         description: The filter on towns
 *         required: false
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Fetched Successfully
 *       400:
 *         description: Bad Request
 *       404:
 *         description: Not Found
 *       500:
 *         description: Server Error
 */
        this.router.get('/',
        this.validators.getAllValidators(),
        this.validators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)
    }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.controller.getAll(req, res, next)
    }
}

export default CommuneRoute