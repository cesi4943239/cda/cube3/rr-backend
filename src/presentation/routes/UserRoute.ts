import express, { NextFunction, Request, Response, Router } from 'express'

import BaseRoute from "./BaseRoute"
import UserController from "../controllers/user/UserController"
import ErrorsHandler from "../errors/ErrorsHandler"
import UserValidators from "../validators/UserValidators"

class UserRoute implements BaseRoute {

    public router: Router
    private userController: UserController = new UserController()
    private errorsHandler: ErrorsHandler = new ErrorsHandler()
    private userValidators: UserValidators = new UserValidators()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }
    
    initializeRoutes(): void {
    /**
     * @openapi
     * '/user':
     *   get:
     *     tags:
     *       - User Controller
     *     summary: Get all users
     *     description: Retrieve a list of all users.
     *     responses:
     *       200:
     *         description: Fetched Successfully
     *       400:
     *         description: Bad Request
     *       404:
     *         description: Not Found
     *       500:
     *         description: Server Error
     */
        this.router.get('/',
        this.userValidators.getAllValidators(),
        this.userValidators.validatorsHandler,
        this.getAll,
        this.errorsHandler.errorsHandler)
    }

    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.userController.getAll(req, res, next)
    }
}

export default UserRoute