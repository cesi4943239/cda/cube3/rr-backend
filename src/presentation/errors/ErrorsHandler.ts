import { NextFunction, Request, Response } from "express";

import ApiError from "./ApiError";

class ErrorsHandler {
    errorsHandler(err: Error, req: Request, res: Response, next: NextFunction): void {
        if(err instanceof ApiError) {
            res.status(err.code).json({ errors: err.message })
            next(err)
        }
        
        if(err.name === 'SequelizeUniqueConstraintError') {
            res.status(ApiError.conflit().code).json({ errors: ApiError.conflit().message })
            next(err)
        }
        
        res.status(500).json({ errors: ApiError.internal().message })
        next(err)
    }
}

export default ErrorsHandler