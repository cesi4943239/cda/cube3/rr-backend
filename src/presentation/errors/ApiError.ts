class ApiError extends Error{

    constructor(public code: number, public message: string) { super() }

    static badRequest(message: string = DefaultErrorMessage.BadRequest): ApiError {
        return new ApiError(400, message)
    }

    static notFound(message: string = DefaultErrorMessage.NotFound): ApiError {
        return new ApiError(404, message)
    }

    static internal(message: string = DefaultErrorMessage.Internal): ApiError {
        return new ApiError(500, message)
    }

    static unauthorized(message: string = DefaultErrorMessage.Unauthorized): ApiError {
        return new ApiError(401, message);
    }
    
    static forbidden(message: string = DefaultErrorMessage.Forbidden): ApiError {
        return new ApiError(403, message);
    }

    static noContent(message: string = DefaultErrorMessage.NoContent): ApiError {
        return new ApiError(204, message);
    }

    static conflit(message: string = DefaultErrorMessage.Conflit): ApiError {
        return new ApiError(409, message);
    }
}

enum DefaultErrorMessage {
    BadRequest = 'Bad request',
    NotFound = 'Resource not found',
    Internal = 'Internal server error',
    Unauthorized = "Unauthorized",
    Forbidden = "Forbidden",
    NoContent = "No content to display",
    Conflit = "Conflit occured"
}

export default ApiError