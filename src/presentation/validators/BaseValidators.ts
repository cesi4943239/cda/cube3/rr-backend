import { NextFunction, Request, Response } from 'express'
import { validationResult } from 'express-validator'

abstract class BaseValidators {
    validatorsHandler(req: Request, res: Response, next: NextFunction): any {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            const errorsMessages = errors.array().map((err) => err.msg)
            return res.status(400).json({ errors: errorsMessages })
        }
        next()
    }
}

export default BaseValidators