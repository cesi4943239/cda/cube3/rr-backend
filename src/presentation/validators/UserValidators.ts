import { body, param, ValidationChain } from "express-validator";

import BaseValidators from "./BaseValidators";

class UserValidators extends BaseValidators {
    getAllValidators(): ValidationChain[] {
        return [
            body().custom((value) => {
                if(Object.keys(value).length > 0) { return false }
                return true
            }).withMessage('Invalid request: no body expected for this endpoint')
        ]
    }
}

export default UserValidators