import { body, ValidationChain } from "express-validator";

import BaseValidators from "./BaseValidators";

class CodeValidators extends BaseValidators {
    getAllValidators(): ValidationChain[] {
        return [
            body().custom((value) => {
                if(Object.keys(value).length > 0) { return false }
                return true
            }).withMessage('Invalid request: no body expected for this endpoint')
        ]
    }
}

export default CodeValidators