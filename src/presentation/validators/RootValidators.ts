import { body, param, ValidationChain } from "express-validator";
import jwt  from "jsonwebtoken";

import BaseValidators from "./BaseValidators";

class RootValidators extends BaseValidators {
    registerValidators(): ValidationChain[] {
        return [
            body('firstName')
            .notEmpty().withMessage('First name is required')
            .bail()
            .isString().withMessage('First name must be a string'),
            
            body('lastName')
            .notEmpty().withMessage('Last name is required')
            .bail()
            .isString().withMessage('Last name must be a string'),
            
            body('mail')
            .notEmpty().withMessage('Mail is required')
            .bail()
            .isEmail().withMessage('Invalid email address'),
            
            body('password')
            .notEmpty().withMessage('Password is required')
            .bail()
            .isString().withMessage('Password must be a string'),
            
            // body('idTown')
            // .notEmpty().withMessage('idTown is required')
            // .bail()
            // .isNumeric().withMessage('idTown must be a number'),
        ]
    }

    loginValidators(): ValidationChain[] {
        return [
            // body('token')
            // .custom((value) => {
            //     const decodedToken: any = jwt.decode(value)
            //     if(!decodedToken) { return false }
            //     return decodedToken.role === 'UTILISATEUR' ? true : false
            // })
            // .withMessage('Not authorized: role must be UTILISATEUR'),

            body('mail')
            .notEmpty().withMessage('Mail is required')
            .bail()
            .isEmail().withMessage('Invalid email address'),
            
            body('password')
            .notEmpty().withMessage('Password is required')
            .bail()
            .isString().withMessage('Password must be a string'),
        ]
    }
}

export default RootValidators