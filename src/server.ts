import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'

import Database from './data-access/db/Database'
import CommentRoute from './presentation/routes/CommentRoute'
import CommuneRoute from './presentation/routes/CommuneRoute'
import RelationTypeRoute from './presentation/routes/RelationTypeRoute'
import ResourceCategoryRoute from './presentation/routes/ResourceCategoryRoute'
import ResourceRoute from './presentation/routes/ResourceRoute'
import ResourceTypeRoute from './presentation/routes/ResourceTypeRoute'
import RootRoute from './presentation/routes/RootRoute'
import StatRoute from './presentation/routes/StatRoute'
import StatusRoute from './presentation/routes/StatusRoute'
import SwaggerRoute from './presentation/routes/SwaggerRoute'
import UserRoute from './presentation/routes/UserRoute'
import config from '../config'

const app = express()
app.use(bodyParser.json())
app.use(cors({
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'X-Requested-With', 'Content', 'Accept']
  }))

Database.initDb()

app.use('/', new RootRoute().router)
app.use('/user', new UserRoute().router)
app.use('/resources', new ResourceRoute().router)
app.use('/resourcetypes', new ResourceTypeRoute().router)
app.use('/resourcecategories', new ResourceCategoryRoute().router)
app.use('/relationtypes', new RelationTypeRoute().router)
app.use('/status', new StatusRoute().router)
app.use('/comment', new CommentRoute().router)
app.use('/docs', new SwaggerRoute().router)
app.use('/towns', new CommuneRoute().router)
app.use('/stats', new StatRoute().router)

const port = config.dev.api_port
app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})

export default app
