import { Op } from "sequelize";

import CommuneDTO from "./DTO/CommuneDTO";
import { Commune } from "../models/Commune";

class CommuneRepository {
    async getAll(limit?: number, filter?: string) {
        if (!filter) filter = ''
        const communes = await Commune.findAll({
            limit: limit,
            where: { $Ville$: {[Op.like]: filter + '%'}}
        })

        return communes.map(c => new CommuneDTO(c))
    }
}

export default CommuneRepository