import IBaseDTO from "./IBaseDTO"

abstract class CodeDTO<TDTO> implements IBaseDTO<TDTO> {
    private _code!: string
    public get code(): string {
        return this._code
    }
    public set code(value: string) {
        this._code = value
    }
    private _caption!: string
    public get caption(): string {
        return this._caption
    }
    public set caption(value: string) {
        this._caption = value
    }

    constructor(model?: any | undefined) {
        
        if (!model) return
        this.code = model.Code
        this.caption = model.Libelle
    }
    abstract mapToModel(): Promise<TDTO | undefined>
}

export default CodeDTO