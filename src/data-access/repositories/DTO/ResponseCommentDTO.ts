import UserDTO from "./UserDTO"
import { Utilisateur } from "../../models/Utilisateur"

class ResponseCommentDTO {
    public id?: number
    public dateCreation?: Date
    public contenu?: string
    public estActif?: number
    public utilisateur?: UserDTO 
}

export default ResponseCommentDTO