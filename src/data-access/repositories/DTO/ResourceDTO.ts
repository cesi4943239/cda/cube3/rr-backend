import IBaseDTO from "./IBaseDTO"
import RelationTypeDTO from "./RelationTypeDTO"
import ResourceCategoryDTO from "./ResourceCategoryDTO"
import ResourceTypeDTO from "./ResourceTypeDTO"
import StatusDTO from "./StatusDTO"
import UserDTO from "./UserDTO"
import ApiError from "../../../presentation/errors/ApiError"
import { CategorieRessource } from "../../models/CategorieRessource"
import { LienTypeRelation } from "../../models/LienTypeRelation"
import { Ressource } from "../../models/Ressource"
import { Statut } from "../../models/Statut"
import { TypeRelation } from "../../models/TypeRelation"
import { TypeRessource } from "../../models/TypeRessource"
import { Utilisateur } from "../../models/Utilisateur"

class ResourceDTO implements IBaseDTO<Ressource> {

    private _id!: number
    private _title!: string
    private _creationDate!: Date
    private _content!: string
    private _status!: StatusDTO
    private _author!: UserDTO
    private _resourceCategory!: ResourceCategoryDTO
    private _resourceType!: ResourceTypeDTO
    private _relationTypes!: RelationTypeDTO[]
    private _likeCount!: number
    private _savedCount!: number
    private _exploitedCount!: number
    private _nonExploitedCount!: number
    

    constructor(model?: Ressource) {
        if (!model) return

        this.id = model.ID
        this.title = model.Titre
        this.creationDate = model.DateCreation
        this.content = model.Contenu
        this.status = new StatusDTO(model.IDStatut_Statut)
        this.author = new UserDTO(model.IDAuteur_Utilisateur)
        this.resourceCategory = new ResourceCategoryDTO(model.IDCategorieRessource_CategorieRessource)
        this.resourceType = new ResourceTypeDTO(model.IDTypeRessource_TypeRessource)
        this.relationTypes = model.IDTypeRelation_TypeRelations?.map(typeRelation => new RelationTypeDTO(typeRelation))
        this.likeCount = model.IDUtilisateur_Utilisateur_RessourceFavoris.length
        this.savedCount = model.IDUtilisateur_Utilisateur_RessourceEnregistres.length
        this.exploitedCount = model.IDUtilisateur_Utilisateur_RessourceExploites.length
        this.nonExploitedCount = model.IDUtilisateur_Utilisateur_RessourceNonExploites.length
    }

    public async mapToModel(): Promise<Ressource | undefined> {
        try {
            const [status, author, resourceCategory, resourceType] = await Promise.all([
                await Statut.findOne({ where: { Code: this.status.code }, attributes: ['ID'] }),
                await Utilisateur.findOne({ where: { Email: this.author.getMail }, attributes: ['ID'] }),
                await CategorieRessource.findOne({ where: { Code: this.resourceCategory.code }, attributes: ['ID'] }),
                await TypeRessource.findOne({ where: { Code: this.resourceType.code }, attributes: ['ID'] })
            ])
            if(!status?.ID || !author?.ID || !resourceCategory?.ID || !resourceType?.ID) { throw ApiError.internal() }

            const resource: Ressource | null = Ressource.build({
                ID: this.id,
                Titre: this.title,
                DateCreation: this.creationDate,
                Contenu: this.content,
                IDStatut: status.ID,
                IDAuteur: author.ID,
                IDCategorieRessource: resourceCategory.ID,
                IDTypeRessource: resourceType.ID
            })

            return resource
        } catch(err: any) {
            throw err as Error
        }
    }

    public async mapToRelationTypes(idRessource: number): Promise<LienTypeRelation[] | undefined> {
        const lienRelationTypes = await Promise.all(this.relationTypes.map(async rt => {
            const typeRelation = await TypeRelation.findOne({ where: { Code: rt.code }, attributes: ['ID'] })
            if (!typeRelation) return null
            const lienTypeRelation = new LienTypeRelation()
            lienTypeRelation.IDRessource = idRessource
            lienTypeRelation.IDTypeRelation = typeRelation?.ID
            return lienTypeRelation
        }))

        return lienRelationTypes.filter((item): item is LienTypeRelation => item !== null);
    }

    public get status(): StatusDTO {
        return this._status
    }
    public set status(value: StatusDTO) {
        this._status = value
    }
    public get author(): UserDTO {
        return this._author
    }
    public set author(value: UserDTO) {
        this._author = value
    }
    public get resourceCategory(): ResourceCategoryDTO {
        return this._resourceCategory
    }
    public set resourceCategory(value: ResourceCategoryDTO) {
        this._resourceCategory = value
    }
    public get resourceType(): ResourceTypeDTO {
        return this._resourceType
    }
    public set resourceType(value: ResourceTypeDTO) {
        this._resourceType = value
    }
    public get relationTypes(): RelationTypeDTO[] {
        return this._relationTypes
    }
    public set relationTypes(value: RelationTypeDTO[]) {
        this._relationTypes = value
    }
    public get id(): number {
        return this._id
    }
    public set id(value: number) {
        this._id = value
    }
    public get title(): string {
        return this._title
    }
    public set title(value: string) {
        this._title = value
    }
    public get creationDate(): Date {
        return this._creationDate
    }
    public set creationDate(value: Date) {
        this._creationDate = value
    }
    public get content(): string {
        return this._content
    }
    public set content(value: string) {
        this._content = value
    }
    public get likeCount(): number {
        return this._likeCount
    }
    public set likeCount(value: number) {
        this._likeCount = value
    }
    public get savedCount(): number {
        return this._savedCount
    }
    public set savedCount(value: number) {
        this._savedCount = value
    }
    public get exploitedCount(): number {
        return this._exploitedCount
    }
    public set exploitedCount(value: number) {
        this._exploitedCount = value
    }
    public get nonExploitedCount(): number {
        return this._nonExploitedCount
    }
    public set nonExploitedCount(value: number) {
        this._nonExploitedCount = value
    }
}

export default ResourceDTO