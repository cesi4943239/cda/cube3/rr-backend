import UserDTO from "./UserDTO"
import { Utilisateur } from "../../models/Utilisateur"

class CommentDTO {
    public id?: number
    public dateCreation?: Date
    public contenu?: string
    public estActif?: number
    public utilisateur?: UserDTO
    public idResource?: number
    public reponseCommentaires?: CommentDTO[] 
}

export default CommentDTO