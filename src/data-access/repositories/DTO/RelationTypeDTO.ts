import CodeDTO from "./CodeDTO"
import { TypeRelation } from "../../models/TypeRelation"

class RelationTypeDTO extends CodeDTO<TypeRelation> {

    public async mapToModel(): Promise<TypeRelation | undefined> {
        try {
            const ResourceCategory: TypeRelation | null = TypeRelation.build({
                Code: this.code,
                Libelle: this.caption
            })
            return ResourceCategory
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default RelationTypeDTO