import CodeDTO from "./CodeDTO"
import { TypeRessource } from "../../models/TypeRessource"

class ResourceTypeDTO extends CodeDTO<TypeRessource> {
    public async mapToModel(): Promise<TypeRessource | undefined> {
        try {
            const resourceType: TypeRessource | null = TypeRessource.build({
                Code: this.code,
                Libelle: this.caption
            })
            return resourceType
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default ResourceTypeDTO