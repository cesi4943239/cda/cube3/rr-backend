import CodeDTO from "./CodeDTO"
import { CategorieRessource } from "../../models/CategorieRessource"

class ResourceCategoryDTO extends CodeDTO<CategorieRessource> {
    public async mapToModel(): Promise<CategorieRessource | undefined> {
        try {
            const ResourceCategory: CategorieRessource | null = CategorieRessource.build({
                Code: this.code,
                Libelle: this.caption
            })
            return ResourceCategory
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default ResourceCategoryDTO