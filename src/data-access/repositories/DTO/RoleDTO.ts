import CodeDTO from "./CodeDTO"
import { Role } from "../../models/Role"

class RoleDTO extends CodeDTO<Role> {

    public async mapToModel(): Promise<Role | undefined> {
        try {
            const role: Role | null = Role.build({
                Code: this.code,
                Libelle: this.caption
            })
            return role
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default RoleDTO