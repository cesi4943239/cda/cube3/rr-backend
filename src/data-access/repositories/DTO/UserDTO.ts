import CommuneDTO from "./CommuneDTO"
import IBaseDTO from "./IBaseDTO"
import RoleDTO from "./RoleDTO"
import ApiError from "../../../presentation/errors/ApiError"
import { Commune } from "../../models/Commune"
import { Role } from "../../models/Role"
import { Utilisateur } from "../../models/Utilisateur"

class UserDTO implements IBaseDTO<Utilisateur> {
    
    private id!: number
    private lastName!: string
    private firstName!: string
    private mail!: string
    private password!: string
    private isActive!: number
    private role!: RoleDTO
    private commune!: CommuneDTO
    private idCommune!: number

    constructor(model?: Utilisateur | undefined) {
        if (!model) return
        
        this.id = model.ID
        this.lastName = model.Nom
        this.firstName = model.Prenom
        this.mail = model.Email
        this.password = model.MotDePasse
        this.isActive = model.EstActif
        this.role = new RoleDTO(model.IDRole_Role)
        this.commune = new CommuneDTO(model.IDCommune_Commune)
    }

    public async mapToModel() : Promise<Utilisateur | undefined> {
        try {
            const idRole = (await Role.findOne({ where: { Code: this.getRole.code }, attributes: ['ID'] }))?.ID
            const idCommune = this.idCommune ? this.idCommune :
                (await Commune.findOne({ where: { CodePostal: this.getCommune.zipCode, Ville: this.getCommune.town }, attributes: ['ID'] }))?.ID
            
            if(!idRole || !idCommune) { throw ApiError.internal() }

            const user: Utilisateur | null = Utilisateur.build({
                Nom: this.getLastName,
                Prenom: this.getFirstName,
                Email: this.getMail,
                MotDePasse: this.getPassword,
                EstActif: this.getIsActive,
                IDRole: idRole,
                IDCommune: idCommune
            })

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    // Getters

    get getFirstName(): string{
        return this.firstName;
    }

    get getLastName(): string {
        return this.lastName;
    }

    get getMail(): string{
        return this.mail;
    }

    get getPassword(): string{
        return this.password;
    }

    get getIsActive(): number{
        return this.isActive;
    }

    get getId(): number{
        return this.id;
    }

    get getCommune(): CommuneDTO{
        return this.commune;
    }

    get getIDCommune(): number{
        return this.idCommune;
    }

    get getRole(): RoleDTO{
        return this.role;
    }

    // Setters
    set setFirstName(value: string) {
        this.firstName = value;
    }

    set setLastName(value: string) {
        this.lastName = value;
    }

    set setMail(value: string) {
        this.mail = value;
    }

    set setPassword(value: string) {
        this.password = value;
    }

    set setIsActive(value: number) {
        this.isActive = value;
    }

    set setId(value: number) {
        this.id = value;
    }

    set setCommune(value: CommuneDTO) {
        this.commune = value;
    }

    set setRole(value: RoleDTO) {
        this.role = value;
    }

    set setIDCommune(value: number) {
        this.idCommune = value;
    }
}

export default UserDTO