import CodeDTO from "./CodeDTO"
import { Statut } from "../../models/Statut"

class StatusDTO extends CodeDTO<Statut> {
    
    public async mapToModel(): Promise<Statut | undefined> {
        try {
            const status: Statut | null = Statut.build({
                Code: this.code,
                Libelle: this.caption
            })
            return status
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default StatusDTO