interface IBaseDTO<TDTO> {
    mapToModel() : Promise<TDTO | undefined>
}

export default IBaseDTO