import IBaseDTO from "./IBaseDTO";
import { Commune } from "../../models/Commune";

class CommuneDTO implements IBaseDTO<Commune> {

    private _zipCode!: string;
    private _town!: string;

    public get zipCode(): string {
        return this._zipCode;
    }
    public set zipCode(value: string) {
        this._zipCode = value;
    }
    public get town(): string {
        return this._town;
    }
    public set town(value: string) {
        this._town = value;
    }

    constructor(model?: Commune) {
        
        if (!model) return
        this.zipCode = model.CodePostal
        this.town = model.Ville
    }

    public async mapToModel(): Promise<Commune | undefined> {
        try {
            const commune: Commune | null = Commune.build({
                CodePostal: this.zipCode,
                Ville: this.town
            })
            return commune
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default CommuneDTO