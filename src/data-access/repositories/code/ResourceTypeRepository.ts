import { FindOptions } from "sequelize"

import ApiError from "../../../presentation/errors/ApiError"
import { TypeRessource } from "../../models/TypeRessource"
import BaseRepository from "../BaseRepository"
import ResourceTypeDTO from "../DTO/ResourceTypeDTO"


class ResourceTypeRepository implements BaseRepository<ResourceTypeDTO, TypeRessource>{
    
    //#region Data transform
    
    public toDTO(ResourceType: any): ResourceTypeDTO {
        const dto: ResourceTypeDTO = new ResourceTypeDTO()
        dto.code = ResourceType.Code
        dto.caption = ResourceType.Libelle
        return dto
    }

    public async toModel(ResourceTypeDTO: ResourceTypeDTO): Promise<TypeRessource | undefined> {
        try {
            const ResourceType: TypeRessource | null = TypeRessource.build({
                Code: ResourceTypeDTO.code,
                Libelle: ResourceTypeDTO.caption
            })
            return ResourceType
        } catch (err: any) {
            throw err as Error
        }
    }

    //#endregion

    //#region Options commands

    getFindOptions() : FindOptions<TypeRessource> {
        const options: FindOptions<TypeRessource> = {
            raw: true,
            attributes: [
                'Code',
                'Libelle'
            ]
        }
        return options
    }

    private getFindOptionsWhere(code: string) : FindOptions<TypeRessource> {
        const options = this.getFindOptions()
        options.where = { Code: code }
        return options
    }

    //#endregion

    async getAll(): Promise<ResourceTypeDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const ResourceTypes: ResourceTypeDTO[] | undefined = await TypeRessource.findAll(options)
                .then((datas: any[] | null): ResourceTypeDTO[] | undefined => {
                    return datas ? datas.map((data: any) => this.toDTO({ ...data })) : undefined
                })
            if (!ResourceTypes) { throw ApiError.notFound() }

            return ResourceTypes
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByCode(code: string): Promise<ResourceTypeDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const ResourceType: ResourceTypeDTO | undefined = await TypeRessource.findOne(options)
                .then((ResourceType: any | null): ResourceTypeDTO | undefined => {
                    return ResourceType ? this.toDTO({ ...ResourceType }) : undefined
                })
            if (!ResourceType) { throw ApiError.notFound() }

            return ResourceType
        } catch (err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<ResourceTypeDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const user: ResourceTypeDTO | undefined = await TypeRessource.findByPk(id, options)
            .then((user: any | null): ResourceTypeDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(ResourceTypeDTO: ResourceTypeDTO): Promise<ResourceTypeDTO | undefined> {
        try {
            const ResourceTypeModel: TypeRessource | undefined = await this.toModel(ResourceTypeDTO)
            if (!ResourceTypeModel) { throw ApiError.internal() }
            await ResourceTypeModel.save()

            const options = this.getFindOptionsWhere(ResourceTypeModel.Code)

            const createdResourceType: ResourceTypeDTO | undefined = await TypeRessource.findOne(options)
                .then((ResourceType: any | null): ResourceTypeDTO | undefined => { return ResourceType ? this.toDTO({ ...ResourceType }) : undefined })
            if (!createdResourceType) { throw ApiError.internal() }

            return createdResourceType
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(ResourceTypeDTO: ResourceTypeDTO): Promise<ResourceTypeDTO | undefined> {
        try {
            const ResourceTypeModel: TypeRessource | undefined = await this.toModel(ResourceTypeDTO)
            if (!ResourceTypeModel) { throw ApiError.internal() }

            await TypeRessource.update({
                Code: ResourceTypeModel.Code,
                Libelle: ResourceTypeModel.Libelle
            },
            { where: { Code: ResourceTypeModel.Code } })

            const options = this.getFindOptionsWhere(ResourceTypeModel.Code)

            const updatedResourceType: ResourceTypeDTO | undefined = await TypeRessource.findOne(options)
                .then((ResourceType: any | null): ResourceTypeDTO | undefined => { return ResourceType ? this.toDTO({ ...ResourceType }) : undefined })
            if (!updatedResourceType) { throw ApiError.internal() }

            return updatedResourceType
        } catch (err: any) {
            throw err as Error
        }
    }

    async delete(code: string): Promise<ResourceTypeDTO | undefined> {
        try {
            const options = this.getFindOptionsWhere(code)

            const ResourceTypeDTO: ResourceTypeDTO | undefined = await TypeRessource.findOne(options)
                .then((ResourceType: any | null): ResourceTypeDTO | undefined => {
                    return ResourceType ? this.toDTO({ ...ResourceType }) : undefined
                })
            if (!ResourceTypeDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await TypeRessource.destroy({ where: { Code: ResourceTypeDTO.code } })
            if (deletedRowsCount == 0) { ApiError.internal() }

            return ResourceTypeDTO
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default ResourceTypeRepository