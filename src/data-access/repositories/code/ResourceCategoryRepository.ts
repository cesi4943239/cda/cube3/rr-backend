import { FindOptions } from "sequelize"

import ApiError from "../../../presentation/errors/ApiError"
import { CategorieRessource } from "../../models/CategorieRessource"
import BaseRepository from "../BaseRepository"
import ResourceCategoryDTO from "../DTO/ResourceCategoryDTO"


class ResourceCategoryRepository implements BaseRepository<ResourceCategoryDTO, CategorieRessource>{
    
    //#region Data transform
    
    public toDTO(ResourceCategory: any): ResourceCategoryDTO {
        const dto: ResourceCategoryDTO = new ResourceCategoryDTO()
        dto.code = ResourceCategory.Code
        dto.caption = ResourceCategory.Libelle
        return dto
    }

    //#endregion

    //#region Options commands

    getFindOptions() : FindOptions<CategorieRessource> {
        const options: FindOptions<CategorieRessource> = {
            raw: true,
            attributes: [
                'Code',
                'Libelle'
            ]
        }
        return options
    }

    private getFindOptionsWhere(code: string) : FindOptions<CategorieRessource> {
        const options = this.getFindOptions()
        options.where = { Code: code }
        return options
    }

    //#endregion

    async getAll(): Promise<ResourceCategoryDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const ResourceCategorys: ResourceCategoryDTO[] | undefined = await CategorieRessource.findAll(options)
                .then((datas: any[] | null): ResourceCategoryDTO[] | undefined => {
                    return datas ? datas.map((data: any) => this.toDTO({ ...data })) : undefined
                })
            if (!ResourceCategorys) { throw ApiError.notFound() }

            return ResourceCategorys
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByCode(code: string): Promise<ResourceCategoryDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const ResourceCategory: ResourceCategoryDTO | undefined = await CategorieRessource.findOne(options)
                .then((ResourceCategory: any | null): ResourceCategoryDTO | undefined => {
                    return ResourceCategory ? this.toDTO({ ...ResourceCategory }) : undefined
                })
            if (!ResourceCategory) { throw ApiError.notFound() }

            return ResourceCategory
        } catch (err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<ResourceCategoryDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const user: ResourceCategoryDTO | undefined = await CategorieRessource.findByPk(id, options)
            .then((user: any | null): ResourceCategoryDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(ResourceCategoryDTO: ResourceCategoryDTO): Promise<ResourceCategoryDTO | undefined> {
        try {
            const ResourceCategoryModel: CategorieRessource | undefined = await ResourceCategoryDTO.mapToModel()
            if (!ResourceCategoryModel) { throw ApiError.internal() }
            await ResourceCategoryModel.save()

            const options = this.getFindOptionsWhere(ResourceCategoryModel.Code)

            const createdResourceCategory: ResourceCategoryDTO | undefined = await CategorieRessource.findOne(options)
                .then((ResourceCategory: any | null): ResourceCategoryDTO | undefined => { return ResourceCategory ? this.toDTO({ ...ResourceCategory }) : undefined })
            if (!createdResourceCategory) { throw ApiError.internal() }

            return createdResourceCategory
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(ResourceCategoryDTO: ResourceCategoryDTO): Promise<ResourceCategoryDTO | undefined> {
        try {
            const ResourceCategoryModel: CategorieRessource | undefined = await ResourceCategoryDTO.mapToModel()
            if (!ResourceCategoryModel) { throw ApiError.internal() }

            await CategorieRessource.update({
                Code: ResourceCategoryModel.Code,
                Libelle: ResourceCategoryModel.Libelle
            },
            { where: { Code: ResourceCategoryModel.Code } })

            const options = this.getFindOptionsWhere(ResourceCategoryModel.Code)

            const updatedResourceCategory: ResourceCategoryDTO | undefined = await CategorieRessource.findOne(options)
                .then((ResourceCategory: any | null): ResourceCategoryDTO | undefined => { return ResourceCategory ? this.toDTO({ ...ResourceCategory }) : undefined })
            if (!updatedResourceCategory) { throw ApiError.internal() }

            return updatedResourceCategory
        } catch (err: any) {
            throw err as Error
        }
    }

    async delete(code: string): Promise<ResourceCategoryDTO | undefined> {
        try {
            const options = this.getFindOptionsWhere(code)

            const ResourceCategoryDTO: ResourceCategoryDTO | undefined = await CategorieRessource.findOne(options)
                .then((ResourceCategory: any | null): ResourceCategoryDTO | undefined => {
                    return ResourceCategory ? this.toDTO({ ...ResourceCategory }) : undefined
                })
            if (!ResourceCategoryDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await CategorieRessource.destroy({ where: { Code: ResourceCategoryDTO.code } })
            if (deletedRowsCount == 0) { ApiError.internal() }

            return ResourceCategoryDTO
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default ResourceCategoryRepository