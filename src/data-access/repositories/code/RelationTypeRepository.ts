import { FindOptions } from "sequelize"

import ApiError from "../../../presentation/errors/ApiError"
import { TypeRelation } from "../../models/TypeRelation"
import BaseRepository from "../BaseRepository"
import RelationTypeDTO from "../DTO/RelationTypeDTO"


class RelationTypeRepository implements BaseRepository<RelationTypeDTO, TypeRelation>{
    
    //#region Data transform
    
    public async toModel(RelationTypeDTO: RelationTypeDTO): Promise<TypeRelation | undefined> {
        try {
            const RelationType: TypeRelation | null = TypeRelation.build({
                Code: RelationTypeDTO.code,
                Libelle: RelationTypeDTO.caption
            })
            return RelationType
        } catch (err: any) {
            throw err as Error
        }
    }

    //#endregion

    //#region Options commands

    getFindOptions() : FindOptions<TypeRelation> {
        const options: FindOptions<TypeRelation> = {
            raw: true,
            attributes: [
                'Code',
                'Libelle'
            ]
        }
        return options
    }

    private getFindOptionsWhere(code: string) : FindOptions<TypeRelation> {
        const options = this.getFindOptions()
        options.where = { Code: code }
        return options
    }

    //#endregion

    async getAll(): Promise<RelationTypeDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const RelationTypes: RelationTypeDTO[] | undefined = await TypeRelation.findAll(options)
                .then((datas: any[] | null): RelationTypeDTO[] | undefined => {
                    return datas ? datas.map((data: any) => new RelationTypeDTO({ ...data })) : undefined
                })
            if (!RelationTypes) { throw ApiError.notFound() }

            return RelationTypes
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByCode(code: string): Promise<RelationTypeDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const relationType: RelationTypeDTO | undefined = await TypeRelation.findOne(options)
                .then((RelationType: any | null): RelationTypeDTO | undefined => {
                    return RelationType ? new RelationTypeDTO({ ...RelationType }) : undefined
                })
            if (!relationType) { throw ApiError.notFound() }

            return relationType
        } catch (err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<RelationTypeDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const user: RelationTypeDTO | undefined = await TypeRelation.findByPk(id, options)
            .then((user: any | null): RelationTypeDTO | undefined => {
                return user ? new RelationTypeDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(relationTypeDTO: RelationTypeDTO): Promise<RelationTypeDTO | undefined> {
        try {
            const RelationTypeModel: TypeRelation | undefined = await this.toModel(relationTypeDTO)
            if (!RelationTypeModel) { throw ApiError.internal() }
            await RelationTypeModel.save()

            const options = this.getFindOptionsWhere(RelationTypeModel.Code)

            const createdRelationType: RelationTypeDTO | undefined = await TypeRelation.findOne(options)
                .then((RelationType: any | null): RelationTypeDTO | undefined => { return RelationType ? new RelationTypeDTO({ ...RelationType }) : undefined })
            if (!createdRelationType) { throw ApiError.internal() }

            return createdRelationType
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(relationTypeDTO: RelationTypeDTO): Promise<RelationTypeDTO | undefined> {
        try {
            const RelationTypeModel: TypeRelation | undefined = await this.toModel(relationTypeDTO)
            if (!RelationTypeModel) { throw ApiError.internal() }

            await TypeRelation.update({
                Code: RelationTypeModel.Code,
                Libelle: RelationTypeModel.Libelle
            },
            { where: { Code: RelationTypeModel.Code } })

            const options = this.getFindOptionsWhere(RelationTypeModel.Code)

            const updatedRelationType: RelationTypeDTO | undefined = await TypeRelation.findOne(options)
                .then((RelationType: any | null): RelationTypeDTO | undefined => { return RelationType ? new RelationTypeDTO({ ...RelationType }) : undefined })
            if (!updatedRelationType) { throw ApiError.internal() }

            return updatedRelationType
        } catch (err: any) {
            throw err as Error
        }
    }

    async delete(code: string): Promise<RelationTypeDTO | undefined> {
        try {
            const options = this.getFindOptionsWhere(code)

            const relationTypeDTO: RelationTypeDTO | undefined = await TypeRelation.findOne(options)
                .then((RelationType: any | null): RelationTypeDTO | undefined => {
                    return RelationType ? new RelationTypeDTO({ ...RelationType }) : undefined
                })
            if (!relationTypeDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await TypeRelation.destroy({ where: { Code: relationTypeDTO.code } })
            if (deletedRowsCount == 0) { ApiError.internal() }

            return relationTypeDTO
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default RelationTypeRepository