import { FindOptions } from "sequelize"

import ApiError from "../../../presentation/errors/ApiError"
import { Statut } from "../../models/Statut"
import BaseRepository from "../BaseRepository"
import StatusDTO from "../DTO/StatusDTO"


class StatusRepository implements BaseRepository<StatusDTO, Statut>{
    
    //#region Data transform
    
    public toDTO(Status: any): StatusDTO {
        const dto: StatusDTO = new StatusDTO()
        dto.code = Status.Code
        dto.caption = Status.Libelle
        return dto
    }

    public async toModel(StatusDTO: StatusDTO): Promise<Statut | undefined> {
        try {
            const Status: Statut | null = Statut.build({
                Code: StatusDTO.code,
                Libelle: StatusDTO.caption
            })
            return Status
        } catch (err: any) {
            throw err as Error
        }
    }

    //#endregion

    //#region Options commands

    getFindOptions() : FindOptions<Statut> {
        const options: FindOptions<Statut> = {
            raw: true,
            attributes: [
                'Code',
                'Libelle'
            ]
        }
        return options
    }

    private getFindOptionsWhere(code: string) : FindOptions<Statut> {
        const options = this.getFindOptions()
        options.where = { Code: code }
        return options
    }

    //#endregion

    async getAll(): Promise<StatusDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const Statuss: StatusDTO[] | undefined = await Statut.findAll(options)
                .then((datas: any[] | null): StatusDTO[] | undefined => {
                    return datas ? datas.map((data: any) => this.toDTO({ ...data })) : undefined
                })
            if (!Statuss) { throw ApiError.notFound() }

            return Statuss
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByCode(code: string): Promise<StatusDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const Status: StatusDTO | undefined = await Statut.findOne(options)
                .then((Status: any | null): StatusDTO | undefined => {
                    return Status ? this.toDTO({ ...Status }) : undefined
                })
            if (!Status) { throw ApiError.notFound() }

            return Status
        } catch (err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<StatusDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const user: StatusDTO | undefined = await Statut.findByPk(id, options)
            .then((user: any | null): StatusDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(StatusDTO: StatusDTO): Promise<StatusDTO | undefined> {
        try {
            const StatusModel: Statut | undefined = await this.toModel(StatusDTO)
            if (!StatusModel) { throw ApiError.internal() }
            await StatusModel.save()

            const options = this.getFindOptionsWhere(StatusModel.Code)

            const createdStatus: StatusDTO | undefined = await Statut.findOne(options)
                .then((Status: any | null): StatusDTO | undefined => { return Status ? this.toDTO({ ...Status }) : undefined })
            if (!createdStatus) { throw ApiError.internal() }

            return createdStatus
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(StatusDTO: StatusDTO): Promise<StatusDTO | undefined> {
        try {
            const StatusModel: Statut | undefined = await this.toModel(StatusDTO)
            if (!StatusModel) { throw ApiError.internal() }

            await Statut.update({
                Code: StatusModel.Code,
                Libelle: StatusModel.Libelle
            },
            { where: { Code: StatusModel.Code } })

            const options = this.getFindOptionsWhere(StatusModel.Code)

            const updatedStatus: StatusDTO | undefined = await Statut.findOne(options)
                .then((Status: any | null): StatusDTO | undefined => { return Status ? this.toDTO({ ...Status }) : undefined })
            if (!updatedStatus) { throw ApiError.internal() }

            return updatedStatus
        } catch (err: any) {
            throw err as Error
        }
    }

    async delete(code: string): Promise<StatusDTO | undefined> {
        try {
            const options = this.getFindOptionsWhere(code)

            const StatusDTO: StatusDTO | undefined = await Statut.findOne(options)
                .then((Status: any | null): StatusDTO | undefined => {
                    return Status ? this.toDTO({ ...Status }) : undefined
                })
            if (!StatusDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await Statut.destroy({ where: { Code: StatusDTO.code } })
            if (deletedRowsCount == 0) { ApiError.internal() }

            return StatusDTO
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default StatusRepository