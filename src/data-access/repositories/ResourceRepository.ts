import { FindOptions, literal, Op } from "sequelize"

import BaseRepository from "./BaseRepository"
import ResourceDTO from "./DTO/ResourceDTO"
import FilterOptions from "./FilterOptions"
import ApiError from "../../presentation/errors/ApiError"
import { CategorieRessource } from "../models/CategorieRessource"
import { LienTypeRelation } from "../models/LienTypeRelation"
import { Ressource } from "../models/Ressource"
import { Statut } from "../models/Statut"
import { TypeRelation } from "../models/TypeRelation"
import { TypeRessource } from "../models/TypeRessource"
import { Utilisateur } from "../models/Utilisateur"

class ResourceRepository implements BaseRepository<ResourceDTO, Ressource>{
    
    //#region Options commands

    getFindOptions(): FindOptions<Ressource> {
        let r: Ressource = new Ressource()
        const options: FindOptions<Ressource> = {
            include: [
                {
                    model: CategorieRessource,
                    as: 'IDCategorieRessource_CategorieRessource'
                },
                {
                    model: TypeRessource,
                    as: 'IDTypeRessource_TypeRessource'
                },
                {
                    model: Utilisateur,
                    as: 'IDAuteur_Utilisateur'
                },
                {
                    model: TypeRelation,
                    as: 'IDTypeRelation_TypeRelations'
                },
                {
                    model: Statut,
                    as: 'IDStatut_Statut'
                },
                {
                    model: Utilisateur,
                    as: 'IDUtilisateur_Utilisateur_RessourceFavoris',
                    include: []
                },
                {
                    model: Utilisateur,
                    as: 'IDUtilisateur_Utilisateur_RessourceEnregistres',
                    include: []
                },
                {
                    model: Utilisateur,
                    as: 'IDUtilisateur_Utilisateur_RessourceExploites',
                    include: []
                },
                {
                    model: Utilisateur,
                    as: 'IDUtilisateur_Utilisateur_RessourceNonExploites',
                    include: []
                }
            ],
        }

        return options
    }

    getFilteredOptions(filterOptions: FilterOptions) : FindOptions<Ressource> {
        const options = this.getFindOptions()
        if (!filterOptions) return options;

        if (filterOptions.limit) {
            options.limit = parseInt(filterOptions.limit.toString());
        }
        if (filterOptions.page) {
            options.offset = filterOptions.getOffset();
        }
        if (filterOptions.authors) {
            options.where = {...options.where, '$IDAuteur_Utilisateur.Email$': {[Op.in]: filterOptions.authors} };
        }
        if (filterOptions.search) {
            options.where = {
                ...options.where,
                Titre: { [Op.like]: `%${filterOptions.search}%` }
            };
        }
        if (filterOptions.ressourceCategories) {
            options.where = {...options.where, '$IDCategorieRessource_CategorieRessource.Code$': {[Op.in]: filterOptions.ressourceCategories}};
        }
        if (filterOptions.ressourceTypes) {
            options.where = {...options.where, '$IDTypeRessource_TypeRessource.Code$': {[Op.in]: filterOptions.ressourceTypes} };
        }
        if (filterOptions.relationTypes) {
            options.where = {...options.where, '$IDTypeRelation_TypeRelations.Code$': {[Op.in]: filterOptions.relationTypes} };
        }
        if (filterOptions.status) {
            options.where = {...options.where, '$IDStatut_Statut.Code$': {[Op.in]: filterOptions.status} };
        }
        if (filterOptions.orderByField && filterOptions.sort) {
            const orderDirection: 'ASC' | 'DESC' = filterOptions.sort.toLowerCase() === 'asc' ? 'ASC' : 'DESC';
            const orderColumn = filterOptions.orderByField;
            options.order = [[literal(orderColumn), orderDirection]];
        }
        options.subQuery = false

        return options;
    }

    //#endregion

    async getAll(filterOptions: FilterOptions): Promise<ResourceDTO[] | undefined> {
        const options = this.getFilteredOptions(filterOptions)
        try {
            const Resources: ResourceDTO[] | undefined = await Ressource.findAll(options)
            .then((datas: any[] | null): ResourceDTO[] | undefined => {
                return datas ? datas.map((data: any) => new ResourceDTO(data)) : undefined
            })
            if(!Resources) { throw ApiError.notFound() }

            return Resources
        } catch(err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<ResourceDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const Resource: ResourceDTO | undefined = await Ressource.findByPk(id, options)
            .then((Resource: any | null): ResourceDTO | undefined => {
                return Resource ? new ResourceDTO(Resource) : undefined
            })
            if(!Resource) { throw ApiError.notFound() }

            return Resource
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(resourceDTO: ResourceDTO): Promise<ResourceDTO | undefined> {
        try {
            const ResourceModel: Ressource | undefined = await resourceDTO.mapToModel()
            if(!ResourceModel) { throw ApiError.internal() }
            
            const createdResource = await ResourceModel.save()
            const lienRelationTypes: LienTypeRelation[] | undefined = await resourceDTO.mapToRelationTypes(createdResource.ID)
            if (lienRelationTypes) {
                await Promise.all(lienRelationTypes.map(async rt => await rt.save()))
            }

            const responseDTO = await this.getById(createdResource.ID)
            return responseDTO
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(resourceDTO: ResourceDTO): Promise<ResourceDTO | undefined> {
        try {
            const ResourceModel: Ressource | undefined = await resourceDTO.mapToModel()
            if(!ResourceModel) { throw ApiError.internal() }

            await Ressource.update({
                Titre: ResourceModel.Titre,
                DateCreation: ResourceModel.DateCreation,
                Contenu: ResourceModel.Contenu,
                IDStatut: ResourceModel.IDStatut,
                IDAuteur: ResourceModel.IDAuteur,
                IDCategorieRessource: ResourceModel.IDCategorieRessource,
                IDTypeRessource: ResourceModel.IDTypeRessource
            }, 
            { where: { ID: resourceDTO.id }})

            const updatedResource: ResourceDTO | undefined = await Ressource.findByPk(resourceDTO.id)
            .then((Resource: any | null): ResourceDTO | undefined => { return Resource ? new ResourceDTO(Resource) : undefined })
            if(!updatedResource) { throw ApiError.internal() }

            return updatedResource
        } catch (err: any) {
            throw err as Error
        }
    }
    
    async delete(id: number): Promise<ResourceDTO | undefined> {
        try {
            const resourceDTO: ResourceDTO | undefined = await Ressource.findByPk(id)
            .then((Resource: any | null): ResourceDTO | undefined => {
                return Resource ? new ResourceDTO(Resource) : undefined
            })
            if(!resourceDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await Ressource.destroy({ where: { ID: id }})
            if(deletedRowsCount == 0) { ApiError.internal() }

            return resourceDTO
        } catch (err: any) {
            throw err as Error
        }
    }
}

export default ResourceRepository