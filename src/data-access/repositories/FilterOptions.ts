class FilterOptions {
    public orderByField?: string;
    public sort?: string;
    public limit?: number;
    public page?: number;
    public authors?: string[];
    public search?: string;

    public ressourceCategories?: string[];
    public ressourceTypes?: string[];
    public relationTypes?: string[];
    public status?: string[];

    public getOffset() : number | undefined {
        if (!this.page || !this.limit)  return
        return (this.page - 1) * this.limit;
    }

    constructor() {}
}

export default FilterOptions