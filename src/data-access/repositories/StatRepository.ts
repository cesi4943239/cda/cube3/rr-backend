import { Ressource } from "../models/Ressource";
import { Role } from "../models/Role";
import { Statut } from "../models/Statut";
import { Utilisateur } from "../models/Utilisateur";

class StatRepository {

    async countUsers(role?: string | undefined) : Promise<number> {
        if (role) {
            return await Utilisateur.count({
                include: [
                    {
                        model: Role,
                        as: 'IDRole_Role'
                    }
                ],
                where: {'$IDRole_Role.Code$': role}
            })
        }

        return await Utilisateur.count()
    }

    async countResources(status?: string | undefined) : Promise<number> {
        if (status) {
            return await Ressource.count({
                include: [
                    {
                        model: Statut,
                        as: 'IDStatut_Statut'
                    }
                ],
                where: {'$IDStatut_Statut.Code$': status}
            })
        }

        return await Ressource.count()
    }
}

export default StatRepository