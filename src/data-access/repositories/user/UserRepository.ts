import UserDTO from "./UserDTO"
import BaseRepository from "../BaseRepository"
import { Utilisateur } from "../../models/Utilisateur"
import { Role } from "../../models/Role"
import { Commune } from "../../models/Commune"
import { FindOptions, Sequelize } from "sequelize"
import ApiError from "../../../presentation/errors/ApiError"

class UserRepository implements BaseRepository<UserDTO, Utilisateur>{
    public toDTO(user: any): UserDTO {
        const dto: UserDTO = new UserDTO()
        dto.setId = user.ID
        dto.setLastName = user.Nom
        dto.setFirstName = user.Prenom
        dto.setMail = user.Email
        dto.setPassword = user.MotDePasse
        dto.setIsActive = user.EstActif
        dto.setZIPCode = user.ZIPCode
        dto.setRole = user.Role
        return dto
    }

    public async toModel(userDTO: UserDTO): Promise<Utilisateur | undefined> {
        try {
            const [role, ZIPCode] = await Promise.all([
                await Role.findOne({ where: { Code: userDTO.getRole }, attributes: ['ID'] }),
                await Commune.findOne({ where: { CodePostal: userDTO.getZIPCode }, attributes: ['ID'] })
            ])
            if(!role?.ID || !ZIPCode?.ID) { throw ApiError.internal() }

            const user: Utilisateur | null = Utilisateur.build({
                Nom: userDTO.getLastName,
                Prenom: userDTO.getFirstName,
                Email: userDTO.getMail,
                MotDePasse: userDTO.getPassword,
                EstActif: userDTO.getIsActive,
                IDRole: role?.ID,
                IDCommune: ZIPCode?.ID
            })

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async getAll(): Promise<UserDTO[] | undefined> {
        const options: Object = {
            include: [
                {
                    model: Role,
                    as: 'IDRole_Role',
                    attributes: []
                },
                {
                    model: Commune,
                    as: 'IDCommune_Commune',
                    attributes: []
                }
            ],
            raw: true,
            attributes: [
                'ID',
                'Nom',
                'Prenom',
                'Email',
                'MotDePasse',
                'EstActif',
                [Sequelize.col('IDRole_Role.Code'), 'Role'],
                [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
            ]
        }
        try {
            const users: UserDTO[] | undefined = await Utilisateur.findAll(options)
            .then((datas: any[] | null): UserDTO[] | undefined => {
                return datas ? datas.map((data: any) => this.toDTO({...data})) : undefined
            })
            if(!users) { throw ApiError.notFound() }

            return users
        } catch(err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<UserDTO | undefined> {
        const options: Object = {
            include: [
                {
                    model: Role,
                    as: 'IDRole_Role',
                    attributes: []
                },
                {
                    model: Commune,
                    as: 'IDCommune_Commune',
                    attributes: []
                }
            ],
            raw: true,
            attributes: [
                'ID',
                'Nom',
                'Prenom',
                'Email',
                'MotDePasse',
                'EstActif',
                [Sequelize.col('IDRole_Role.Code'), 'Role'],
                [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
            ]
        }
        try {
            const user: UserDTO | undefined = await Utilisateur.findByPk(id, options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(userDTO: UserDTO): Promise<UserDTO | undefined> {
        try {
            const userModel: Utilisateur | undefined = await this.toModel(userDTO)
            if(!userModel) { throw ApiError.internal() }
            await userModel.save()

            const options: Object = {
                where: { Email: userModel.Email },
                include: [
                    {
                        model: Role,
                        as: 'IDRole_Role',
                        attributes: []
                    },
                    {
                        model: Commune,
                        as: 'IDCommune_Commune',
                        attributes: []
                    }
                ],
                raw: true,
                attributes: [
                    'ID',
                    'Nom',
                    'Prenom',
                    'Email',
                    'EstActif',
                    [Sequelize.col('IDRole_Role.Code'), 'Role'],
                    [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
                ]
            }

            const createdUser: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => { return user ? this.toDTO({...user}) : undefined })
            if(!createdUser) { throw ApiError.internal() }

            return createdUser
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(userDTO: UserDTO): Promise<UserDTO | undefined> {
        try {
            const userModel: Utilisateur | undefined = await this.toModel(userDTO)
            if(!userModel) { throw ApiError.internal() }

            await Utilisateur.update({
                Nom: userModel.Nom,
                Prenom: userModel.Prenom,
                Email: userModel.Email,
                MotDePasse: userModel.MotDePasse,
                EstActif: userModel.EstActif,
                IDRole: userModel.IDRole,
                IDCommune: userModel.IDCommune
            }, 
            { where: { Email: userModel.Email }})

            const options: Object = {
                where: { Email: userModel.Email },
                include: [
                    {
                        model: Role,
                        as: 'IDRole_Role',
                        attributes: []
                    },
                    {
                        model: Commune,
                        as: 'IDCommune_Commune',
                        attributes: []
                    }
                ],
                raw: true,
                attributes: [
                    'ID',
                    'Nom',
                    'Prenom',
                    'Email',
                    'MotDePasse',
                    'EstActif',
                    [Sequelize.col('IDRole_Role.Code'), 'Role'],
                    [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
                ]
            }
            const updatedUser: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => { return user ? this.toDTO({...user}) : undefined })
            if(!updatedUser) { throw ApiError.internal() }

            return updatedUser
        } catch (err: any) {
            throw err as Error
        }
    }
    
    async delete(mail: string): Promise<UserDTO | undefined> {
        try {
            const options: Object = {
                where: { Email: mail },
                include: [
                    {
                        model: Role,
                        as: 'IDRole_Role',
                        attributes: []
                    },
                    {
                        model: Commune,
                        as: 'IDCommune_Commune',
                        attributes: []
                    }
                ],
                raw: true,
                attributes: [
                    'ID',
                    'Nom',
                    'Prenom',
                    'Email',
                    'MotDePasse',
                    'EstActif',
                    [Sequelize.col('IDRole_Role.Code'), 'Role'],
                    [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
                ]
            }
            const userDTO: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!userDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await Utilisateur.destroy({ where: { Email: userDTO.getMail }})
            if(deletedRowsCount == 0) { ApiError.internal() }

            return userDTO
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByMail(mail: string): Promise<UserDTO | undefined> {
        const options: Object = {
            where: { Email: mail },
            include: [
                {
                    model: Role,
                    as: 'IDRole_Role',
                    attributes: []
                },
                {
                    model: Commune,
                    as: 'IDCommune_Commune',
                    attributes: []
                }
            ],
            raw: true,
            attributes: [
                'ID',
                'Nom',
                'Prenom',
                'Email',
                'MotDePasse',
                'EstActif',
                [Sequelize.col('IDRole_Role.Code'), 'Role'],
                [Sequelize.col('IDCommune_Commune.CodePostal'), 'ZIPCode']
            ]
        }
        try {
            const user: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? this.toDTO({...user}) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    getFindOptions(): FindOptions<Utilisateur> {
        const options: FindOptions<Utilisateur> = {
            include: [
                {
                    model: Role,
                    as: 'IDRole_Role'
                },
                {
                    model: Commune,
                    as: 'IDCommune_Commune'
                }
            ],
            attributes: [
                'ID',
                'Nom',
                'Prenom',
                'Email',
                'MotDePasse',
                'EstActif'
            ]
        }

        return options
    }
}

export default UserRepository