
class UserDTO {
    private id!: number
    private lastName!: string
    private firstName!: string
    private mail!: string
    private password!: string
    private isActive!: number
    private role!: string
    private ZIPCode!: string

    constructor(){}

    // Getters

    get getFirstName(): string{
        return this.firstName;
    }

    get getLastName(): string {
        return this.lastName;
    }

    get getMail(): string{
        return this.mail;
    }

    get getPassword(): string{
        return this.password;
    }

    get getIsActive(): number{
        return this.isActive;
    }

    get getId(): number{
        return this.id;
    }

    get getZIPCode(): string{
        return this.ZIPCode;
    }

    get getRole(): string{
        return this.role;
    }

    // Setters
    set setFirstName(value: string) {
        this.firstName = value;
    }

    set setLastName(value: string) {
        this.lastName = value;
    }

    set setMail(value: string) {
        this.mail = value;
    }

    set setPassword(value: string) {
        this.password = value;
    }

    set setIsActive(value: number) {
        this.isActive = value;
    }

    set setId(value: number) {
        this.id = value;
    }

    set setZIPCode(value: string) {
        this.ZIPCode = value;
    }

    set setRole(value: string) {
        this.role = value;
    }
}

export default UserDTO