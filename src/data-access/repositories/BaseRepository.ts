import { FindOptions } from "sequelize"

interface BaseRepository <T, U>{
    getFindOptions(): FindOptions<U>
}

export default BaseRepository