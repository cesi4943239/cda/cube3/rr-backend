import { FindOptions } from "sequelize"

import BaseRepository from "./BaseRepository"
import UserDTO from "./DTO/UserDTO"
import ApiError from "../../presentation/errors/ApiError"
import { Commune } from "../models/Commune"
import { Role } from "../models/Role"
import { Utilisateur } from "../models/Utilisateur"

class UserRepository implements BaseRepository<UserDTO, Utilisateur>{

    //#region Options commands

    getFindOptions(): FindOptions<Utilisateur> {
        const options: FindOptions<Utilisateur> = {
            include: [
                {
                    model: Role,
                    as: 'IDRole_Role'
                },
                {
                    model: Commune,
                    as: 'IDCommune_Commune'
                }
            ],
            attributes: [
                'ID',
                'Nom',
                'Prenom',
                'Email',
                'MotDePasse',
                'EstActif'
            ]
        }

        return options
    }

    private getFindOptionsWhere(email: string) {
        const options = this.getFindOptions()
        options.where = { Email: email }
        return options
    }

    //#endregion

    async getAll(): Promise<UserDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const users: UserDTO[] | undefined = await Utilisateur.findAll(options)
            .then((datas: any[] | null): UserDTO[] | undefined => {
                return datas ? datas.map((data: Utilisateur) => new UserDTO(data)) : undefined
            })
            if(!users) { throw ApiError.notFound() }

            return users
        } catch(err: any) {
            throw err as Error
        }
    }

    async getById(id: number): Promise<UserDTO | undefined> {
        const options = this.getFindOptions()
        try {
            const user: UserDTO | undefined = await Utilisateur.findByPk(id, options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? new UserDTO(user) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }

    async create(userDTO: UserDTO): Promise<UserDTO | undefined> {
        try {
            const userModel: Utilisateur | undefined = await userDTO.mapToModel()
            if(!userModel) { throw ApiError.internal() }
            await userModel.save()

            const options = this.getFindOptionsWhere(userModel.Email)

            const createdUser: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => { return user ? new UserDTO(user) : undefined })
            if(!createdUser) { throw ApiError.internal() }

            return createdUser
        } catch (err: any) {
            throw err as Error
        }
    }

    async update(userDTO: UserDTO): Promise<UserDTO | undefined> {
        try {
            const userModel: Utilisateur | undefined = await userDTO.mapToModel()
            if(!userModel) { throw ApiError.internal() }

            await Utilisateur.update({
                Nom: userModel.Nom,
                Prenom: userModel.Prenom,
                Email: userModel.Email,
                MotDePasse: userModel.MotDePasse,
                EstActif: userModel.EstActif,
                IDRole: userModel.IDRole,
                IDCommune: userModel.IDCommune
            }, 
            { where: { Email: userModel.Email }})

            const options = this.getFindOptionsWhere(userModel.Email)

            const updatedUser: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => { return user ? new UserDTO(user) : undefined })
            if(!updatedUser) { throw ApiError.internal() }

            return updatedUser
        } catch (err: any) {
            throw err as Error
        }
    }
    
    async delete(mail: string): Promise<UserDTO | undefined> {
        try {
            const options = this.getFindOptionsWhere(mail)
            const userDTO: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? new UserDTO(user) : undefined
            })
            if(!userDTO) { throw ApiError.notFound() }

            const deletedRowsCount: number = await Utilisateur.destroy({ where: { Email: userDTO.getMail }})
            if(deletedRowsCount == 0) { ApiError.internal() }

            return userDTO
        } catch (err: any) {
            throw err as Error
        }
    }

    async getByMail(mail: string): Promise<UserDTO | undefined> {
        const options = this.getFindOptionsWhere(mail)
        try {
            const user: UserDTO | undefined = await Utilisateur.findOne(options)
            .then((user: any | null): UserDTO | undefined => {
                return user ? new UserDTO(user) : undefined
            })
            if(!user) { throw ApiError.notFound() }

            return user
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default UserRepository