import { FindOptions } from "sequelize"

import BaseRepository from "./BaseRepository"
import CommentDTO from "./DTO/CommentDTO"
import ResponseCommentDTO from "./DTO/ResponseCommentDTO"
import UserDTO from "./DTO/UserDTO"
import ApiError from "../../presentation/errors/ApiError"
import { Commentaire } from "../models/Commentaire"
import { DonneesCommentaire } from "../models/DonneesCommentaire"
import { ReponseCommentaire } from "../models/ReponseCommentaire"
import { Utilisateur } from "../models/Utilisateur"

class CommentRepository implements BaseRepository<CommentDTO, Commentaire>{

    //#region Options commands

    getFindOptions(): FindOptions<Commentaire> {
        const options: FindOptions<Commentaire> = {
            include: [
                {
                    model: DonneesCommentaire,
                    as: 'ID_DonneesCommentaire',
                    include: [
                        {
                            model: Utilisateur,
                            as: 'IDUtilisateur_Utilisateur',
                            attributes: [
                                'Prenom',
                                'Nom'
                            ]
                        }
                    ]
                },
                {
                    model: ReponseCommentaire,
                    as: 'ReponseCommentaires',
                    include: [
                        {
                            model: DonneesCommentaire,
                            as: 'ID_DonneesCommentaire',
                            include: [
                                {
                                    model: Utilisateur,
                                    as: 'IDUtilisateur_Utilisateur',
                                    attributes: [
                                        'Prenom',
                                        'Nom'
                                    ]
                                }
                            ]
                        }
                    ]
                },
            ]
        }

        return options
    }

    //#endregion

    async getAll(): Promise<UserDTO[] | undefined> {
        const options = this.getFindOptions()
        try {
            const users: UserDTO[] | undefined = await Utilisateur.findAll(options)
            .then((datas: any[] | null): UserDTO[] | undefined => {
                return datas ? datas.map((data: Utilisateur) => new UserDTO(data)) : undefined
            })
            if(!users) { throw ApiError.notFound() }

            return users
        } catch(err: any) {
            throw err as Error
        }
    }

    async getByResourceId(id: number): Promise<CommentDTO[] | undefined> {
        const options = this.getFindOptions()

        options.where = { IDRessource: id }
        try {
            const commentaireSchema: Commentaire[] = await Commentaire.findAll(options)

            // console.log(commentaireSchema)
            // console.log(commentaireSchema[0].ID_DonneesCommentaire)
            const commentDTO: CommentDTO = new CommentDTO()

            const commentsDTO: CommentDTO[] =  commentaireSchema.map((comment) => {
                const commentDTO: CommentDTO = new CommentDTO()
                commentDTO.id = comment.ID
                commentDTO.idResource = comment.IDRessource
                commentDTO.utilisateur = new UserDTO()
                commentDTO.contenu = comment.ID_DonneesCommentaire.Contenu
                commentDTO.estActif = comment.ID_DonneesCommentaire.EstActif
            
                if (comment.ID_DonneesCommentaire?.IDUtilisateur_Utilisateur) {
                    commentDTO.utilisateur.setFirstName = comment.ID_DonneesCommentaire.IDUtilisateur_Utilisateur.Prenom
                    commentDTO.utilisateur.setLastName = comment.ID_DonneesCommentaire.IDUtilisateur_Utilisateur.Nom
                }
            
                const responseCommentaires: ResponseCommentDTO[] = commentDTO.reponseCommentaires = comment.ReponseCommentaires.map((value: ReponseCommentaire) => {
                    const responseCommentaire = new ResponseCommentDTO()
                    responseCommentaire.contenu = value.ID_DonneesCommentaire.Contenu
                    responseCommentaire.dateCreation = value.ID_DonneesCommentaire.DateCreation
                    responseCommentaire.estActif = value.ID_DonneesCommentaire.EstActif
                    responseCommentaire.id = value.ID_DonneesCommentaire.ID
                    responseCommentaire.utilisateur = new UserDTO()
            
                    if (value.ID_DonneesCommentaire?.IDUtilisateur_Utilisateur) {
                        responseCommentaire.utilisateur.setFirstName = value.ID_DonneesCommentaire.IDUtilisateur_Utilisateur.Prenom
                        responseCommentaire.utilisateur.setLastName = value.ID_DonneesCommentaire.IDUtilisateur_Utilisateur.Nom
                    }
            
                    return responseCommentaire
                });
                commentDTO.reponseCommentaires = responseCommentaires
                return commentDTO
            });

            return commentsDTO
        } catch(err: any) {
            throw err as Error
        }
    }
}

export default CommentRepository