import bcrypt  from "bcryptjs"
import { Sequelize } from "sequelize";

import config from "../../../config";
import AbonnementFactory from "../../../tools/mock-data/mock-factories/abonnementFactory";
import ActiviteFactory from "../../../tools/mock-data/mock-factories/activiteFactory";
import CommentaireFactory from "../../../tools/mock-data/mock-factories/commentaireFactory";
import DonnesCommentaireFactory from "../../../tools/mock-data/mock-factories/donneesCommentaireFactory";
import HistoriqueConsultationFactory from "../../../tools/mock-data/mock-factories/historiqueConsultationFactory";
import HistoriqueFactory from "../../../tools/mock-data/mock-factories/historiqueFactory";
import HistoriqueRechercheFactory from "../../../tools/mock-data/mock-factories/historiqueRechercheFactory";
import InscriptionActiviteFactory from "../../../tools/mock-data/mock-factories/inscriptionActiviteFactory";
import LienTypeRelationFactory from "../../../tools/mock-data/mock-factories/lienTypeRelationFactory";
import LikeMessageFactory from "../../../tools/mock-data/mock-factories/likeMessageFactory";
import ReponseCommentaireFactory from "../../../tools/mock-data/mock-factories/reponseCommentaireFactory";
import RessourceEnregistreFactory from "../../../tools/mock-data/mock-factories/ressourceEnregistre";
import RessourceExploiteFactory from "../../../tools/mock-data/mock-factories/ressourceExploiteFactory";
import RessourceFactory from "../../../tools/mock-data/mock-factories/ressourceFactory";
import RessourceFavorisFactory from "../../../tools/mock-data/mock-factories/ressourceFavorisFactory";
import RessourceNonExploiteFactory from "../../../tools/mock-data/mock-factories/ressourceNonExploite";
import UtilisateurFactory from "../../../tools/mock-data/mock-factories/utilisateurFactory";
import { Abonnement, Activite, Commentaire, DonneesCommentaire, Historique, HistoriqueConsultation, HistoriqueRecherche, initModels, InscriptionActivite, LienTypeRelation, LikeMessage, ReponseCommentaire, Ressource, RessourceEnregistre, RessourceExploite, RessourceFavoris, RessourceNonExploite, Utilisateur } from "../models/init-models";

class Database {
    private static sequelize: Sequelize;

    constructor() {}

    public static getInstance(): Sequelize {
        if (!this.sequelize) {
            this.sequelize = new Sequelize(
                config.db.name,
                config.db.user,
                config.db.user_pwd,
                {
                    host: config.db.host,
                    port: config.db.port,
                    dialect: 'mariadb',
                    dialectOptions: {
                        timezone: 'Etc/GMT-2',
                    },
                    logging: false
                })
        }
        return this.sequelize;
    }

    public static initDb(): void {
        initModels(Database.getInstance())
        Database.initHooksModels()
    }

    private static initHooksModels(): void {
        Utilisateur.beforeSave(async (user: Utilisateur) => {
            if(user.changed('MotDePasse')) {
                const salt: string = await bcrypt.genSalt(10)
                const cryptedPassword: string = await bcrypt.hash(user.MotDePasse, salt)
                user.MotDePasse = cryptedPassword
            }
          })
    }

    private static async generateData(): Promise<void> {
        let USERS_COUNT: number = 200
        

        const RESOURCE_COUNT: number = USERS_COUNT
        const ACTIVITIES_COUNT: number = USERS_COUNT
        const COMMENTS_COUNT: number = USERS_COUNT

        Database.getInstance()
            .query('SET FOREIGN_KEY_CHECKS = 0')
            .then(async () => {
                await Utilisateur.sync({ force: true })
                return await Utilisateur.bulkCreate(UtilisateurFactory.getResources(USERS_COUNT), {
                    fields: ['ID', 'Prenom', 'Nom', 'Email', 'MotDePasse', 'EstActif', 'IDRole', 'IDCommune']
                })
            })
            .then(async () => {
                await Abonnement.sync({ force: true })
                return await Abonnement.bulkCreate(AbonnementFactory.getResources(USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDAbonnement']
                })
            })
            .then(async () => {
                await Historique.sync({ force: true })
                return await Historique.bulkCreate(HistoriqueFactory.getResources(USERS_COUNT), {
                    fields: ['DateEvenement', 'IDUtilisateur']
                })
            })
            .then(async () => {
                await HistoriqueRecherche.sync({ force: true })
                return await HistoriqueRecherche.bulkCreate(HistoriqueRechercheFactory.getResources(USERS_COUNT), {
                    fields: ['RechercheEffectuee', 'ID']
                })
            })
            .then(async () => {
                await Ressource.sync({ force: true })
                return await Ressource.bulkCreate(RessourceFactory.getResources(USERS_COUNT, RESOURCE_COUNT), {
                    fields: ['ID', 'Titre', 'DateCreation', 'Contenu', 'IDStatut', 'IDAuteur', 'IDCategorieRessource', 'IDTypeRessource']
                })
            })
            .then(async () => {
                await HistoriqueConsultation.sync({ force: true })
                return await HistoriqueConsultation.bulkCreate(HistoriqueConsultationFactory.getResources(USERS_COUNT, RESOURCE_COUNT), {
                    fields: ['ID', 'IDRessource']
                })
            })
            .then(async () => {
                await Activite.sync({ force: true })
                return await Activite.bulkCreate(ActiviteFactory.getResources(ACTIVITIES_COUNT), {
                    fields: ['ID', 'DateDebut', 'DateFin', 'IDRessource']
                })
            })
            .then(async () => {
                await InscriptionActivite.sync({ force: true })
                return await InscriptionActivite.bulkCreate(InscriptionActiviteFactory.getResources(ACTIVITIES_COUNT), {
                    fields: ['IDUtilisateur', 'IDActivite']
                })
            })
            .then(async () => {
                await DonneesCommentaire.sync({ force: true })
                return await DonneesCommentaire.bulkCreate(DonnesCommentaireFactory.getResources(COMMENTS_COUNT), {
                    fields: ['ID', 'DateCreation', 'Contenu', 'EstActif', 'IDUtilisateur']
                })
            })
            .then(async () => {
                await ReponseCommentaire.sync({ force: true })
                return await ReponseCommentaire.bulkCreate(ReponseCommentaireFactory.getResources(Math.ceil(COMMENTS_COUNT / 2), COMMENTS_COUNT), {
                    fields: ['ID', 'ID_IDCommentaire']
                })
            })
            .then(async () => {
                await Commentaire.sync({ force: true })
                return await Commentaire.bulkCreate(CommentaireFactory.getResources(COMMENTS_COUNT, RESOURCE_COUNT), {
                    fields: ['ID', 'IDRessource']
                })
            })
            .then(async () => {
                await LikeMessage.sync({ force: true })
                return await LikeMessage.bulkCreate(LikeMessageFactory.getResources(Math.ceil(USERS_COUNT / 2), COMMENTS_COUNT, USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDCommentaire']
                })
            })
            .then(async () => {
                await RessourceNonExploite.sync({ force: true })
                return await RessourceNonExploite.bulkCreate(RessourceNonExploiteFactory.getResources(Math.ceil(USERS_COUNT / 2), COMMENTS_COUNT, USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDRessource']
                })
            })
            .then(async () => {
                await RessourceExploite.sync({ force: true })
                return await RessourceExploite.bulkCreate(RessourceExploiteFactory.getResources(Math.ceil(USERS_COUNT / 2), COMMENTS_COUNT, USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDRessource']
                })
            })
            .then(async () => {
                await RessourceEnregistre.sync({ force: true })
                return await RessourceEnregistre.bulkCreate(RessourceEnregistreFactory.getResources(Math.ceil(USERS_COUNT / 2), COMMENTS_COUNT, USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDRessource']
                })
            })
            .then(async () => {
                await RessourceFavoris.sync({ force: true })
                return await RessourceFavoris.bulkCreate(RessourceFavorisFactory.getResources(Math.ceil(USERS_COUNT / 2), COMMENTS_COUNT, USERS_COUNT), {
                    fields: ['IDUtilisateur', 'IDRessource']
                })
            })
            .then(async () => {
                await LienTypeRelation.sync({ force: true })
                return await LienTypeRelation.bulkCreate(LienTypeRelationFactory.getResources(USERS_COUNT), {
                    fields: ['IDRessource', 'IDTypeRelation']
                })
            })
            .then(() => {
                return Database.getInstance().query('SET FOREIGN_KEY_CHECKS = 1')
            })
            .then(() => {
                console.log(`The DB has been initialized with generated dataset`)
            })
            .catch((err: any) => {
                throw err as Error
            })
    }

}

export default Database