import type { DonneesCommentaire, DonneesCommentaireId } from './DonneesCommentaire';
import type { ReponseCommentaire, ReponseCommentaireId } from './ReponseCommentaire';
import type { Ressource, RessourceId } from './Ressource';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface CommentaireAttributes {
  ID: number;
  IDRessource: number;
}

export type CommentairePk = "ID";
export type CommentaireId = Commentaire[CommentairePk];
export type CommentaireCreationAttributes = CommentaireAttributes;

export class Commentaire extends Model<CommentaireAttributes, CommentaireCreationAttributes> implements CommentaireAttributes {
  ID!: number;
  IDRessource!: number;

  // Commentaire hasMany ReponseCommentaire via ID_IDCommentaire
  ReponseCommentaires!: ReponseCommentaire[];
  getReponseCommentaires!: Sequelize.HasManyGetAssociationsMixin<ReponseCommentaire>;
  setReponseCommentaires!: Sequelize.HasManySetAssociationsMixin<ReponseCommentaire, ReponseCommentaireId>;
  addReponseCommentaire!: Sequelize.HasManyAddAssociationMixin<ReponseCommentaire, ReponseCommentaireId>;
  addReponseCommentaires!: Sequelize.HasManyAddAssociationsMixin<ReponseCommentaire, ReponseCommentaireId>;
  createReponseCommentaire!: Sequelize.HasManyCreateAssociationMixin<ReponseCommentaire>;
  removeReponseCommentaire!: Sequelize.HasManyRemoveAssociationMixin<ReponseCommentaire, ReponseCommentaireId>;
  removeReponseCommentaires!: Sequelize.HasManyRemoveAssociationsMixin<ReponseCommentaire, ReponseCommentaireId>;
  hasReponseCommentaire!: Sequelize.HasManyHasAssociationMixin<ReponseCommentaire, ReponseCommentaireId>;
  hasReponseCommentaires!: Sequelize.HasManyHasAssociationsMixin<ReponseCommentaire, ReponseCommentaireId>;
  countReponseCommentaires!: Sequelize.HasManyCountAssociationsMixin;
  // Commentaire belongsTo DonneesCommentaire via ID
  ID_DonneesCommentaire!: DonneesCommentaire;
  getID_DonneesCommentaire!: Sequelize.BelongsToGetAssociationMixin<DonneesCommentaire>;
  setID_DonneesCommentaire!: Sequelize.BelongsToSetAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  createID_DonneesCommentaire!: Sequelize.BelongsToCreateAssociationMixin<DonneesCommentaire>;
  // Commentaire belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Commentaire {
    return Commentaire.init({
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'DonneesCommentaire',
        key: 'ID'
      }
    },
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Ressource',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Commentaire',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDRessource",
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
        ]
      },
    ]
  });
  }
}
