import type { AbonnementAttributes, AbonnementCreationAttributes } from "./Abonnement";
import type { ActiviteAttributes, ActiviteCreationAttributes } from "./Activite";
import type { CategorieRessourceAttributes, CategorieRessourceCreationAttributes } from "./CategorieRessource";
import type { CommentaireAttributes, CommentaireCreationAttributes } from "./Commentaire";
import type { CommuneAttributes, CommuneCreationAttributes } from "./Commune";
import type { DocumentAttributes, DocumentCreationAttributes } from "./Document";
import type { DonneesCommentaireAttributes, DonneesCommentaireCreationAttributes } from "./DonneesCommentaire";
import type { HistoriqueAttributes, HistoriqueCreationAttributes } from "./Historique";
import type { HistoriqueConsultationAttributes, HistoriqueConsultationCreationAttributes } from "./HistoriqueConsultation";
import type { HistoriqueRechercheAttributes, HistoriqueRechercheCreationAttributes } from "./HistoriqueRecherche";
import type { InscriptionActiviteAttributes, InscriptionActiviteCreationAttributes } from "./InscriptionActivite";
import type { LienTypeRelationAttributes, LienTypeRelationCreationAttributes } from "./LienTypeRelation";
import type { LikeMessageAttributes, LikeMessageCreationAttributes } from "./LikeMessage";
import type { ReponseCommentaireAttributes, ReponseCommentaireCreationAttributes } from "./ReponseCommentaire";
import type { RessourceAttributes, RessourceCreationAttributes } from "./Ressource";
import type { RessourceEnregistreAttributes, RessourceEnregistreCreationAttributes } from "./RessourceEnregistre";
import type { RessourceExploiteAttributes, RessourceExploiteCreationAttributes } from "./RessourceExploite";
import type { RessourceFavorisAttributes, RessourceFavorisCreationAttributes } from "./RessourceFavoris";
import type { RessourceNonExploiteAttributes, RessourceNonExploiteCreationAttributes } from "./RessourceNonExploite";
import type { RoleAttributes, RoleCreationAttributes } from "./Role";
import type { StatutAttributes, StatutCreationAttributes } from "./Statut";
import type { TypeRelationAttributes, TypeRelationCreationAttributes } from "./TypeRelation";
import type { TypeRessourceAttributes, TypeRessourceCreationAttributes } from "./TypeRessource";
import type { UtilisateurAttributes, UtilisateurCreationAttributes } from "./Utilisateur";
import type { Sequelize } from "sequelize";

import { Abonnement as _Abonnement } from "./Abonnement";
import { Activite as _Activite } from "./Activite";
import { CategorieRessource as _CategorieRessource } from "./CategorieRessource";
import { Commentaire as _Commentaire } from "./Commentaire";
import { Commune as _Commune } from "./Commune";
import { Document as _Document } from "./Document";
import { DonneesCommentaire as _DonneesCommentaire } from "./DonneesCommentaire";
import { Historique as _Historique } from "./Historique";
import { HistoriqueConsultation as _HistoriqueConsultation } from "./HistoriqueConsultation";
import { HistoriqueRecherche as _HistoriqueRecherche } from "./HistoriqueRecherche";
import { InscriptionActivite as _InscriptionActivite } from "./InscriptionActivite";
import { LienTypeRelation as _LienTypeRelation } from "./LienTypeRelation";
import { LikeMessage as _LikeMessage } from "./LikeMessage";
import { ReponseCommentaire as _ReponseCommentaire } from "./ReponseCommentaire";
import { Ressource as _Ressource } from "./Ressource";
import { RessourceEnregistre as _RessourceEnregistre } from "./RessourceEnregistre";
import { RessourceExploite as _RessourceExploite } from "./RessourceExploite";
import { RessourceFavoris as _RessourceFavoris } from "./RessourceFavoris";
import { RessourceNonExploite as _RessourceNonExploite } from "./RessourceNonExploite";
import { Role as _Role } from "./Role";
import { Statut as _Statut } from "./Statut";
import { TypeRelation as _TypeRelation } from "./TypeRelation";
import { TypeRessource as _TypeRessource } from "./TypeRessource";
import { Utilisateur as _Utilisateur } from "./Utilisateur";


export {
  _Abonnement as Abonnement,
  _Activite as Activite,
  _CategorieRessource as CategorieRessource,
  _Commentaire as Commentaire,
  _Commune as Commune,
  _Document as Document,
  _DonneesCommentaire as DonneesCommentaire,
  _Historique as Historique,
  _HistoriqueConsultation as HistoriqueConsultation,
  _HistoriqueRecherche as HistoriqueRecherche,
  _InscriptionActivite as InscriptionActivite,
  _LienTypeRelation as LienTypeRelation,
  _LikeMessage as LikeMessage,
  _ReponseCommentaire as ReponseCommentaire,
  _Ressource as Ressource,
  _RessourceEnregistre as RessourceEnregistre,
  _RessourceExploite as RessourceExploite,
  _RessourceFavoris as RessourceFavoris,
  _RessourceNonExploite as RessourceNonExploite,
  _Role as Role,
  _Statut as Statut,
  _TypeRelation as TypeRelation,
  _TypeRessource as TypeRessource,
  _Utilisateur as Utilisateur,
};

export type {
  AbonnementAttributes,
  AbonnementCreationAttributes,
  ActiviteAttributes,
  ActiviteCreationAttributes,
  CategorieRessourceAttributes,
  CategorieRessourceCreationAttributes,
  CommentaireAttributes,
  CommentaireCreationAttributes,
  CommuneAttributes,
  CommuneCreationAttributes,
  DocumentAttributes,
  DocumentCreationAttributes,
  DonneesCommentaireAttributes,
  DonneesCommentaireCreationAttributes,
  HistoriqueAttributes,
  HistoriqueCreationAttributes,
  HistoriqueConsultationAttributes,
  HistoriqueConsultationCreationAttributes,
  HistoriqueRechercheAttributes,
  HistoriqueRechercheCreationAttributes,
  InscriptionActiviteAttributes,
  InscriptionActiviteCreationAttributes,
  LienTypeRelationAttributes,
  LienTypeRelationCreationAttributes,
  LikeMessageAttributes,
  LikeMessageCreationAttributes,
  ReponseCommentaireAttributes,
  ReponseCommentaireCreationAttributes,
  RessourceAttributes,
  RessourceCreationAttributes,
  RessourceEnregistreAttributes,
  RessourceEnregistreCreationAttributes,
  RessourceExploiteAttributes,
  RessourceExploiteCreationAttributes,
  RessourceFavorisAttributes,
  RessourceFavorisCreationAttributes,
  RessourceNonExploiteAttributes,
  RessourceNonExploiteCreationAttributes,
  RoleAttributes,
  RoleCreationAttributes,
  StatutAttributes,
  StatutCreationAttributes,
  TypeRelationAttributes,
  TypeRelationCreationAttributes,
  TypeRessourceAttributes,
  TypeRessourceCreationAttributes,
  UtilisateurAttributes,
  UtilisateurCreationAttributes,
};

export function initModels(sequelize: Sequelize) {
  const Abonnement = _Abonnement.initModel(sequelize);
  const Activite = _Activite.initModel(sequelize);
  const CategorieRessource = _CategorieRessource.initModel(sequelize);
  const Commentaire = _Commentaire.initModel(sequelize);
  const Commune = _Commune.initModel(sequelize);
  const Document = _Document.initModel(sequelize);
  const DonneesCommentaire = _DonneesCommentaire.initModel(sequelize);
  const Historique = _Historique.initModel(sequelize);
  const HistoriqueConsultation = _HistoriqueConsultation.initModel(sequelize);
  const HistoriqueRecherche = _HistoriqueRecherche.initModel(sequelize);
  const InscriptionActivite = _InscriptionActivite.initModel(sequelize);
  const LienTypeRelation = _LienTypeRelation.initModel(sequelize);
  const LikeMessage = _LikeMessage.initModel(sequelize);
  const ReponseCommentaire = _ReponseCommentaire.initModel(sequelize);
  const Ressource = _Ressource.initModel(sequelize);
  const RessourceEnregistre = _RessourceEnregistre.initModel(sequelize);
  const RessourceExploite = _RessourceExploite.initModel(sequelize);
  const RessourceFavoris = _RessourceFavoris.initModel(sequelize);
  const RessourceNonExploite = _RessourceNonExploite.initModel(sequelize);
  const Role = _Role.initModel(sequelize);
  const Statut = _Statut.initModel(sequelize);
  const TypeRelation = _TypeRelation.initModel(sequelize);
  const TypeRessource = _TypeRessource.initModel(sequelize);
  const Utilisateur = _Utilisateur.initModel(sequelize);

  Activite.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_InscriptionActivites', through: InscriptionActivite, foreignKey: "IDActivite", otherKey: "IDUtilisateur" });
  DonneesCommentaire.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_LikeMessages', through: LikeMessage, foreignKey: "IDCommentaire", otherKey: "IDUtilisateur" });
  Ressource.belongsToMany(TypeRelation, { as: 'IDTypeRelation_TypeRelations', through: LienTypeRelation, foreignKey: "IDRessource", otherKey: "IDTypeRelation" });
  Ressource.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_RessourceEnregistres', through: RessourceEnregistre, foreignKey: "IDRessource", otherKey: "IDUtilisateur" });
  Ressource.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_RessourceExploites', through: RessourceExploite, foreignKey: "IDRessource", otherKey: "IDUtilisateur" });
  Ressource.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_RessourceFavoris', through: RessourceFavoris, foreignKey: "IDRessource", otherKey: "IDUtilisateur" });
  Ressource.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateur_RessourceNonExploites', through: RessourceNonExploite, foreignKey: "IDRessource", otherKey: "IDUtilisateur" });
  TypeRelation.belongsToMany(Ressource, { as: 'IDRessource_Ressources', through: LienTypeRelation, foreignKey: "IDTypeRelation", otherKey: "IDRessource" });
  Utilisateur.belongsToMany(Activite, { as: 'IDActivite_Activites', through: InscriptionActivite, foreignKey: "IDUtilisateur", otherKey: "IDActivite" });
  Utilisateur.belongsToMany(DonneesCommentaire, { as: 'IDCommentaire_DonneesCommentaires', through: LikeMessage, foreignKey: "IDUtilisateur", otherKey: "IDCommentaire" });
  Utilisateur.belongsToMany(Ressource, { as: 'IDRessource_Ressource_RessourceEnregistres', through: RessourceEnregistre, foreignKey: "IDUtilisateur", otherKey: "IDRessource" });
  Utilisateur.belongsToMany(Ressource, { as: 'IDRessource_Ressource_RessourceExploites', through: RessourceExploite, foreignKey: "IDUtilisateur", otherKey: "IDRessource" });
  Utilisateur.belongsToMany(Ressource, { as: 'IDRessource_Ressource_RessourceFavoris', through: RessourceFavoris, foreignKey: "IDUtilisateur", otherKey: "IDRessource" });
  Utilisateur.belongsToMany(Ressource, { as: 'IDRessource_Ressource_RessourceNonExploites', through: RessourceNonExploite, foreignKey: "IDUtilisateur", otherKey: "IDRessource" });
  Utilisateur.belongsToMany(Utilisateur, { as: 'IDAbonnement_Utilisateurs', through: Abonnement, foreignKey: "IDUtilisateur", otherKey: "IDAbonnement" });
  Utilisateur.belongsToMany(Utilisateur, { as: 'IDUtilisateur_Utilisateurs', through: Abonnement, foreignKey: "IDAbonnement", otherKey: "IDUtilisateur" });
  InscriptionActivite.belongsTo(Activite, { as: "IDActivite_Activite", foreignKey: "IDActivite"});
  Activite.hasMany(InscriptionActivite, { as: "InscriptionActivites", foreignKey: "IDActivite"});
  Ressource.belongsTo(CategorieRessource, { as: "IDCategorieRessource_CategorieRessource", foreignKey: "IDCategorieRessource"});
  CategorieRessource.hasMany(Ressource, { as: "Ressources", foreignKey: "IDCategorieRessource"});
  ReponseCommentaire.belongsTo(Commentaire, { as: "ID_IDCommentaire_Commentaire", foreignKey: "ID_IDCommentaire"});
  Commentaire.hasMany(ReponseCommentaire, { as: "ReponseCommentaires", foreignKey: "ID_IDCommentaire"});
  Utilisateur.belongsTo(Commune, { as: "IDCommune_Commune", foreignKey: "IDCommune"});
  Commune.hasMany(Utilisateur, { as: "Utilisateurs", foreignKey: "IDCommune"});
  Commentaire.belongsTo(DonneesCommentaire, { as: "ID_DonneesCommentaire", foreignKey: "ID"});
  DonneesCommentaire.hasOne(Commentaire, { as: "Commentaire", foreignKey: "ID"});
  LikeMessage.belongsTo(DonneesCommentaire, { as: "IDCommentaire_DonneesCommentaire", foreignKey: "IDCommentaire"});
  DonneesCommentaire.hasMany(LikeMessage, { as: "LikeMessages", foreignKey: "IDCommentaire"});
  ReponseCommentaire.belongsTo(DonneesCommentaire, { as: "ID_DonneesCommentaire", foreignKey: "ID"});
  DonneesCommentaire.hasOne(ReponseCommentaire, { as: "ReponseCommentaire", foreignKey: "ID"});
  HistoriqueConsultation.belongsTo(Historique, { as: "ID_Historique", foreignKey: "ID"});
  Historique.hasOne(HistoriqueConsultation, { as: "HistoriqueConsultation", foreignKey: "ID"});
  HistoriqueRecherche.belongsTo(Historique, { as: "ID_Historique", foreignKey: "ID"});
  Historique.hasOne(HistoriqueRecherche, { as: "HistoriqueRecherche", foreignKey: "ID"});
  Activite.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasOne(Activite, { as: "Activite", foreignKey: "IDRessource"});
  Commentaire.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(Commentaire, { as: "Commentaires", foreignKey: "IDRessource"});
  Document.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(Document, { as: "Documents", foreignKey: "IDRessource"});
  HistoriqueConsultation.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(HistoriqueConsultation, { as: "HistoriqueConsultations", foreignKey: "IDRessource"});
  LienTypeRelation.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(LienTypeRelation, { as: "LienTypeRelations", foreignKey: "IDRessource"});
  RessourceEnregistre.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(RessourceEnregistre, { as: "RessourceEnregistres", foreignKey: "IDRessource"});
  RessourceExploite.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(RessourceExploite, { as: "RessourceExploites", foreignKey: "IDRessource"});
  RessourceFavoris.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(RessourceFavoris, { as: "RessourceFavoris", foreignKey: "IDRessource"});
  RessourceNonExploite.belongsTo(Ressource, { as: "IDRessource_Ressource", foreignKey: "IDRessource"});
  Ressource.hasMany(RessourceNonExploite, { as: "RessourceNonExploites", foreignKey: "IDRessource"});
  Utilisateur.belongsTo(Role, { as: "IDRole_Role", foreignKey: "IDRole"});
  Role.hasMany(Utilisateur, { as: "Utilisateurs", foreignKey: "IDRole"});
  Ressource.belongsTo(Statut, { as: "IDStatut_Statut", foreignKey: "IDStatut"});
  Statut.hasMany(Ressource, { as: "Ressources", foreignKey: "IDStatut"});
  LienTypeRelation.belongsTo(TypeRelation, { as: "IDTypeRelation_TypeRelation", foreignKey: "IDTypeRelation"});
  TypeRelation.hasMany(LienTypeRelation, { as: "LienTypeRelations", foreignKey: "IDTypeRelation"});
  Ressource.belongsTo(TypeRessource, { as: "IDTypeRessource_TypeRessource", foreignKey: "IDTypeRessource"});
  TypeRessource.hasMany(Ressource, { as: "Ressources", foreignKey: "IDTypeRessource"});
  Abonnement.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(Abonnement, { as: "Abonnements", foreignKey: "IDUtilisateur"});
  Abonnement.belongsTo(Utilisateur, { as: "IDAbonnement_Utilisateur", foreignKey: "IDAbonnement"});
  Utilisateur.hasMany(Abonnement, { as: "IDAbonnement_Abonnements", foreignKey: "IDAbonnement"});
  DonneesCommentaire.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(DonneesCommentaire, { as: "DonneesCommentaires", foreignKey: "IDUtilisateur"});
  Historique.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(Historique, { as: "Historiques", foreignKey: "IDUtilisateur"});
  InscriptionActivite.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(InscriptionActivite, { as: "InscriptionActivites", foreignKey: "IDUtilisateur"});
  LikeMessage.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(LikeMessage, { as: "LikeMessages", foreignKey: "IDUtilisateur"});
  Ressource.belongsTo(Utilisateur, { as: "IDAuteur_Utilisateur", foreignKey: "IDAuteur"});
  Utilisateur.hasMany(Ressource, { as: "Ressources", foreignKey: "IDAuteur"});
  RessourceEnregistre.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(RessourceEnregistre, { as: "RessourceEnregistres", foreignKey: "IDUtilisateur"});
  RessourceExploite.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(RessourceExploite, { as: "RessourceExploites", foreignKey: "IDUtilisateur"});
  RessourceFavoris.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(RessourceFavoris, { as: "RessourceFavoris", foreignKey: "IDUtilisateur"});
  RessourceNonExploite.belongsTo(Utilisateur, { as: "IDUtilisateur_Utilisateur", foreignKey: "IDUtilisateur"});
  Utilisateur.hasMany(RessourceNonExploite, { as: "RessourceNonExploites", foreignKey: "IDUtilisateur"});

  return {
    Abonnement: Abonnement,
    Activite: Activite,
    CategorieRessource: CategorieRessource,
    Commentaire: Commentaire,
    Commune: Commune,
    Document: Document,
    DonneesCommentaire: DonneesCommentaire,
    Historique: Historique,
    HistoriqueConsultation: HistoriqueConsultation,
    HistoriqueRecherche: HistoriqueRecherche,
    InscriptionActivite: InscriptionActivite,
    LienTypeRelation: LienTypeRelation,
    LikeMessage: LikeMessage,
    ReponseCommentaire: ReponseCommentaire,
    Ressource: Ressource,
    RessourceEnregistre: RessourceEnregistre,
    RessourceExploite: RessourceExploite,
    RessourceFavoris: RessourceFavoris,
    RessourceNonExploite: RessourceNonExploite,
    Role: Role,
    Statut: Statut,
    TypeRelation: TypeRelation,
    TypeRessource: TypeRessource,
    Utilisateur: Utilisateur,
  };
}
