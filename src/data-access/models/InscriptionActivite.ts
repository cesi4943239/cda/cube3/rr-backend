import type { Activite, ActiviteId } from './Activite';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface InscriptionActiviteAttributes {
  IDUtilisateur: number;
  IDActivite: number;
}

export type InscriptionActivitePk = "IDUtilisateur" | "IDActivite";
export type InscriptionActiviteId = InscriptionActivite[InscriptionActivitePk];
export type InscriptionActiviteCreationAttributes = InscriptionActiviteAttributes;

export class InscriptionActivite extends Model<InscriptionActiviteAttributes, InscriptionActiviteCreationAttributes> implements InscriptionActiviteAttributes {
  IDUtilisateur!: number;
  IDActivite!: number;

  // InscriptionActivite belongsTo Activite via IDActivite
  IDActivite_Activite!: Activite;
  getIDActivite_Activite!: Sequelize.BelongsToGetAssociationMixin<Activite>;
  setIDActivite_Activite!: Sequelize.BelongsToSetAssociationMixin<Activite, ActiviteId>;
  createIDActivite_Activite!: Sequelize.BelongsToCreateAssociationMixin<Activite>;
  // InscriptionActivite belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof InscriptionActivite {
    return InscriptionActivite.init({
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    },
    IDActivite: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Activite',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'InscriptionActivite',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
          { name: "IDActivite" },
        ]
      },
      {
        name: "IDActivite",
        using: "BTREE",
        fields: [
          { name: "IDActivite" },
        ]
      },
    ]
  });
  }
}
