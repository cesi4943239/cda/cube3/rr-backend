import type { Ressource, RessourceId } from './Ressource';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface RessourceEnregistreAttributes {
  IDUtilisateur: number;
  IDRessource: number;
}

export type RessourceEnregistrePk = "IDUtilisateur" | "IDRessource";
export type RessourceEnregistreId = RessourceEnregistre[RessourceEnregistrePk];
export type RessourceEnregistreCreationAttributes = RessourceEnregistreAttributes;

export class RessourceEnregistre extends Model<RessourceEnregistreAttributes, RessourceEnregistreCreationAttributes> implements RessourceEnregistreAttributes {
  IDUtilisateur!: number;
  IDRessource!: number;

  // RessourceEnregistre belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;
  // RessourceEnregistre belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof RessourceEnregistre {
    return RessourceEnregistre.init({
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    },
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Ressource',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'RessourceEnregistre',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
          { name: "IDRessource" },
        ]
      },
      {
        name: "IDRessource",
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
        ]
      },
      {
        name: "IDUtilisateur",
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
        ]
      },
    ]
  });
  }
}
