import type { Abonnement, AbonnementId } from './Abonnement';
import type { Activite, ActiviteId } from './Activite';
import type { Commune, CommuneId } from './Commune';
import type { DonneesCommentaire, DonneesCommentaireId } from './DonneesCommentaire';
import type { Historique, HistoriqueId } from './Historique';
import type { InscriptionActivite, InscriptionActiviteId } from './InscriptionActivite';
import type { LikeMessage, LikeMessageId } from './LikeMessage';
import type { Ressource, RessourceId } from './Ressource';
import type { RessourceEnregistre, RessourceEnregistreId } from './RessourceEnregistre';
import type { RessourceExploite, RessourceExploiteId } from './RessourceExploite';
import type { RessourceFavoris, RessourceFavorisId } from './RessourceFavoris';
import type { RessourceNonExploite, RessourceNonExploiteId } from './RessourceNonExploite';
import type { Role, RoleId } from './Role';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface UtilisateurAttributes {
  ID: number;
  Nom: string;
  Prenom: string;
  Email: string;
  MotDePasse: string;
  EstActif: number;
  IDRole: number;
  IDCommune: number;
}

export type UtilisateurPk = "ID";
export type UtilisateurId = Utilisateur[UtilisateurPk];
export type UtilisateurOptionalAttributes = "ID" | "EstActif" | "IDRole";
export type UtilisateurCreationAttributes = Optional<UtilisateurAttributes, UtilisateurOptionalAttributes>;

export class Utilisateur extends Model<UtilisateurAttributes, UtilisateurCreationAttributes> implements UtilisateurAttributes {
  ID!: number;
  Nom!: string;
  Prenom!: string;
  Email!: string;
  MotDePasse!: string;
  EstActif!: number;
  IDRole!: number;
  IDCommune!: number;

  // Utilisateur belongsTo Commune via IDCommune
  IDCommune_Commune!: Commune;
  getIDCommune_Commune!: Sequelize.BelongsToGetAssociationMixin<Commune>;
  setIDCommune_Commune!: Sequelize.BelongsToSetAssociationMixin<Commune, CommuneId>;
  createIDCommune_Commune!: Sequelize.BelongsToCreateAssociationMixin<Commune>;
  // Utilisateur belongsTo Role via IDRole
  IDRole_Role!: Role;
  getIDRole_Role!: Sequelize.BelongsToGetAssociationMixin<Role>;
  setIDRole_Role!: Sequelize.BelongsToSetAssociationMixin<Role, RoleId>;
  createIDRole_Role!: Sequelize.BelongsToCreateAssociationMixin<Role>;
  // Utilisateur hasMany Abonnement via IDUtilisateur
  Abonnements!: Abonnement[];
  getAbonnements!: Sequelize.HasManyGetAssociationsMixin<Abonnement>;
  setAbonnements!: Sequelize.HasManySetAssociationsMixin<Abonnement, AbonnementId>;
  addAbonnement!: Sequelize.HasManyAddAssociationMixin<Abonnement, AbonnementId>;
  addAbonnements!: Sequelize.HasManyAddAssociationsMixin<Abonnement, AbonnementId>;
  createAbonnement!: Sequelize.HasManyCreateAssociationMixin<Abonnement>;
  removeAbonnement!: Sequelize.HasManyRemoveAssociationMixin<Abonnement, AbonnementId>;
  removeAbonnements!: Sequelize.HasManyRemoveAssociationsMixin<Abonnement, AbonnementId>;
  hasAbonnement!: Sequelize.HasManyHasAssociationMixin<Abonnement, AbonnementId>;
  hasAbonnements!: Sequelize.HasManyHasAssociationsMixin<Abonnement, AbonnementId>;
  countAbonnements!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany Abonnement via IDAbonnement
  IDAbonnement_Abonnements!: Abonnement[];
  getIDAbonnement_Abonnements!: Sequelize.HasManyGetAssociationsMixin<Abonnement>;
  setIDAbonnement_Abonnements!: Sequelize.HasManySetAssociationsMixin<Abonnement, AbonnementId>;
  addIDAbonnement_Abonnement!: Sequelize.HasManyAddAssociationMixin<Abonnement, AbonnementId>;
  addIDAbonnement_Abonnements!: Sequelize.HasManyAddAssociationsMixin<Abonnement, AbonnementId>;
  createIDAbonnement_Abonnement!: Sequelize.HasManyCreateAssociationMixin<Abonnement>;
  removeIDAbonnement_Abonnement!: Sequelize.HasManyRemoveAssociationMixin<Abonnement, AbonnementId>;
  removeIDAbonnement_Abonnements!: Sequelize.HasManyRemoveAssociationsMixin<Abonnement, AbonnementId>;
  hasIDAbonnement_Abonnement!: Sequelize.HasManyHasAssociationMixin<Abonnement, AbonnementId>;
  hasIDAbonnement_Abonnements!: Sequelize.HasManyHasAssociationsMixin<Abonnement, AbonnementId>;
  countIDAbonnement_Abonnements!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur belongsToMany Activite via IDUtilisateur and IDActivite
  IDActivite_Activites!: Activite[];
  getIDActivite_Activites!: Sequelize.BelongsToManyGetAssociationsMixin<Activite>;
  setIDActivite_Activites!: Sequelize.BelongsToManySetAssociationsMixin<Activite, ActiviteId>;
  addIDActivite_Activite!: Sequelize.BelongsToManyAddAssociationMixin<Activite, ActiviteId>;
  addIDActivite_Activites!: Sequelize.BelongsToManyAddAssociationsMixin<Activite, ActiviteId>;
  createIDActivite_Activite!: Sequelize.BelongsToManyCreateAssociationMixin<Activite>;
  removeIDActivite_Activite!: Sequelize.BelongsToManyRemoveAssociationMixin<Activite, ActiviteId>;
  removeIDActivite_Activites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Activite, ActiviteId>;
  hasIDActivite_Activite!: Sequelize.BelongsToManyHasAssociationMixin<Activite, ActiviteId>;
  hasIDActivite_Activites!: Sequelize.BelongsToManyHasAssociationsMixin<Activite, ActiviteId>;
  countIDActivite_Activites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur hasMany DonneesCommentaire via IDUtilisateur
  DonneesCommentaires!: DonneesCommentaire[];
  getDonneesCommentaires!: Sequelize.HasManyGetAssociationsMixin<DonneesCommentaire>;
  setDonneesCommentaires!: Sequelize.HasManySetAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  addDonneesCommentaire!: Sequelize.HasManyAddAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  addDonneesCommentaires!: Sequelize.HasManyAddAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  createDonneesCommentaire!: Sequelize.HasManyCreateAssociationMixin<DonneesCommentaire>;
  removeDonneesCommentaire!: Sequelize.HasManyRemoveAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  removeDonneesCommentaires!: Sequelize.HasManyRemoveAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  hasDonneesCommentaire!: Sequelize.HasManyHasAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  hasDonneesCommentaires!: Sequelize.HasManyHasAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  countDonneesCommentaires!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur belongsToMany DonneesCommentaire via IDUtilisateur and IDCommentaire
  IDCommentaire_DonneesCommentaires!: DonneesCommentaire[];
  getIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManyGetAssociationsMixin<DonneesCommentaire>;
  setIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManySetAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  addIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToManyAddAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  addIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManyAddAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  createIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToManyCreateAssociationMixin<DonneesCommentaire>;
  removeIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToManyRemoveAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  removeIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManyRemoveAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  hasIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToManyHasAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  hasIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManyHasAssociationsMixin<DonneesCommentaire, DonneesCommentaireId>;
  countIDCommentaire_DonneesCommentaires!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur hasMany Historique via IDUtilisateur
  Historiques!: Historique[];
  getHistoriques!: Sequelize.HasManyGetAssociationsMixin<Historique>;
  setHistoriques!: Sequelize.HasManySetAssociationsMixin<Historique, HistoriqueId>;
  addHistorique!: Sequelize.HasManyAddAssociationMixin<Historique, HistoriqueId>;
  addHistoriques!: Sequelize.HasManyAddAssociationsMixin<Historique, HistoriqueId>;
  createHistorique!: Sequelize.HasManyCreateAssociationMixin<Historique>;
  removeHistorique!: Sequelize.HasManyRemoveAssociationMixin<Historique, HistoriqueId>;
  removeHistoriques!: Sequelize.HasManyRemoveAssociationsMixin<Historique, HistoriqueId>;
  hasHistorique!: Sequelize.HasManyHasAssociationMixin<Historique, HistoriqueId>;
  hasHistoriques!: Sequelize.HasManyHasAssociationsMixin<Historique, HistoriqueId>;
  countHistoriques!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany InscriptionActivite via IDUtilisateur
  InscriptionActivites!: InscriptionActivite[];
  getInscriptionActivites!: Sequelize.HasManyGetAssociationsMixin<InscriptionActivite>;
  setInscriptionActivites!: Sequelize.HasManySetAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  addInscriptionActivite!: Sequelize.HasManyAddAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  addInscriptionActivites!: Sequelize.HasManyAddAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  createInscriptionActivite!: Sequelize.HasManyCreateAssociationMixin<InscriptionActivite>;
  removeInscriptionActivite!: Sequelize.HasManyRemoveAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  removeInscriptionActivites!: Sequelize.HasManyRemoveAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  hasInscriptionActivite!: Sequelize.HasManyHasAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  hasInscriptionActivites!: Sequelize.HasManyHasAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  countInscriptionActivites!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany LikeMessage via IDUtilisateur
  LikeMessages!: LikeMessage[];
  getLikeMessages!: Sequelize.HasManyGetAssociationsMixin<LikeMessage>;
  setLikeMessages!: Sequelize.HasManySetAssociationsMixin<LikeMessage, LikeMessageId>;
  addLikeMessage!: Sequelize.HasManyAddAssociationMixin<LikeMessage, LikeMessageId>;
  addLikeMessages!: Sequelize.HasManyAddAssociationsMixin<LikeMessage, LikeMessageId>;
  createLikeMessage!: Sequelize.HasManyCreateAssociationMixin<LikeMessage>;
  removeLikeMessage!: Sequelize.HasManyRemoveAssociationMixin<LikeMessage, LikeMessageId>;
  removeLikeMessages!: Sequelize.HasManyRemoveAssociationsMixin<LikeMessage, LikeMessageId>;
  hasLikeMessage!: Sequelize.HasManyHasAssociationMixin<LikeMessage, LikeMessageId>;
  hasLikeMessages!: Sequelize.HasManyHasAssociationsMixin<LikeMessage, LikeMessageId>;
  countLikeMessages!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany Ressource via IDAuteur
  Ressources!: Ressource[];
  getRessources!: Sequelize.HasManyGetAssociationsMixin<Ressource>;
  setRessources!: Sequelize.HasManySetAssociationsMixin<Ressource, RessourceId>;
  addRessource!: Sequelize.HasManyAddAssociationMixin<Ressource, RessourceId>;
  addRessources!: Sequelize.HasManyAddAssociationsMixin<Ressource, RessourceId>;
  createRessource!: Sequelize.HasManyCreateAssociationMixin<Ressource>;
  removeRessource!: Sequelize.HasManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeRessources!: Sequelize.HasManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasRessource!: Sequelize.HasManyHasAssociationMixin<Ressource, RessourceId>;
  hasRessources!: Sequelize.HasManyHasAssociationsMixin<Ressource, RessourceId>;
  countRessources!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur belongsToMany Ressource via IDUtilisateur and IDRessource
  IDRessource_Ressource_RessourceEnregistres!: Ressource[];
  getIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManyGetAssociationsMixin<Ressource>;
  setIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManySetAssociationsMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceEnregistre!: Sequelize.BelongsToManyAddAssociationMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManyAddAssociationsMixin<Ressource, RessourceId>;
  createIDRessource_Ressource_RessourceEnregistre!: Sequelize.BelongsToManyCreateAssociationMixin<Ressource>;
  removeIDRessource_Ressource_RessourceEnregistre!: Sequelize.BelongsToManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceEnregistre!: Sequelize.BelongsToManyHasAssociationMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManyHasAssociationsMixin<Ressource, RessourceId>;
  countIDRessource_Ressource_RessourceEnregistres!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur belongsToMany Ressource via IDUtilisateur and IDRessource
  IDRessource_Ressource_RessourceExploites!: Ressource[];
  getIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManyGetAssociationsMixin<Ressource>;
  setIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManySetAssociationsMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceExploite!: Sequelize.BelongsToManyAddAssociationMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManyAddAssociationsMixin<Ressource, RessourceId>;
  createIDRessource_Ressource_RessourceExploite!: Sequelize.BelongsToManyCreateAssociationMixin<Ressource>;
  removeIDRessource_Ressource_RessourceExploite!: Sequelize.BelongsToManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceExploite!: Sequelize.BelongsToManyHasAssociationMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManyHasAssociationsMixin<Ressource, RessourceId>;
  countIDRessource_Ressource_RessourceExploites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur belongsToMany Ressource via IDUtilisateur and IDRessource
  IDRessource_Ressource_RessourceFavoris!: Ressource[];
  getIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManyGetAssociationsMixin<Ressource>;
  setIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManySetAssociationsMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceFavori!: Sequelize.BelongsToManyAddAssociationMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManyAddAssociationsMixin<Ressource, RessourceId>;
  createIDRessource_Ressource_RessourceFavori!: Sequelize.BelongsToManyCreateAssociationMixin<Ressource>;
  removeIDRessource_Ressource_RessourceFavori!: Sequelize.BelongsToManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceFavori!: Sequelize.BelongsToManyHasAssociationMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManyHasAssociationsMixin<Ressource, RessourceId>;
  countIDRessource_Ressource_RessourceFavoris!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur belongsToMany Ressource via IDUtilisateur and IDRessource
  IDRessource_Ressource_RessourceNonExploites!: Ressource[];
  getIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManyGetAssociationsMixin<Ressource>;
  setIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManySetAssociationsMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceNonExploite!: Sequelize.BelongsToManyAddAssociationMixin<Ressource, RessourceId>;
  addIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManyAddAssociationsMixin<Ressource, RessourceId>;
  createIDRessource_Ressource_RessourceNonExploite!: Sequelize.BelongsToManyCreateAssociationMixin<Ressource>;
  removeIDRessource_Ressource_RessourceNonExploite!: Sequelize.BelongsToManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceNonExploite!: Sequelize.BelongsToManyHasAssociationMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManyHasAssociationsMixin<Ressource, RessourceId>;
  countIDRessource_Ressource_RessourceNonExploites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur hasMany RessourceEnregistre via IDUtilisateur
  RessourceEnregistres!: RessourceEnregistre[];
  getRessourceEnregistres!: Sequelize.HasManyGetAssociationsMixin<RessourceEnregistre>;
  setRessourceEnregistres!: Sequelize.HasManySetAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  addRessourceEnregistre!: Sequelize.HasManyAddAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  addRessourceEnregistres!: Sequelize.HasManyAddAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  createRessourceEnregistre!: Sequelize.HasManyCreateAssociationMixin<RessourceEnregistre>;
  removeRessourceEnregistre!: Sequelize.HasManyRemoveAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  removeRessourceEnregistres!: Sequelize.HasManyRemoveAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  hasRessourceEnregistre!: Sequelize.HasManyHasAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  hasRessourceEnregistres!: Sequelize.HasManyHasAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  countRessourceEnregistres!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany RessourceExploite via IDUtilisateur
  RessourceExploites!: RessourceExploite[];
  getRessourceExploites!: Sequelize.HasManyGetAssociationsMixin<RessourceExploite>;
  setRessourceExploites!: Sequelize.HasManySetAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  addRessourceExploite!: Sequelize.HasManyAddAssociationMixin<RessourceExploite, RessourceExploiteId>;
  addRessourceExploites!: Sequelize.HasManyAddAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  createRessourceExploite!: Sequelize.HasManyCreateAssociationMixin<RessourceExploite>;
  removeRessourceExploite!: Sequelize.HasManyRemoveAssociationMixin<RessourceExploite, RessourceExploiteId>;
  removeRessourceExploites!: Sequelize.HasManyRemoveAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  hasRessourceExploite!: Sequelize.HasManyHasAssociationMixin<RessourceExploite, RessourceExploiteId>;
  hasRessourceExploites!: Sequelize.HasManyHasAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  countRessourceExploites!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany RessourceFavoris via IDUtilisateur
  RessourceFavoris!: RessourceFavoris[];
  getRessourceFavoris!: Sequelize.HasManyGetAssociationsMixin<RessourceFavoris>;
  setRessourceFavoris!: Sequelize.HasManySetAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  addRessourceFavori!: Sequelize.HasManyAddAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  addRessourceFavoris!: Sequelize.HasManyAddAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  createRessourceFavori!: Sequelize.HasManyCreateAssociationMixin<RessourceFavoris>;
  removeRessourceFavori!: Sequelize.HasManyRemoveAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  removeRessourceFavoris!: Sequelize.HasManyRemoveAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  hasRessourceFavori!: Sequelize.HasManyHasAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  hasRessourceFavoris!: Sequelize.HasManyHasAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  countRessourceFavoris!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur hasMany RessourceNonExploite via IDUtilisateur
  RessourceNonExploites!: RessourceNonExploite[];
  getRessourceNonExploites!: Sequelize.HasManyGetAssociationsMixin<RessourceNonExploite>;
  setRessourceNonExploites!: Sequelize.HasManySetAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  addRessourceNonExploite!: Sequelize.HasManyAddAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  addRessourceNonExploites!: Sequelize.HasManyAddAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  createRessourceNonExploite!: Sequelize.HasManyCreateAssociationMixin<RessourceNonExploite>;
  removeRessourceNonExploite!: Sequelize.HasManyRemoveAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  removeRessourceNonExploites!: Sequelize.HasManyRemoveAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  hasRessourceNonExploite!: Sequelize.HasManyHasAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  hasRessourceNonExploites!: Sequelize.HasManyHasAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  countRessourceNonExploites!: Sequelize.HasManyCountAssociationsMixin;
  // Utilisateur belongsToMany Utilisateur via IDUtilisateur and IDAbonnement
  IDAbonnement_Utilisateurs!: Utilisateur[];
  getIDAbonnement_Utilisateurs!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDAbonnement_Utilisateurs!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDAbonnement_Utilisateur!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDAbonnement_Utilisateurs!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDAbonnement_Utilisateur!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDAbonnement_Utilisateur!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDAbonnement_Utilisateurs!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDAbonnement_Utilisateur!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDAbonnement_Utilisateurs!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDAbonnement_Utilisateurs!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Utilisateur belongsToMany Utilisateur via IDAbonnement and IDUtilisateur
  IDUtilisateur_Utilisateurs!: Utilisateur[];
  getIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateurs!: Sequelize.BelongsToManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Utilisateur {
    return Utilisateur.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Nom: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    Prenom: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    Email: {
      type: DataTypes.STRING(200),
      allowNull: false,
      unique: "Email"
    },
    MotDePasse: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    EstActif: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 1
    },
    IDRole: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      references: {
        model: 'Role',
        key: 'ID'
      }
    },
    IDCommune: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Commune',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Utilisateur',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Email",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Email" },
        ]
      },
      {
        name: "IDRole",
        using: "BTREE",
        fields: [
          { name: "IDRole" },
        ]
      },
      {
        name: "IDCommune",
        using: "BTREE",
        fields: [
          { name: "IDCommune" },
        ]
      },
    ]
  });
  }
}
