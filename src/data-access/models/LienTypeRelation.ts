import type { Ressource, RessourceId } from './Ressource';
import type { TypeRelation, TypeRelationId } from './TypeRelation';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface LienTypeRelationAttributes {
  IDRessource: number;
  IDTypeRelation: number;
}

export type LienTypeRelationPk = "IDRessource" | "IDTypeRelation";
export type LienTypeRelationId = LienTypeRelation[LienTypeRelationPk];
export type LienTypeRelationCreationAttributes = LienTypeRelationAttributes;

export class LienTypeRelation extends Model<LienTypeRelationAttributes, LienTypeRelationCreationAttributes> implements LienTypeRelationAttributes {
  IDRessource!: number;
  IDTypeRelation!: number;

  // LienTypeRelation belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;
  // LienTypeRelation belongsTo TypeRelation via IDTypeRelation
  IDTypeRelation_TypeRelation!: TypeRelation;
  getIDTypeRelation_TypeRelation!: Sequelize.BelongsToGetAssociationMixin<TypeRelation>;
  setIDTypeRelation_TypeRelation!: Sequelize.BelongsToSetAssociationMixin<TypeRelation, TypeRelationId>;
  createIDTypeRelation_TypeRelation!: Sequelize.BelongsToCreateAssociationMixin<TypeRelation>;

  static initModel(sequelize: Sequelize.Sequelize): typeof LienTypeRelation {
    return LienTypeRelation.init({
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Ressource',
        key: 'ID'
      }
    },
    IDTypeRelation: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'TypeRelation',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'LienTypeRelation',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
          { name: "IDTypeRelation" },
        ]
      },
      {
        name: "IDTypeRelation",
        using: "BTREE",
        fields: [
          { name: "IDTypeRelation" },
        ]
      },
    ]
  });
  }
}
