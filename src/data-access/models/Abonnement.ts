import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface AbonnementAttributes {
  IDUtilisateur: number;
  IDAbonnement: number;
}

export type AbonnementPk = "IDUtilisateur" | "IDAbonnement";
export type AbonnementId = Abonnement[AbonnementPk];
export type AbonnementCreationAttributes = AbonnementAttributes;

export class Abonnement extends Model<AbonnementAttributes, AbonnementCreationAttributes> implements AbonnementAttributes {
  IDUtilisateur!: number;
  IDAbonnement!: number;

  // Abonnement belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;
  // Abonnement belongsTo Utilisateur via IDAbonnement
  IDAbonnement_Utilisateur!: Utilisateur;
  getIDAbonnement_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDAbonnement_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDAbonnement_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Abonnement {
    return Abonnement.init({
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    },
    IDAbonnement: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Abonnement',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
          { name: "IDAbonnement" },
        ]
      },
      {
        name: "IDAbonnement",
        using: "BTREE",
        fields: [
          { name: "IDAbonnement" },
        ]
      },
    ]
  });
  }
}
