import type { Commentaire, CommentaireCreationAttributes, CommentaireId } from './Commentaire';
import type { LikeMessage, LikeMessageId } from './LikeMessage';
import type { ReponseCommentaire, ReponseCommentaireCreationAttributes, ReponseCommentaireId } from './ReponseCommentaire';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface DonneesCommentaireAttributes {
  ID: number;
  DateCreation: Date;
  Contenu: string;
  EstActif: number;
  IDUtilisateur: number;
}

export type DonneesCommentairePk = "ID";
export type DonneesCommentaireId = DonneesCommentaire[DonneesCommentairePk];
export type DonneesCommentaireOptionalAttributes = "ID";
export type DonneesCommentaireCreationAttributes = Optional<DonneesCommentaireAttributes, DonneesCommentaireOptionalAttributes>;

export class DonneesCommentaire extends Model<DonneesCommentaireAttributes, DonneesCommentaireCreationAttributes> implements DonneesCommentaireAttributes {
  ID!: number;
  DateCreation!: Date;
  Contenu!: string;
  EstActif!: number;
  IDUtilisateur!: number;

  // DonneesCommentaire hasOne Commentaire via ID
  Commentaire!: Commentaire;
  getCommentaire!: Sequelize.HasOneGetAssociationMixin<Commentaire>;
  setCommentaire!: Sequelize.HasOneSetAssociationMixin<Commentaire, CommentaireId>;
  createCommentaire!: Sequelize.HasOneCreateAssociationMixin<Commentaire>;
  // DonneesCommentaire hasMany LikeMessage via IDCommentaire
  LikeMessages!: LikeMessage[];
  getLikeMessages!: Sequelize.HasManyGetAssociationsMixin<LikeMessage>;
  setLikeMessages!: Sequelize.HasManySetAssociationsMixin<LikeMessage, LikeMessageId>;
  addLikeMessage!: Sequelize.HasManyAddAssociationMixin<LikeMessage, LikeMessageId>;
  addLikeMessages!: Sequelize.HasManyAddAssociationsMixin<LikeMessage, LikeMessageId>;
  createLikeMessage!: Sequelize.HasManyCreateAssociationMixin<LikeMessage>;
  removeLikeMessage!: Sequelize.HasManyRemoveAssociationMixin<LikeMessage, LikeMessageId>;
  removeLikeMessages!: Sequelize.HasManyRemoveAssociationsMixin<LikeMessage, LikeMessageId>;
  hasLikeMessage!: Sequelize.HasManyHasAssociationMixin<LikeMessage, LikeMessageId>;
  hasLikeMessages!: Sequelize.HasManyHasAssociationsMixin<LikeMessage, LikeMessageId>;
  countLikeMessages!: Sequelize.HasManyCountAssociationsMixin;
  // DonneesCommentaire hasOne ReponseCommentaire via ID
  ReponseCommentaire!: ReponseCommentaire;
  getReponseCommentaire!: Sequelize.HasOneGetAssociationMixin<ReponseCommentaire>;
  setReponseCommentaire!: Sequelize.HasOneSetAssociationMixin<ReponseCommentaire, ReponseCommentaireId>;
  createReponseCommentaire!: Sequelize.HasOneCreateAssociationMixin<ReponseCommentaire>;
  // DonneesCommentaire belongsToMany Utilisateur via IDCommentaire and IDUtilisateur
  IDUtilisateur_Utilisateur_LikeMessages!: Utilisateur[];
  getIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_LikeMessage!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_LikeMessage!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_LikeMessage!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_LikeMessage!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_LikeMessages!: Sequelize.BelongsToManyCountAssociationsMixin;
  // DonneesCommentaire belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof DonneesCommentaire {
    return DonneesCommentaire.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    DateCreation: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Contenu: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    EstActif: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'DonneesCommentaire',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDUtilisateur",
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
        ]
      },
    ]
  });
  }
}
