import type { Activite, ActiviteCreationAttributes, ActiviteId } from './Activite';
import type { CategorieRessource, CategorieRessourceId } from './CategorieRessource';
import type { Commentaire, CommentaireId } from './Commentaire';
import type { Document, DocumentId } from './Document';
import type { HistoriqueConsultation, HistoriqueConsultationId } from './HistoriqueConsultation';
import type { LienTypeRelation, LienTypeRelationId } from './LienTypeRelation';
import type { RessourceEnregistre, RessourceEnregistreId } from './RessourceEnregistre';
import type { RessourceExploite, RessourceExploiteId } from './RessourceExploite';
import type { RessourceFavoris, RessourceFavorisId } from './RessourceFavoris';
import type { RessourceNonExploite, RessourceNonExploiteId } from './RessourceNonExploite';
import type { Statut, StatutId } from './Statut';
import type { TypeRelation, TypeRelationId } from './TypeRelation';
import type { TypeRessource, TypeRessourceId } from './TypeRessource';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface RessourceAttributes {
  ID: number;
  Titre: string;
  DateCreation: Date;
  Contenu: string;
  IDStatut: number;
  IDAuteur: number;
  IDCategorieRessource: number;
  IDTypeRessource: number;
}

export type RessourcePk = "ID";
export type RessourceId = Ressource[RessourcePk];
export type RessourceOptionalAttributes = "ID";
export type RessourceCreationAttributes = Optional<RessourceAttributes, RessourceOptionalAttributes>;

export class Ressource extends Model<RessourceAttributes, RessourceCreationAttributes> implements RessourceAttributes {
  ID!: number;
  Titre!: string;
  DateCreation!: Date;
  Contenu!: string;
  IDStatut!: number;
  IDAuteur!: number;
  IDCategorieRessource!: number;
  IDTypeRessource!: number;

  // Ressource belongsTo CategorieRessource via IDCategorieRessource
  IDCategorieRessource_CategorieRessource!: CategorieRessource;
  getIDCategorieRessource_CategorieRessource!: Sequelize.BelongsToGetAssociationMixin<CategorieRessource>;
  setIDCategorieRessource_CategorieRessource!: Sequelize.BelongsToSetAssociationMixin<CategorieRessource, CategorieRessourceId>;
  createIDCategorieRessource_CategorieRessource!: Sequelize.BelongsToCreateAssociationMixin<CategorieRessource>;
  // Ressource hasOne Activite via IDRessource
  Activite!: Activite;
  getActivite!: Sequelize.HasOneGetAssociationMixin<Activite>;
  setActivite!: Sequelize.HasOneSetAssociationMixin<Activite, ActiviteId>;
  createActivite!: Sequelize.HasOneCreateAssociationMixin<Activite>;
  // Ressource hasMany Commentaire via IDRessource
  Commentaires!: Commentaire[];
  getCommentaires!: Sequelize.HasManyGetAssociationsMixin<Commentaire>;
  setCommentaires!: Sequelize.HasManySetAssociationsMixin<Commentaire, CommentaireId>;
  addCommentaire!: Sequelize.HasManyAddAssociationMixin<Commentaire, CommentaireId>;
  addCommentaires!: Sequelize.HasManyAddAssociationsMixin<Commentaire, CommentaireId>;
  createCommentaire!: Sequelize.HasManyCreateAssociationMixin<Commentaire>;
  removeCommentaire!: Sequelize.HasManyRemoveAssociationMixin<Commentaire, CommentaireId>;
  removeCommentaires!: Sequelize.HasManyRemoveAssociationsMixin<Commentaire, CommentaireId>;
  hasCommentaire!: Sequelize.HasManyHasAssociationMixin<Commentaire, CommentaireId>;
  hasCommentaires!: Sequelize.HasManyHasAssociationsMixin<Commentaire, CommentaireId>;
  countCommentaires!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany Document via IDRessource
  Documents!: Document[];
  getDocuments!: Sequelize.HasManyGetAssociationsMixin<Document>;
  setDocuments!: Sequelize.HasManySetAssociationsMixin<Document, DocumentId>;
  addDocument!: Sequelize.HasManyAddAssociationMixin<Document, DocumentId>;
  addDocuments!: Sequelize.HasManyAddAssociationsMixin<Document, DocumentId>;
  createDocument!: Sequelize.HasManyCreateAssociationMixin<Document>;
  removeDocument!: Sequelize.HasManyRemoveAssociationMixin<Document, DocumentId>;
  removeDocuments!: Sequelize.HasManyRemoveAssociationsMixin<Document, DocumentId>;
  hasDocument!: Sequelize.HasManyHasAssociationMixin<Document, DocumentId>;
  hasDocuments!: Sequelize.HasManyHasAssociationsMixin<Document, DocumentId>;
  countDocuments!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany HistoriqueConsultation via IDRessource
  HistoriqueConsultations!: HistoriqueConsultation[];
  getHistoriqueConsultations!: Sequelize.HasManyGetAssociationsMixin<HistoriqueConsultation>;
  setHistoriqueConsultations!: Sequelize.HasManySetAssociationsMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  addHistoriqueConsultation!: Sequelize.HasManyAddAssociationMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  addHistoriqueConsultations!: Sequelize.HasManyAddAssociationsMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  createHistoriqueConsultation!: Sequelize.HasManyCreateAssociationMixin<HistoriqueConsultation>;
  removeHistoriqueConsultation!: Sequelize.HasManyRemoveAssociationMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  removeHistoriqueConsultations!: Sequelize.HasManyRemoveAssociationsMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  hasHistoriqueConsultation!: Sequelize.HasManyHasAssociationMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  hasHistoriqueConsultations!: Sequelize.HasManyHasAssociationsMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  countHistoriqueConsultations!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany LienTypeRelation via IDRessource
  LienTypeRelations!: LienTypeRelation[];
  getLienTypeRelations!: Sequelize.HasManyGetAssociationsMixin<LienTypeRelation>;
  setLienTypeRelations!: Sequelize.HasManySetAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  addLienTypeRelation!: Sequelize.HasManyAddAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  addLienTypeRelations!: Sequelize.HasManyAddAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  createLienTypeRelation!: Sequelize.HasManyCreateAssociationMixin<LienTypeRelation>;
  removeLienTypeRelation!: Sequelize.HasManyRemoveAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  removeLienTypeRelations!: Sequelize.HasManyRemoveAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  hasLienTypeRelation!: Sequelize.HasManyHasAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  hasLienTypeRelations!: Sequelize.HasManyHasAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  countLienTypeRelations!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany RessourceEnregistre via IDRessource
  RessourceEnregistres!: RessourceEnregistre[];
  getRessourceEnregistres!: Sequelize.HasManyGetAssociationsMixin<RessourceEnregistre>;
  setRessourceEnregistres!: Sequelize.HasManySetAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  addRessourceEnregistre!: Sequelize.HasManyAddAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  addRessourceEnregistres!: Sequelize.HasManyAddAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  createRessourceEnregistre!: Sequelize.HasManyCreateAssociationMixin<RessourceEnregistre>;
  removeRessourceEnregistre!: Sequelize.HasManyRemoveAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  removeRessourceEnregistres!: Sequelize.HasManyRemoveAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  hasRessourceEnregistre!: Sequelize.HasManyHasAssociationMixin<RessourceEnregistre, RessourceEnregistreId>;
  hasRessourceEnregistres!: Sequelize.HasManyHasAssociationsMixin<RessourceEnregistre, RessourceEnregistreId>;
  countRessourceEnregistres!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany RessourceExploite via IDRessource
  RessourceExploites!: RessourceExploite[];
  getRessourceExploites!: Sequelize.HasManyGetAssociationsMixin<RessourceExploite>;
  setRessourceExploites!: Sequelize.HasManySetAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  addRessourceExploite!: Sequelize.HasManyAddAssociationMixin<RessourceExploite, RessourceExploiteId>;
  addRessourceExploites!: Sequelize.HasManyAddAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  createRessourceExploite!: Sequelize.HasManyCreateAssociationMixin<RessourceExploite>;
  removeRessourceExploite!: Sequelize.HasManyRemoveAssociationMixin<RessourceExploite, RessourceExploiteId>;
  removeRessourceExploites!: Sequelize.HasManyRemoveAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  hasRessourceExploite!: Sequelize.HasManyHasAssociationMixin<RessourceExploite, RessourceExploiteId>;
  hasRessourceExploites!: Sequelize.HasManyHasAssociationsMixin<RessourceExploite, RessourceExploiteId>;
  countRessourceExploites!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany RessourceFavoris via IDRessource
  RessourceFavoris!: RessourceFavoris[];
  getRessourceFavoris!: Sequelize.HasManyGetAssociationsMixin<RessourceFavoris>;
  setRessourceFavoris!: Sequelize.HasManySetAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  addRessourceFavori!: Sequelize.HasManyAddAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  addRessourceFavoris!: Sequelize.HasManyAddAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  createRessourceFavori!: Sequelize.HasManyCreateAssociationMixin<RessourceFavoris>;
  removeRessourceFavori!: Sequelize.HasManyRemoveAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  removeRessourceFavoris!: Sequelize.HasManyRemoveAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  hasRessourceFavori!: Sequelize.HasManyHasAssociationMixin<RessourceFavoris, RessourceFavorisId>;
  hasRessourceFavoris!: Sequelize.HasManyHasAssociationsMixin<RessourceFavoris, RessourceFavorisId>;
  countRessourceFavoris!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource hasMany RessourceNonExploite via IDRessource
  RessourceNonExploites!: RessourceNonExploite[];
  getRessourceNonExploites!: Sequelize.HasManyGetAssociationsMixin<RessourceNonExploite>;
  setRessourceNonExploites!: Sequelize.HasManySetAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  addRessourceNonExploite!: Sequelize.HasManyAddAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  addRessourceNonExploites!: Sequelize.HasManyAddAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  createRessourceNonExploite!: Sequelize.HasManyCreateAssociationMixin<RessourceNonExploite>;
  removeRessourceNonExploite!: Sequelize.HasManyRemoveAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  removeRessourceNonExploites!: Sequelize.HasManyRemoveAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  hasRessourceNonExploite!: Sequelize.HasManyHasAssociationMixin<RessourceNonExploite, RessourceNonExploiteId>;
  hasRessourceNonExploites!: Sequelize.HasManyHasAssociationsMixin<RessourceNonExploite, RessourceNonExploiteId>;
  countRessourceNonExploites!: Sequelize.HasManyCountAssociationsMixin;
  // Ressource belongsToMany TypeRelation via IDRessource and IDTypeRelation
  IDTypeRelation_TypeRelations!: TypeRelation[];
  getIDTypeRelation_TypeRelations!: Sequelize.BelongsToManyGetAssociationsMixin<TypeRelation>;
  setIDTypeRelation_TypeRelations!: Sequelize.BelongsToManySetAssociationsMixin<TypeRelation, TypeRelationId>;
  addIDTypeRelation_TypeRelation!: Sequelize.BelongsToManyAddAssociationMixin<TypeRelation, TypeRelationId>;
  addIDTypeRelation_TypeRelations!: Sequelize.BelongsToManyAddAssociationsMixin<TypeRelation, TypeRelationId>;
  createIDTypeRelation_TypeRelation!: Sequelize.BelongsToManyCreateAssociationMixin<TypeRelation>;
  removeIDTypeRelation_TypeRelation!: Sequelize.BelongsToManyRemoveAssociationMixin<TypeRelation, TypeRelationId>;
  removeIDTypeRelation_TypeRelations!: Sequelize.BelongsToManyRemoveAssociationsMixin<TypeRelation, TypeRelationId>;
  hasIDTypeRelation_TypeRelation!: Sequelize.BelongsToManyHasAssociationMixin<TypeRelation, TypeRelationId>;
  hasIDTypeRelation_TypeRelations!: Sequelize.BelongsToManyHasAssociationsMixin<TypeRelation, TypeRelationId>;
  countIDTypeRelation_TypeRelations!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Ressource belongsToMany Utilisateur via IDRessource and IDUtilisateur
  IDUtilisateur_Utilisateur_RessourceEnregistres!: Utilisateur[];
  getIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceEnregistre!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_RessourceEnregistre!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_RessourceEnregistre!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceEnregistre!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_RessourceEnregistres!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Ressource belongsToMany Utilisateur via IDRessource and IDUtilisateur
  IDUtilisateur_Utilisateur_RessourceExploites!: Utilisateur[];
  getIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceExploite!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_RessourceExploite!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_RessourceExploite!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceExploite!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_RessourceExploites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Ressource belongsToMany Utilisateur via IDRessource and IDUtilisateur
  IDUtilisateur_Utilisateur_RessourceFavoris!: Utilisateur[];
  getIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceFavori!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_RessourceFavori!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_RessourceFavori!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceFavori!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_RessourceFavoris!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Ressource belongsToMany Utilisateur via IDRessource and IDUtilisateur
  IDUtilisateur_Utilisateur_RessourceNonExploites!: Utilisateur[];
  getIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceNonExploite!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_RessourceNonExploite!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_RessourceNonExploite!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceNonExploite!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_RessourceNonExploites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Ressource belongsTo Statut via IDStatut
  IDStatut_Statut!: Statut;
  getIDStatut_Statut!: Sequelize.BelongsToGetAssociationMixin<Statut>;
  setIDStatut_Statut!: Sequelize.BelongsToSetAssociationMixin<Statut, StatutId>;
  createIDStatut_Statut!: Sequelize.BelongsToCreateAssociationMixin<Statut>;
  // Ressource belongsTo TypeRessource via IDTypeRessource
  IDTypeRessource_TypeRessource!: TypeRessource;
  getIDTypeRessource_TypeRessource!: Sequelize.BelongsToGetAssociationMixin<TypeRessource>;
  setIDTypeRessource_TypeRessource!: Sequelize.BelongsToSetAssociationMixin<TypeRessource, TypeRessourceId>;
  createIDTypeRessource_TypeRessource!: Sequelize.BelongsToCreateAssociationMixin<TypeRessource>;
  // Ressource belongsTo Utilisateur via IDAuteur
  IDAuteur_Utilisateur!: Utilisateur;
  getIDAuteur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDAuteur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDAuteur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Ressource {
    return Ressource.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Titre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    DateCreation: {
      type: DataTypes.DATE,
      allowNull: false
    },
    Contenu: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    IDStatut: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Statut',
        key: 'ID'
      }
    },
    IDAuteur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    },
    IDCategorieRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'CategorieRessource',
        key: 'ID'
      }
    },
    IDTypeRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'TypeRessource',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Ressource',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDStatut",
        using: "BTREE",
        fields: [
          { name: "IDStatut" },
        ]
      },
      {
        name: "IDAuteur",
        using: "BTREE",
        fields: [
          { name: "IDAuteur" },
        ]
      },
      {
        name: "IDCategorieRessource",
        using: "BTREE",
        fields: [
          { name: "IDCategorieRessource" },
        ]
      },
      {
        name: "IDTypeRessource",
        using: "BTREE",
        fields: [
          { name: "IDTypeRessource" },
        ]
      },
    ]
  });
  }
}
