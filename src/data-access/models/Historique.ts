import type { HistoriqueConsultation, HistoriqueConsultationCreationAttributes, HistoriqueConsultationId } from './HistoriqueConsultation';
import type { HistoriqueRecherche, HistoriqueRechercheCreationAttributes, HistoriqueRechercheId } from './HistoriqueRecherche';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface HistoriqueAttributes {
  ID: number;
  DateEvenement: Date;
  IDUtilisateur?: number;
}

export type HistoriquePk = "ID";
export type HistoriqueId = Historique[HistoriquePk];
export type HistoriqueOptionalAttributes = "ID" | "IDUtilisateur";
export type HistoriqueCreationAttributes = Optional<HistoriqueAttributes, HistoriqueOptionalAttributes>;

export class Historique extends Model<HistoriqueAttributes, HistoriqueCreationAttributes> implements HistoriqueAttributes {
  ID!: number;
  DateEvenement!: Date;
  IDUtilisateur?: number;

  // Historique hasOne HistoriqueConsultation via ID
  HistoriqueConsultation!: HistoriqueConsultation;
  getHistoriqueConsultation!: Sequelize.HasOneGetAssociationMixin<HistoriqueConsultation>;
  setHistoriqueConsultation!: Sequelize.HasOneSetAssociationMixin<HistoriqueConsultation, HistoriqueConsultationId>;
  createHistoriqueConsultation!: Sequelize.HasOneCreateAssociationMixin<HistoriqueConsultation>;
  // Historique hasOne HistoriqueRecherche via ID
  HistoriqueRecherche!: HistoriqueRecherche;
  getHistoriqueRecherche!: Sequelize.HasOneGetAssociationMixin<HistoriqueRecherche>;
  setHistoriqueRecherche!: Sequelize.HasOneSetAssociationMixin<HistoriqueRecherche, HistoriqueRechercheId>;
  createHistoriqueRecherche!: Sequelize.HasOneCreateAssociationMixin<HistoriqueRecherche>;
  // Historique belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Historique {
    return Historique.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    DateEvenement: {
      type: DataTypes.DATE,
      allowNull: false
    },
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Historique',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDUtilisateur",
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
        ]
      },
    ]
  });
  }
}
