import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface CategorieRessourceAttributes {
  ID: number;
  Code: string;
  Libelle: string;
}

export type CategorieRessourcePk = "ID";
export type CategorieRessourceId = CategorieRessource[CategorieRessourcePk];
export type CategorieRessourceOptionalAttributes = "ID";
export type CategorieRessourceCreationAttributes = Optional<CategorieRessourceAttributes, CategorieRessourceOptionalAttributes>;

export class CategorieRessource extends Model<CategorieRessourceAttributes, CategorieRessourceCreationAttributes> implements CategorieRessourceAttributes {
  ID!: number;
  Code!: string;
  Libelle!: string;

  // CategorieRessource hasMany Ressource via IDCategorieRessource
  Ressources!: Ressource[];
  getRessources!: Sequelize.HasManyGetAssociationsMixin<Ressource>;
  setRessources!: Sequelize.HasManySetAssociationsMixin<Ressource, RessourceId>;
  addRessource!: Sequelize.HasManyAddAssociationMixin<Ressource, RessourceId>;
  addRessources!: Sequelize.HasManyAddAssociationsMixin<Ressource, RessourceId>;
  createRessource!: Sequelize.HasManyCreateAssociationMixin<Ressource>;
  removeRessource!: Sequelize.HasManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeRessources!: Sequelize.HasManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasRessource!: Sequelize.HasManyHasAssociationMixin<Ressource, RessourceId>;
  hasRessources!: Sequelize.HasManyHasAssociationsMixin<Ressource, RessourceId>;
  countRessources!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof CategorieRessource {
    return CategorieRessource.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "Code"
    },
    Libelle: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'CategorieRessource',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Code" },
        ]
      },
    ]
  });
  }
}
