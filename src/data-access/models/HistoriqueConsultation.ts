import type { Historique, HistoriqueId } from './Historique';
import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface HistoriqueConsultationAttributes {
  ID: number;
  IDRessource: number;
}

export type HistoriqueConsultationPk = "ID";
export type HistoriqueConsultationId = HistoriqueConsultation[HistoriqueConsultationPk];
export type HistoriqueConsultationCreationAttributes = HistoriqueConsultationAttributes;

export class HistoriqueConsultation extends Model<HistoriqueConsultationAttributes, HistoriqueConsultationCreationAttributes> implements HistoriqueConsultationAttributes {
  ID!: number;
  IDRessource!: number;

  // HistoriqueConsultation belongsTo Historique via ID
  ID_Historique!: Historique;
  getID_Historique!: Sequelize.BelongsToGetAssociationMixin<Historique>;
  setID_Historique!: Sequelize.BelongsToSetAssociationMixin<Historique, HistoriqueId>;
  createID_Historique!: Sequelize.BelongsToCreateAssociationMixin<Historique>;
  // HistoriqueConsultation belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;

  static initModel(sequelize: Sequelize.Sequelize): typeof HistoriqueConsultation {
    return HistoriqueConsultation.init({
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Historique',
        key: 'ID'
      }
    },
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Ressource',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'HistoriqueConsultation',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDRessource",
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
        ]
      },
    ]
  });
  }
}
