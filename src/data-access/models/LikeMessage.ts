import type { DonneesCommentaire, DonneesCommentaireId } from './DonneesCommentaire';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface LikeMessageAttributes {
  IDUtilisateur: number;
  IDCommentaire: number;
}

export type LikeMessagePk = "IDUtilisateur" | "IDCommentaire";
export type LikeMessageId = LikeMessage[LikeMessagePk];
export type LikeMessageCreationAttributes = LikeMessageAttributes;

export class LikeMessage extends Model<LikeMessageAttributes, LikeMessageCreationAttributes> implements LikeMessageAttributes {
  IDUtilisateur!: number;
  IDCommentaire!: number;

  // LikeMessage belongsTo DonneesCommentaire via IDCommentaire
  IDCommentaire_DonneesCommentaire!: DonneesCommentaire;
  getIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToGetAssociationMixin<DonneesCommentaire>;
  setIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToSetAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  createIDCommentaire_DonneesCommentaire!: Sequelize.BelongsToCreateAssociationMixin<DonneesCommentaire>;
  // LikeMessage belongsTo Utilisateur via IDUtilisateur
  IDUtilisateur_Utilisateur!: Utilisateur;
  getIDUtilisateur_Utilisateur!: Sequelize.BelongsToGetAssociationMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur!: Sequelize.BelongsToSetAssociationMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur!: Sequelize.BelongsToCreateAssociationMixin<Utilisateur>;

  static initModel(sequelize: Sequelize.Sequelize): typeof LikeMessage {
    return LikeMessage.init({
    IDUtilisateur: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Utilisateur',
        key: 'ID'
      }
    },
    IDCommentaire: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'DonneesCommentaire',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'LikeMessage',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDUtilisateur" },
          { name: "IDCommentaire" },
        ]
      },
      {
        name: "IDCommentaire",
        using: "BTREE",
        fields: [
          { name: "IDCommentaire" },
        ]
      },
    ]
  });
  }
}
