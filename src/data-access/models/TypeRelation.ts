import type { LienTypeRelation, LienTypeRelationId } from './LienTypeRelation';
import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface TypeRelationAttributes {
  ID: number;
  Code: string;
  Libelle: string;
}

export type TypeRelationPk = "ID";
export type TypeRelationId = TypeRelation[TypeRelationPk];
export type TypeRelationOptionalAttributes = "ID";
export type TypeRelationCreationAttributes = Optional<TypeRelationAttributes, TypeRelationOptionalAttributes>;

export class TypeRelation extends Model<TypeRelationAttributes, TypeRelationCreationAttributes> implements TypeRelationAttributes {
  ID!: number;
  Code!: string;
  Libelle!: string;

  // TypeRelation hasMany LienTypeRelation via IDTypeRelation
  LienTypeRelations!: LienTypeRelation[];
  getLienTypeRelations!: Sequelize.HasManyGetAssociationsMixin<LienTypeRelation>;
  setLienTypeRelations!: Sequelize.HasManySetAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  addLienTypeRelation!: Sequelize.HasManyAddAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  addLienTypeRelations!: Sequelize.HasManyAddAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  createLienTypeRelation!: Sequelize.HasManyCreateAssociationMixin<LienTypeRelation>;
  removeLienTypeRelation!: Sequelize.HasManyRemoveAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  removeLienTypeRelations!: Sequelize.HasManyRemoveAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  hasLienTypeRelation!: Sequelize.HasManyHasAssociationMixin<LienTypeRelation, LienTypeRelationId>;
  hasLienTypeRelations!: Sequelize.HasManyHasAssociationsMixin<LienTypeRelation, LienTypeRelationId>;
  countLienTypeRelations!: Sequelize.HasManyCountAssociationsMixin;
  // TypeRelation belongsToMany Ressource via IDTypeRelation and IDRessource
  IDRessource_Ressources!: Ressource[];
  getIDRessource_Ressources!: Sequelize.BelongsToManyGetAssociationsMixin<Ressource>;
  setIDRessource_Ressources!: Sequelize.BelongsToManySetAssociationsMixin<Ressource, RessourceId>;
  addIDRessource_Ressource!: Sequelize.BelongsToManyAddAssociationMixin<Ressource, RessourceId>;
  addIDRessource_Ressources!: Sequelize.BelongsToManyAddAssociationsMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToManyCreateAssociationMixin<Ressource>;
  removeIDRessource_Ressource!: Sequelize.BelongsToManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeIDRessource_Ressources!: Sequelize.BelongsToManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasIDRessource_Ressource!: Sequelize.BelongsToManyHasAssociationMixin<Ressource, RessourceId>;
  hasIDRessource_Ressources!: Sequelize.BelongsToManyHasAssociationsMixin<Ressource, RessourceId>;
  countIDRessource_Ressources!: Sequelize.BelongsToManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof TypeRelation {
    return TypeRelation.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: "Code"
    },
    Libelle: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'TypeRelation',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Code" },
        ]
      },
    ]
  });
  }
}
