import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface TypeRessourceAttributes {
  ID: number;
  Code: string;
  Libelle: string;
}

export type TypeRessourcePk = "ID";
export type TypeRessourceId = TypeRessource[TypeRessourcePk];
export type TypeRessourceOptionalAttributes = "ID";
export type TypeRessourceCreationAttributes = Optional<TypeRessourceAttributes, TypeRessourceOptionalAttributes>;

export class TypeRessource extends Model<TypeRessourceAttributes, TypeRessourceCreationAttributes> implements TypeRessourceAttributes {
  ID!: number;
  Code!: string;
  Libelle!: string;

  // TypeRessource hasMany Ressource via IDTypeRessource
  Ressources!: Ressource[];
  getRessources!: Sequelize.HasManyGetAssociationsMixin<Ressource>;
  setRessources!: Sequelize.HasManySetAssociationsMixin<Ressource, RessourceId>;
  addRessource!: Sequelize.HasManyAddAssociationMixin<Ressource, RessourceId>;
  addRessources!: Sequelize.HasManyAddAssociationsMixin<Ressource, RessourceId>;
  createRessource!: Sequelize.HasManyCreateAssociationMixin<Ressource>;
  removeRessource!: Sequelize.HasManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeRessources!: Sequelize.HasManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasRessource!: Sequelize.HasManyHasAssociationMixin<Ressource, RessourceId>;
  hasRessources!: Sequelize.HasManyHasAssociationsMixin<Ressource, RessourceId>;
  countRessources!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof TypeRessource {
    return TypeRessource.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "Code"
    },
    Libelle: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'TypeRessource',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Code" },
        ]
      },
    ]
  });
  }
}
