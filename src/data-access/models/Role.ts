import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface RoleAttributes {
  ID: number;
  Code: string;
  Libelle: string;
}

export type RolePk = "ID";
export type RoleId = Role[RolePk];
export type RoleOptionalAttributes = "ID";
export type RoleCreationAttributes = Optional<RoleAttributes, RoleOptionalAttributes>;

export class Role extends Model<RoleAttributes, RoleCreationAttributes> implements RoleAttributes {
  ID!: number;
  Code!: string;
  Libelle!: string;

  // Role hasMany Utilisateur via IDRole
  Utilisateurs!: Utilisateur[];
  getUtilisateurs!: Sequelize.HasManyGetAssociationsMixin<Utilisateur>;
  setUtilisateurs!: Sequelize.HasManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addUtilisateur!: Sequelize.HasManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addUtilisateurs!: Sequelize.HasManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createUtilisateur!: Sequelize.HasManyCreateAssociationMixin<Utilisateur>;
  removeUtilisateur!: Sequelize.HasManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeUtilisateurs!: Sequelize.HasManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasUtilisateur!: Sequelize.HasManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasUtilisateurs!: Sequelize.HasManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countUtilisateurs!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Role {
    return Role.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "Code"
    },
    Libelle: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'Role',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Code" },
        ]
      },
    ]
  });
  }
}
