import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface StatutAttributes {
  ID: number;
  Code: string;
  Libelle: string;
}

export type StatutPk = "ID";
export type StatutId = Statut[StatutPk];
export type StatutOptionalAttributes = "ID";
export type StatutCreationAttributes = Optional<StatutAttributes, StatutOptionalAttributes>;

export class Statut extends Model<StatutAttributes, StatutCreationAttributes> implements StatutAttributes {
  ID!: number;
  Code!: string;
  Libelle!: string;

  // Statut hasMany Ressource via IDStatut
  Ressources!: Ressource[];
  getRessources!: Sequelize.HasManyGetAssociationsMixin<Ressource>;
  setRessources!: Sequelize.HasManySetAssociationsMixin<Ressource, RessourceId>;
  addRessource!: Sequelize.HasManyAddAssociationMixin<Ressource, RessourceId>;
  addRessources!: Sequelize.HasManyAddAssociationsMixin<Ressource, RessourceId>;
  createRessource!: Sequelize.HasManyCreateAssociationMixin<Ressource>;
  removeRessource!: Sequelize.HasManyRemoveAssociationMixin<Ressource, RessourceId>;
  removeRessources!: Sequelize.HasManyRemoveAssociationsMixin<Ressource, RessourceId>;
  hasRessource!: Sequelize.HasManyHasAssociationMixin<Ressource, RessourceId>;
  hasRessources!: Sequelize.HasManyHasAssociationsMixin<Ressource, RessourceId>;
  countRessources!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Statut {
    return Statut.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Code: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "Code"
    },
    Libelle: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'Statut',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "Code" },
        ]
      },
    ]
  });
  }
}
