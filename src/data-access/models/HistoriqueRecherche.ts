import type { Historique, HistoriqueId } from './Historique';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface HistoriqueRechercheAttributes {
  ID: number;
  RechercheEffectuee?: string;
}

export type HistoriqueRecherchePk = "ID";
export type HistoriqueRechercheId = HistoriqueRecherche[HistoriqueRecherchePk];
export type HistoriqueRechercheOptionalAttributes = "RechercheEffectuee";
export type HistoriqueRechercheCreationAttributes = Optional<HistoriqueRechercheAttributes, HistoriqueRechercheOptionalAttributes>;

export class HistoriqueRecherche extends Model<HistoriqueRechercheAttributes, HistoriqueRechercheCreationAttributes> implements HistoriqueRechercheAttributes {
  ID!: number;
  RechercheEffectuee?: string;

  // HistoriqueRecherche belongsTo Historique via ID
  ID_Historique!: Historique;
  getID_Historique!: Sequelize.BelongsToGetAssociationMixin<Historique>;
  setID_Historique!: Sequelize.BelongsToSetAssociationMixin<Historique, HistoriqueId>;
  createID_Historique!: Sequelize.BelongsToCreateAssociationMixin<Historique>;

  static initModel(sequelize: Sequelize.Sequelize): typeof HistoriqueRecherche {
    return HistoriqueRecherche.init({
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Historique',
        key: 'ID'
      }
    },
    RechercheEffectuee: {
      type: DataTypes.STRING(1000),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'HistoriqueRecherche',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
    ]
  });
  }
}
