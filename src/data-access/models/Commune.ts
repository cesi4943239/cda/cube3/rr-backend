import type { Utilisateur, UtilisateurId } from './Utilisateur';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface CommuneAttributes {
  ID: number;
  CodePostal: string;
  Ville: string;
}

export type CommunePk = "ID";
export type CommuneId = Commune[CommunePk];
export type CommuneOptionalAttributes = "ID";
export type CommuneCreationAttributes = Optional<CommuneAttributes, CommuneOptionalAttributes>;

export class Commune extends Model<CommuneAttributes, CommuneCreationAttributes> implements CommuneAttributes {
  ID!: number;
  CodePostal!: string;
  Ville!: string;

  // Commune hasMany Utilisateur via IDCommune
  Utilisateurs!: Utilisateur[];
  getUtilisateurs!: Sequelize.HasManyGetAssociationsMixin<Utilisateur>;
  setUtilisateurs!: Sequelize.HasManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addUtilisateur!: Sequelize.HasManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addUtilisateurs!: Sequelize.HasManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createUtilisateur!: Sequelize.HasManyCreateAssociationMixin<Utilisateur>;
  removeUtilisateur!: Sequelize.HasManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeUtilisateurs!: Sequelize.HasManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasUtilisateur!: Sequelize.HasManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasUtilisateurs!: Sequelize.HasManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countUtilisateurs!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Commune {
    return Commune.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    CodePostal: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    Ville: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'Commune',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "Ville",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "CodePostal" },
          { name: "Ville" },
        ]
      },
    ]
  });
  }
}
