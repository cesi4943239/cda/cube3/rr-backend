import type { Ressource, RessourceId } from './Ressource';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface DocumentAttributes {
  ID: number;
  Nom: string;
  Extension: string;
  NomHash: string;
  IDRessource: number;
}

export type DocumentPk = "ID";
export type DocumentId = Document[DocumentPk];
export type DocumentOptionalAttributes = "ID";
export type DocumentCreationAttributes = Optional<DocumentAttributes, DocumentOptionalAttributes>;

export class Document extends Model<DocumentAttributes, DocumentCreationAttributes> implements DocumentAttributes {
  ID!: number;
  Nom!: string;
  Extension!: string;
  NomHash!: string;
  IDRessource!: number;

  // Document belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Document {
    return Document.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Nom: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    Extension: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    NomHash: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Ressource',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'Document',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDRessource",
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
        ]
      },
    ]
  });
  }
}
