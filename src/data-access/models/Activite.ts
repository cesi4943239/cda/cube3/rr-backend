import type { InscriptionActivite, InscriptionActiviteId } from './InscriptionActivite';
import type { Ressource, RessourceId } from './Ressource';
import type { Utilisateur, UtilisateurId } from './Utilisateur';
import { DataTypes, Model, Optional } from 'sequelize';
import * as Sequelize from 'sequelize';

export interface ActiviteAttributes {
  ID: number;
  DateDebut?: Date;
  DateFin?: Date;
  IDRessource: number;
}

export type ActivitePk = "ID";
export type ActiviteId = Activite[ActivitePk];
export type ActiviteOptionalAttributes = "ID" | "DateDebut" | "DateFin";
export type ActiviteCreationAttributes = Optional<ActiviteAttributes, ActiviteOptionalAttributes>;

export class Activite extends Model<ActiviteAttributes, ActiviteCreationAttributes> implements ActiviteAttributes {
  ID!: number;
  DateDebut?: Date;
  DateFin?: Date;
  IDRessource!: number;

  // Activite hasMany InscriptionActivite via IDActivite
  InscriptionActivites!: InscriptionActivite[];
  getInscriptionActivites!: Sequelize.HasManyGetAssociationsMixin<InscriptionActivite>;
  setInscriptionActivites!: Sequelize.HasManySetAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  addInscriptionActivite!: Sequelize.HasManyAddAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  addInscriptionActivites!: Sequelize.HasManyAddAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  createInscriptionActivite!: Sequelize.HasManyCreateAssociationMixin<InscriptionActivite>;
  removeInscriptionActivite!: Sequelize.HasManyRemoveAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  removeInscriptionActivites!: Sequelize.HasManyRemoveAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  hasInscriptionActivite!: Sequelize.HasManyHasAssociationMixin<InscriptionActivite, InscriptionActiviteId>;
  hasInscriptionActivites!: Sequelize.HasManyHasAssociationsMixin<InscriptionActivite, InscriptionActiviteId>;
  countInscriptionActivites!: Sequelize.HasManyCountAssociationsMixin;
  // Activite belongsToMany Utilisateur via IDActivite and IDUtilisateur
  IDUtilisateur_Utilisateur_InscriptionActivites!: Utilisateur[];
  getIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManyGetAssociationsMixin<Utilisateur>;
  setIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManySetAssociationsMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_InscriptionActivite!: Sequelize.BelongsToManyAddAssociationMixin<Utilisateur, UtilisateurId>;
  addIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManyAddAssociationsMixin<Utilisateur, UtilisateurId>;
  createIDUtilisateur_Utilisateur_InscriptionActivite!: Sequelize.BelongsToManyCreateAssociationMixin<Utilisateur>;
  removeIDUtilisateur_Utilisateur_InscriptionActivite!: Sequelize.BelongsToManyRemoveAssociationMixin<Utilisateur, UtilisateurId>;
  removeIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManyRemoveAssociationsMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_InscriptionActivite!: Sequelize.BelongsToManyHasAssociationMixin<Utilisateur, UtilisateurId>;
  hasIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManyHasAssociationsMixin<Utilisateur, UtilisateurId>;
  countIDUtilisateur_Utilisateur_InscriptionActivites!: Sequelize.BelongsToManyCountAssociationsMixin;
  // Activite belongsTo Ressource via IDRessource
  IDRessource_Ressource!: Ressource;
  getIDRessource_Ressource!: Sequelize.BelongsToGetAssociationMixin<Ressource>;
  setIDRessource_Ressource!: Sequelize.BelongsToSetAssociationMixin<Ressource, RessourceId>;
  createIDRessource_Ressource!: Sequelize.BelongsToCreateAssociationMixin<Ressource>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Activite {
    return Activite.init({
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    DateDebut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateFin: {
      type: DataTypes.DATE,
      allowNull: true
    },
    IDRessource: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Ressource',
        key: 'ID'
      },
      unique: "Activite_ibfk_1"
    }
  }, {
    sequelize,
    tableName: 'Activite',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "IDRessource",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "IDRessource" },
        ]
      },
    ]
  });
  }
}
