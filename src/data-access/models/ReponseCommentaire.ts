import type { Commentaire, CommentaireId } from './Commentaire';
import type { DonneesCommentaire, DonneesCommentaireId } from './DonneesCommentaire';
import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';

export interface ReponseCommentaireAttributes {
  ID: number;
  ID_IDCommentaire: number;
}

export type ReponseCommentairePk = "ID";
export type ReponseCommentaireId = ReponseCommentaire[ReponseCommentairePk];
export type ReponseCommentaireCreationAttributes = ReponseCommentaireAttributes;

export class ReponseCommentaire extends Model<ReponseCommentaireAttributes, ReponseCommentaireCreationAttributes> implements ReponseCommentaireAttributes {
  ID!: number;
  ID_IDCommentaire!: number;

  // ReponseCommentaire belongsTo Commentaire via ID_IDCommentaire
  ID_IDCommentaire_Commentaire!: Commentaire;
  getID_IDCommentaire_Commentaire!: Sequelize.BelongsToGetAssociationMixin<Commentaire>;
  setID_IDCommentaire_Commentaire!: Sequelize.BelongsToSetAssociationMixin<Commentaire, CommentaireId>;
  createID_IDCommentaire_Commentaire!: Sequelize.BelongsToCreateAssociationMixin<Commentaire>;
  // ReponseCommentaire belongsTo DonneesCommentaire via ID
  ID_DonneesCommentaire!: DonneesCommentaire;
  getID_DonneesCommentaire!: Sequelize.BelongsToGetAssociationMixin<DonneesCommentaire>;
  setID_DonneesCommentaire!: Sequelize.BelongsToSetAssociationMixin<DonneesCommentaire, DonneesCommentaireId>;
  createID_DonneesCommentaire!: Sequelize.BelongsToCreateAssociationMixin<DonneesCommentaire>;

  static initModel(sequelize: Sequelize.Sequelize): typeof ReponseCommentaire {
    return ReponseCommentaire.init({
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'DonneesCommentaire',
        key: 'ID'
      }
    },
    ID_IDCommentaire: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Commentaire',
        key: 'ID'
      }
    }
  }, {
    sequelize,
    tableName: 'ReponseCommentaire',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID" },
        ]
      },
      {
        name: "ID_IDCommentaire",
        using: "BTREE",
        fields: [
          { name: "ID_IDCommentaire" },
        ]
      },
    ]
  });
  }
}
