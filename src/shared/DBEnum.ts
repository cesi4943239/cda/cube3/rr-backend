enum DataSetSize {
    small = "small",
    medium = "medium",
    large = "large"
}
enum InitMode {
    loading = "loading",
    generating = "generating"
}

export const DB = {
    DataSetSize : DataSetSize,
    InitMode: InitMode
}