enum RolesEnum {
    user = 'UTILISATEUR',
    moderator = 'MODERATEUR',
    administrator = 'ADMIN',
    superAdministrator = 'SUPERADMIN'
}

export default RolesEnum