# Ressources Relationnelles - Back-end

> **Notice :**
> To run the following NPM commands, make sure to be located in the `rr-backend` directory.

## Install the development environment

### Quick start 
First, build the application:
```bash
npm run build
```

To quickly install all the dependencies, create the DB containers and launch the API, run the following command :

```bash
npm run quick-start
```

### Set-up the DB containers 

The docker-compose file allows you to easily set up the MariaDB and phpMyAdmin containers.

Use the command below to build the containers.
```bash
npm run docker-up
```

The containers should be build and run automatically. You can verify this by listing the running containers with the command below.
```bash
docker ps
```

To destroy the containers, run the following command :
```bash
npm run docker-down
```

If the containers were successfuly built and running, you can access the phpmyadmin inferface at [localhost:8081](http://localhost:8081).

> In the `development environment`, the default root user doesn't have a password.

### Set-up the Express API

>You have to install Node.js from [nodejs.org](https://nodejs.org) before following the steps below.

First, you have to go to the root project and install the dependencies with npm.
```bash
npm install
```
Then you can start the API locally.
```bash
npm start
```

The service should be accessible at [localhost:3005](http://localhost:3005). You can now start coding, enjoy !

> You can access the swagger docs at [localhost:3005/docs](http://localhost:3005/docs).

## Populate the DB with mock data

You can easily populate and clear the database with bash scripts located in the **tools/sequelize** directory.

**On Windows, the following commands must be run from a WSL terminal.**

To populate the db, use the following command :
```bash
npm run seed-db
```

To clear the database, use this command :
```bash
npm run clear-db
```

## Configure the DB with Docker
> The following section is here to help you to configure the database if necessary. However, you can skip these steps and start working if you've followed the instructions in the previous section.

### Automatically modify the DB structure from SQL script

You can update the DB with the SQL scripts on this location: `/tools/sql`. Once the scripts have been updated, you have to rebuild the containers.

### Configure root user credentials

The configuration of the database environment is accessible in the docker-compose file:

```bash
services:
    db:
        environment:
            # You can add a variable environment here
```
> You can find a resume of all the MariaDB environment variables on the [official MariaDB documentation](https://mariadb.com/kb/en/mariadb-server-docker-official-image-environment-variables/).

By default, the root user doesn't have a password. You can setup a password by adding the variables below.

```bash
services:
    db:
        environment:
            MARIADB_ALLOW_EMPTY_ROOT_PASSWORD: false
            MARIADB_ROOT_PASSWORD: password_example
```

### Add a DB user

You can add a new DB user and setup his credentials by adding the variables below.

```bash
services:
    db:
        environment:
            MYSQL_USER: foo
            MYSQL_PASSWORD: bar
```

## Generate schemas from DB with sequelize-cli
Sequelize provide tools to easily generate typescript models from your DB. You can generate them each time you modify your DB architecture.

### Dependencies
Sequelize-cli is a tool offering a set of commands to facilitate database management tasks. Sequelize-Auto is a sequelize-cli wrapper wich simplifies the process of generating Sequelize models from an existing database.
> You can refer to [sequelize-cli](https://github.com/sequelize/cli) and [sequelize-auto](https://github.com/sequelize/sequelize-auto) documentation for more informations.
```bash
npm install sequelize-cli -D
```
```bash
npm install sequelize-auto -D
```
### Generate schemas from DB
Navigate to the root directory of the project and run the following command. This will generate several directories, including a config directory containing configuration files for Sequelize and a models directory where Sequelize models will be stored.
```bash
npx sequelize-cli init
```

Next, use the command below to generate the schemas from the DB.
```bash
npx sequelize-auto -h <host> -d <db_name> -u <user_name> -x <password> -p <db_port> -l ts --dialect mariadb
```
The schemas should be generated into the `./models` directory.

### Import the generated models
Once the schemas have been generated, you need to move the models directory into to the `./src/data-access` directory. Replace the existing directory.

>Be sure to leave the **init-models.ts** file inside the directory. This file will allow you to easily export the models.

You can now use the models as follow.
```typescript
import { Utilisateur } from "../models/Utilisateur"
const users: Utilisateur[] = await Utilisateur.findAll()
```

## Automatically Populate DB

You can easily populate and clear the database with bash scripts located in the **tools/sequelize** directory.

First, navigate to the sequelize directory:
```bash
cd tools/sequelize
```

To populate the database, use the following command:
```bash
sed -i 's/\r$//' seed-db.sh && bash seed-db.sh
```

To clear the database, use this command:
```bash
sed -i 's/\r$//' clear-db.sh && bash clear-db.sh
```
>The sed command remove any windows style carriage returns from the script to ensure it run correctly on unix like systems.

## Tests your code

Use the following command to launch the e2e tests
```bash
npm test
```