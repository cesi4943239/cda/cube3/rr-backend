import * as dotenv from 'dotenv'


if (process.env.NODE_ENV && process.env.NODE_ENV == 'local ') {
    console.log('LOCAL')
    dotenv.config({ path: '.env.local' });
}
if (process.env.NODE_ENV && process.env.NODE_ENV == 'production') {
    console.log('PROD')
    dotenv.config({ path: '.env.production' });
}
// if (process.env.NODE_ENV && process.env.NODE_ENV == 'development') {
//     console.log('DEV')
//     dotenv.config({ path: '.env.development' });
// }
    console.log('DEV')
    dotenv.config({ path: '.env.development' });

console.log(`NODE_ENV: ${process.env.NODE_ENV}`)
console.log(`DB_PORT: ${process.env.DB_PORT}`)
console.log(`DB_NAME: ${process.env.DB_NAME}`)
console.log(`DB_USER: ${process.env.DB_USER}`)
console.log(`DB_PWD: ${process.env.DB_PWD}`)
console.log(`DB_HOST: ${process.env.DB_HOST}`)
console.log(`API_PORT: ${process.env.API_PORT}`)
console.log(`API_URL: ${process.env.API_URL}`)

const config = {
    jwt: {
        SECRET_KEY: process.env.JWT_SECRET_KEY || "***",
        DURATION: process.env.JWT_DURATION || "***"
    },
    dev: {
        api_port: process.env.API_PORT || "***",
    },
    db: {
        port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 12345,
        name: process.env.DB_NAME || "***",
        user: process.env.DB_USER || "***",
        user_pwd: process.env.DB_PWD || "",
        host: process.env.DB_HOST || "***"
    }
}

export default config