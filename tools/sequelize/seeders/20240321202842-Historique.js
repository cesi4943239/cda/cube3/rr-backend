'use strict';
const dataSet = require('../../mock-data/data-set/ressources_relationnelles_medium')

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return await Promise.all([
      queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0'),
      queryInterface.bulkDelete('Historique', null, {}),
      queryInterface.bulkInsert('Historique', dataSet.find(value => value.name === 'Historique')?.data),
      queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1')
    ])
  },

  async down (queryInterface, Sequelize) {
    return await Promise.all([
      queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0'),
      queryInterface.bulkDelete('Historique', null, {}),
      queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1'),
      queryInterface.sequelize.query(`ALTER TABLE Historique AUTO_INCREMENT = 1`),
    ])
  }
};
