#!/bin/bash

set -e

SEEDERS_DIR="./seeders/"

SEEDERS=(
    "${SEEDERS_DIR}20240308215349-Utilisateur.js"
    "${SEEDERS_DIR}20240320223103-Activite.js"
    "${SEEDERS_DIR}20240320224148-Ressource.js"
    "${SEEDERS_DIR}20240308221933-Abonnement.js"
    "${SEEDERS_DIR}20240321202343-Commentaire.js"
    "${SEEDERS_DIR}20240320223427-DonneesCommentaire.js"
    "${SEEDERS_DIR}20240321202842-Historique.js"
    "${SEEDERS_DIR}20240321203106-HistoriqueConsultation.js"
    "${SEEDERS_DIR}20240321203246-HistoriqueRecherche.js"
    "${SEEDERS_DIR}20240321203954-InscriptionActivite.js"
    "${SEEDERS_DIR}20240321204307-LienTypeRelation.js"
    "${SEEDERS_DIR}20240321205600-LikeMessage.js"
    "${SEEDERS_DIR}20240321210120-ReponseCommentaire.js"
    "${SEEDERS_DIR}20240321210331-RessourceEnregistre.js"
    "${SEEDERS_DIR}20240321210528-RessourceExploite.js"
    "${SEEDERS_DIR}20240321210706-RessourceFavoris.js"
    "${SEEDERS_DIR}20240321211147-RessourceNonExploite.js"
)

for seeder in "${SEEDERS[@]}"
do
    npx sequelize-cli db:seed --seed "$seeder"
done
