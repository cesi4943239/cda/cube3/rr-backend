#!/bin/bash

set -e

SEEDERS_DIR="seeders"

SEEDERS=(
    "20240308215349-Utilisateur.js"
    "20240320223103-Activite.js"
    "20240320224148-Ressource.js"
    "20240308221933-Abonnement.js"
    "20240321202343-Commentaire.js"
    "20240320223427-DonneesCommentaire.js"
    "20240321202842-Historique.js"
    "20240321203106-HistoriqueConsultation.js"
    "20240321203246-HistoriqueRecherche.js"
    "20240321203954-InscriptionActivite.js"
    "20240321204307-LienTypeRelation.js"
    "20240321205600-LikeMessage.js"
    "20240321210120-ReponseCommentaire.js"
    "20240321210331-RessourceEnregistre.js"
    "20240321210528-RessourceExploite.js"
    "20240321210706-RessourceFavoris.js"
    "20240321211147-RessourceNonExploite.js"
)

for seeder in "${SEEDERS[@]}"
do
    npx sequelize-cli db:seed:undo --seed "$seeder" --seeders-path "$SEEDERS_DIR"
done
