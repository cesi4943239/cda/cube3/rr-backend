import { faker } from '@faker-js/faker'

class HistoriqueConsultationFactory{
    constructor(){}

    public static getResources(userCount: number, resourceCount: number) {
        let mock = []
    
        for (let i = 0; i < userCount; i++) {
            const IDUtilisateur: number = i + 1
            const IDRessource: number = faker.number.int({ min: 1, max: resourceCount })
            const tmp = {
                ID: IDUtilisateur,
                IDRessource: IDRessource
            };
    
            mock.push(tmp)
        }
    
        return mock;
    }
}

export default HistoriqueConsultationFactory