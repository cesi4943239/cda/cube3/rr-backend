import { faker } from '@faker-js/faker'

class LienTypeRelationFactory{
    constructor(){}

    public static getResources(count: number) {
        let mock = []
    
        for (let i = 0; i < count; i++) {
            const tmp = {
                IDRessource: i+1,
                IDTypeRelation: faker.number.int({ min: 1, max: 6 })
            };
    
            mock.push(tmp);
        }
    
        return mock;
    }
}

export default LienTypeRelationFactory