import { faker } from '@faker-js/faker'

class RessourceEnregistreFactory {
    constructor(){}

    public static getResources(countEnregistre: number, countResource: number, countUser: number) {
        let mock = []
        const usedIds = new Set<number>()
    
        for (let i = 0; i < countEnregistre; i++) {
            let IDRessource: number = faker.number.int({ min: 1, max: countResource })
            let IDUtilisateur: number
    
            do {
                IDUtilisateur = faker.number.int({ min: 1, max: countUser })
            } while (usedIds.has(IDUtilisateur))
    
            const tmp = {
                IDUtilisateur: IDUtilisateur,
                IDRessource: IDRessource,
            };
    
            usedIds.add(IDUtilisateur)
            mock.push(tmp)
        }
        
        return mock
    }
}

export default RessourceEnregistreFactory
