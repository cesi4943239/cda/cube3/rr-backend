import { faker } from '@faker-js/faker'

class HistoriqueFactory{
    constructor(){}

    public static getResources(count: number) {
        let mock = []
    
        for (let i = 0; i < count; i++) {
            const IDUtilisateur: number = i + 1
            const tmp = {
                DateEvenement: faker.date.recent(),
                IDUtilisateur: IDUtilisateur
            };
    
            mock.push(tmp);
        }
    
        return mock;
    }
}

export default HistoriqueFactory