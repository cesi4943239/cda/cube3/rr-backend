import { faker } from '@faker-js/faker'

class HistoriqueRechercheFactory{
    constructor(){}

    public static getResources(count: number) {
        let mock = []
        const usedIds = new Set<number>()
    
        for (let i = 0; i < count; i++) {
            const IDUtilisateur: number = i + 1
            const tmp = {
                ID: IDUtilisateur,
                RechercheEffectuee: faker.lorem.sentence()
            };
    
            mock.push(tmp);
            usedIds.add(IDUtilisateur)
        }
    
        return mock;
    }
}

export default HistoriqueRechercheFactory