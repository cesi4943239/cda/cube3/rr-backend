import { faker } from '@faker-js/faker'

class LikeMessageFactory {
    constructor(){}

    public static getResources(countLike: number, countComment: number, countUser: number) {
        let mock = []
        const usedIds = new Set<number>()
    
        for (let i = 0; i < countLike; i++) {
            let IDCommentaire: number = faker.number.int({ min: 1, max: countComment })
            let IDUtilisateur: number
    
            do {
                IDUtilisateur = faker.number.int({ min: 1, max: countUser })
            } while (usedIds.has(IDUtilisateur))
    
            const tmp = {
                IDUtilisateur: IDUtilisateur,
                IDCommentaire: IDCommentaire,
            };
    
            usedIds.add(IDUtilisateur)
            mock.push(tmp)
        }
    
        return mock
    }
}

export default LikeMessageFactory
