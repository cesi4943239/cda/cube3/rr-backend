import { faker } from '@faker-js/faker'

class RessourceFactory{
    constructor(){}

    public static getResources(userCount: number, resourceCount: number) {
        let mock = []
    
        for (let i = 0; i < resourceCount; i++) {
            const ID: number = i + 1
            const tmp = {
                ID: ID,
                Titre: faker.lorem.sentence({ min: 1, max: 3 }),
                DateCreation: faker.date.recent({ days: 30 }),
                Contenu: faker.lorem.paragraphs({ min: 1, max: 5 }),
                IDStatut: faker.number.int({ min: 1, max: 5 }),
                IDAuteur: faker.number.int({ min: 1, max: userCount }),
                IDCategorieRessource: faker.number.int({ min: 1, max: 13 }),
                IDTypeRessource: faker.number.int({ min: 1, max: 8 })
            };
    
            mock.push(tmp)
        }
    
        return mock;
    }
}

export default RessourceFactory