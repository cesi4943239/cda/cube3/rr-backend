import { faker } from '@faker-js/faker'

class ReponseCommentaireFactory{
    constructor(){}

    public static getResources(responseCount: number, commentCount: number) {
        let mock = []
    
        for (let i = 0; i < responseCount; i++) {
            const ID: number = i + 1
            const ID_IDCommentaire: number = faker.number.int({ min: 1, max: commentCount })
    
            const tmp = {
                ID: ID,
                ID_IDCommentaire: ID_IDCommentaire,
            };
    
            mock.push(tmp)
        }
    
        return mock
    }
    
}

export default ReponseCommentaireFactory