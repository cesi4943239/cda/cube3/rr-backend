import { faker } from '@faker-js/faker'

class InscriptionActiviteFactory{
    constructor(){}

    public static getResources(count: number) {
        let mock = []
    
        for (let i = 0; i < count; i++) {
            const IDUtilisateur: number = i + 1
            const IDActivite: number = i + 1
    
            const tmp = {
                IDUtilisateur: IDUtilisateur,
                IDActivite: IDActivite,
            };
    
            mock.push(tmp)
        }
    
        return mock
    }
    
}

export default InscriptionActiviteFactory