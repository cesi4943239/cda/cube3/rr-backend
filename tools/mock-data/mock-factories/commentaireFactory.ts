import { faker } from '@faker-js/faker'

class CommentaireFactory{
    constructor(){}

    public static getResources(commentCount: number, resourceCount: number) {
        let mock = []
    
        for (let i = 0; i < commentCount; i++) {
            const ID: number = i + 1
            const IDRessource: number = faker.number.int({ min: 1, max: resourceCount })
            const tmp = {
                ID: ID,
                IDRessource: IDRessource
            };
    
            mock.push(tmp);
        }
    
        return mock;
    }
}

export default CommentaireFactory