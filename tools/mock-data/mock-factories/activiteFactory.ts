import { faker } from '@faker-js/faker';

class ActiviteFactory {
    constructor() {}

    public static getResources(activiteCount: number) {
        let mock = [];

        for (let i = 0; i < activiteCount; i++) {
            const ID: number = i + 1;
            const DateDebut: Date = faker.date.between({ from: '2020-01-01T00:00:00.000Z', to: '2030-02-01T00:00:00.000Z' })
            const DateFin: Date = faker.date.between({ from: '2020-03-01T00:00:00.000Z', to: '2030-04-01T00:00:00.000Z' })
            let IDRessource: number = i + 1;

            const tmp = {
                ID: ID,
                DateDebut: DateDebut,
                DateFin: DateFin,
                IDRessource: IDRessource
            };

            mock.push(tmp);
        }

        return mock;
    }
}

export default ActiviteFactory;
