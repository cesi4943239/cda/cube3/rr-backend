import { faker } from '@faker-js/faker'

class DonnesCommentaireFactory{
    constructor(){}

    public static getResources(userCount: number) {
        let mock = []
    
        for (let i = 0; i < userCount; i++) {
            const ID: number = i + 1
            const DateCreation: Date = faker.date.recent({ days: 10 })
            const Contenu: string = faker.lorem.sentences({ min: 1, max: 4})
            const EstActif: number = faker.number.int({ min: 0, max: 1 })
            const IDUtilisateur: number = faker.number.int({ min: 1, max: userCount })
            const tmp = {
                ID: ID,
                DateCreation: DateCreation,
                Contenu: Contenu,
                EstActif: EstActif,
                IDUtilisateur: IDUtilisateur
            };
            mock.push(tmp);
        }
    
        return mock;
    }
}

export default DonnesCommentaireFactory