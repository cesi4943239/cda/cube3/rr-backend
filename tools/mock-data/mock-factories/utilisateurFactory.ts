import { faker } from '@faker-js/faker'

class UtilisateurFactory{
    constructor(){}

    public static getResources(count: number){
        let mock = []
        let usedEmails = new Set()

        for(let i = 0; i < count; i++) {
            let email = faker.internet.email()
            do {
                email =  `${faker.number.int({ min: 1, max: 100000 })}${email}`
            }while(usedEmails.has(email))

            usedEmails.add(email)
            const tmp = {
                Prenom: faker.person.firstName(),
                Nom: faker.person.lastName(),
                Email: email,
                MotDePasse: faker.internet.password(),
                EstActif: faker.number.int({ min: 0, max: 1 }),
                IDRole: faker.number.int({ min: 1, max: 1 }),
                IDCommune: faker.number.int({ min: 1, max: 200 })
            }

            mock.push(tmp)
        }

        // Limit the number of moderators, administrators, and super-administrators
        const rolePercentage: number = Math.ceil(count * 0.1);
        const maxRole = 5
        let moderatorCount = 0;
        let administratorCount = 0;
        let superadministratorCount = 0;

        for (let i = 0; i < mock.length; i++) {
            if (moderatorCount < rolePercentage && moderatorCount < maxRole) {
                mock[i].IDRole = 2;
                moderatorCount++;
                continue;
            }
            if (administratorCount < rolePercentage && administratorCount < maxRole) {
                mock[i].IDRole = 3;
                administratorCount++;
                continue;
            }
            if (superadministratorCount < rolePercentage && superadministratorCount < maxRole) {
                mock[i].IDRole = 4;
                superadministratorCount++;
                continue;
            }
        }
        return mock
    }
}

export default UtilisateurFactory