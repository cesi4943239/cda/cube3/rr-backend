import { faker } from '@faker-js/faker'

class AbonnementFactory{
    constructor(){}

    public static getResources(count: number) {
        let mock = []
        const usedIds = new Set<number>()
    
        for (let i = 0; i < count; i++) {
            const IDUtilisateur: number = faker.number.int({ min: 1, max: count })
            let IDAbonnement: number
    
            // IDAbonnement should not be the same as IDUtilisateur
            do {
                IDAbonnement = faker.number.int({ min: 1, max: count })
            } while (IDUtilisateur === IDAbonnement || usedIds.has(IDAbonnement))
    
            const tmp = {
                IDUtilisateur: IDUtilisateur,
                IDAbonnement: IDAbonnement,
            };
    
            mock.push(tmp)
            usedIds.add(IDAbonnement)
        }
    
        return mock
    }
    
}

export default AbonnementFactory