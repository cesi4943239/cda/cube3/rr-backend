const mockUsers = [
    {
        Prenom: "marco",
        Nom: "polo",
        Email: "m.p@mail.com",
        MotDePasse: "123",
        EstActif: 1,
        IDRole: 1,
        IDCommune: 1
    },
    {
        Prenom: "marcel",
        Nom: "raclette",
        Email: "raclette@mail.fr",
        MotDePasse: "azerty",
        EstActif: 1,
        IDRole: 2,
        IDCommune: 3
    },
    {
        Prenom: "henry",
        Nom: "paul",
        Email: "henry@mail.com",
        MotDePasse: "p@ssw0rd!",
        EstActif: 0,
        IDRole: 3,
        IDCommune: 10
    },
    {
        Prenom: "jane",
        Nom: "smith",
        Email: "smith.jane@mail.com",
        MotDePasse: "s3CreT",
        EstActif: 0,
        IDRole: 4,
        IDCommune: 20
    }
];

export { mockUsers }