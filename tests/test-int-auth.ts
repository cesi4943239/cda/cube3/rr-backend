import { faker } from '@faker-js/faker';
import jwt  from "jsonwebtoken"
import request from 'supertest'

let jwtToken = ''
const userData = {
    lastName: faker.person.lastName(),
    firstName: faker.person.firstName(),
    mail: `${faker.number.int({ min: 1, max: 100000 })}.${faker.internet.email()}`,
    password: '123',
    idTown: '1'
};

describe('Register a new user', () => {
    it('should return a JWT', (done) => {

        request('localhost:3005')
            .post('/register')
            .send(userData)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                jwtToken = res.body.data.token
                const decodedJwt: any = jwt.decode(jwtToken)
                if(decodedJwt.mail && decodedJwt.mail !== userData.mail) { return done('Mail field in returned JWT is empty or doesn\'t match with the user mail') }
                if(decodedJwt.role && decodedJwt.role !== 'UTILISATEUR') { return done('Role field in returned JWT is empty or is not Utilisateur') }
                if(!decodedJwt.userId) { return done('ID field in returned JWT is null') }
                done();
            });
    });
});

describe("Login an existing user with 'Utilisateur' role", () => {
    it('should return a JWT', (done) => {

        const user = { mail: userData.mail, password: userData.password }
        request('localhost:3005')
            .post('/login')
            .send(user)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                jwtToken = res.body.data.token
                const decodedJwt: any = jwt.decode(jwtToken)
                if(decodedJwt.mail && decodedJwt.mail !== userData.mail) { return done('Mail field in returned JWT is empty or doesn\'t match with the user mail') }
                if(decodedJwt.role && decodedJwt.role !== 'UTILISATEUR') { return done('Role field in returned JWT is empty or is not Utilisateur') }
                if(!decodedJwt.userId) { return done('ID field in returned JWT is null') }
                done();
            });
    });
});
